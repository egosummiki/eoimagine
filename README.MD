# eoImaginé - Image Manipulation Application

*THIS APPLICATION IS IN EARLY DEVELOPMENT, AT THIS POINT IT CAN'T BE PRACTICALLY USED*

## Premise

The idea of this application is to take inspiration from VIM and Blender and other applications. As well as provide
modular photo editing capabilities. Implemented with OpenGL and Qt. (Vulkan support may be considered.)

## Dependencies

+ CMake
+ LibPython
+ OpenGL 4.0
+ Qt 5.11

## Complilation

This has been complied only under linux. No idea if this complies under Windows or MacOS.
*basename()* function may cause some problems under windows.

The project uses CMake. Preferred compilation routine:

```bash
mkdir build
cd build

cmake ..
cmake --build . --target eoimagine
```

Due to early stage of development, no install procedure is provided.
Output binary is located in the *run* directory.


This *README* is going to be updated as the progres of this applications goes on.
