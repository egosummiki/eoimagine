/****************************************************************************
** Meta object code from reading C++ file 'eoMenuBar.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/ui/eoMenuBar.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'eoMenuBar.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_eoMenuBar_t {
    QByteArrayData data[27];
    char stringdata0[311];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_eoMenuBar_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_eoMenuBar_t qt_meta_stringdata_eoMenuBar = {
    {
QT_MOC_LITERAL(0, 0, 9), // "eoMenuBar"
QT_MOC_LITERAL(1, 10, 15), // "quitApplication"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 12), // "openDocument"
QT_MOC_LITERAL(4, 40, 11), // "importImage"
QT_MOC_LITERAL(5, 52, 12), // "saveDocument"
QT_MOC_LITERAL(6, 65, 11), // "newDocument"
QT_MOC_LITERAL(7, 77, 4), // "undo"
QT_MOC_LITERAL(8, 82, 4), // "redo"
QT_MOC_LITERAL(9, 87, 12), // "deleteActive"
QT_MOC_LITERAL(10, 100, 9), // "selectAll"
QT_MOC_LITERAL(11, 110, 10), // "selectNone"
QT_MOC_LITERAL(12, 121, 12), // "selectInvert"
QT_MOC_LITERAL(13, 134, 13), // "selectionGrow"
QT_MOC_LITERAL(14, 148, 15), // "selectionShrink"
QT_MOC_LITERAL(15, 164, 15), // "selectionHarden"
QT_MOC_LITERAL(16, 180, 15), // "canvasBackCheck"
QT_MOC_LITERAL(17, 196, 11), // "canvasWhite"
QT_MOC_LITERAL(18, 208, 11), // "canvasBlack"
QT_MOC_LITERAL(19, 220, 10), // "canvasPink"
QT_MOC_LITERAL(20, 231, 11), // "canvasGreen"
QT_MOC_LITERAL(21, 243, 11), // "layerMoveUp"
QT_MOC_LITERAL(22, 255, 13), // "layerMoveDown"
QT_MOC_LITERAL(23, 269, 11), // "layerToggle"
QT_MOC_LITERAL(24, 281, 11), // "layerRemove"
QT_MOC_LITERAL(25, 293, 11), // "modifyGBlur"
QT_MOC_LITERAL(26, 305, 5) // "about"

    },
    "eoMenuBar\0quitApplication\0\0openDocument\0"
    "importImage\0saveDocument\0newDocument\0"
    "undo\0redo\0deleteActive\0selectAll\0"
    "selectNone\0selectInvert\0selectionGrow\0"
    "selectionShrink\0selectionHarden\0"
    "canvasBackCheck\0canvasWhite\0canvasBlack\0"
    "canvasPink\0canvasGreen\0layerMoveUp\0"
    "layerMoveDown\0layerToggle\0layerRemove\0"
    "modifyGBlur\0about"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_eoMenuBar[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      25,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  139,    2, 0x08 /* Private */,
       3,    0,  140,    2, 0x08 /* Private */,
       4,    0,  141,    2, 0x08 /* Private */,
       5,    0,  142,    2, 0x08 /* Private */,
       6,    0,  143,    2, 0x08 /* Private */,
       7,    0,  144,    2, 0x08 /* Private */,
       8,    0,  145,    2, 0x08 /* Private */,
       9,    0,  146,    2, 0x08 /* Private */,
      10,    0,  147,    2, 0x08 /* Private */,
      11,    0,  148,    2, 0x08 /* Private */,
      12,    0,  149,    2, 0x08 /* Private */,
      13,    0,  150,    2, 0x08 /* Private */,
      14,    0,  151,    2, 0x08 /* Private */,
      15,    0,  152,    2, 0x08 /* Private */,
      16,    0,  153,    2, 0x08 /* Private */,
      17,    0,  154,    2, 0x08 /* Private */,
      18,    0,  155,    2, 0x08 /* Private */,
      19,    0,  156,    2, 0x08 /* Private */,
      20,    0,  157,    2, 0x08 /* Private */,
      21,    0,  158,    2, 0x08 /* Private */,
      22,    0,  159,    2, 0x08 /* Private */,
      23,    0,  160,    2, 0x08 /* Private */,
      24,    0,  161,    2, 0x08 /* Private */,
      25,    0,  162,    2, 0x08 /* Private */,
      26,    0,  163,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void eoMenuBar::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        eoMenuBar *_t = static_cast<eoMenuBar *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->quitApplication(); break;
        case 1: _t->openDocument(); break;
        case 2: _t->importImage(); break;
        case 3: _t->saveDocument(); break;
        case 4: _t->newDocument(); break;
        case 5: _t->undo(); break;
        case 6: _t->redo(); break;
        case 7: _t->deleteActive(); break;
        case 8: _t->selectAll(); break;
        case 9: _t->selectNone(); break;
        case 10: _t->selectInvert(); break;
        case 11: _t->selectionGrow(); break;
        case 12: _t->selectionShrink(); break;
        case 13: _t->selectionHarden(); break;
        case 14: _t->canvasBackCheck(); break;
        case 15: _t->canvasWhite(); break;
        case 16: _t->canvasBlack(); break;
        case 17: _t->canvasPink(); break;
        case 18: _t->canvasGreen(); break;
        case 19: _t->layerMoveUp(); break;
        case 20: _t->layerMoveDown(); break;
        case 21: _t->layerToggle(); break;
        case 22: _t->layerRemove(); break;
        case 23: _t->modifyGBlur(); break;
        case 24: _t->about(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject eoMenuBar::staticMetaObject = {
    { &QMenuBar::staticMetaObject, qt_meta_stringdata_eoMenuBar.data,
      qt_meta_data_eoMenuBar,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *eoMenuBar::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *eoMenuBar::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_eoMenuBar.stringdata0))
        return static_cast<void*>(this);
    return QMenuBar::qt_metacast(_clname);
}

int eoMenuBar::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMenuBar::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 25)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 25;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 25)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 25;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
