/****************************************************************************
** Meta object code from reading C++ file 'eoWidgetLayers.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/ui/eoWidgetLayers.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'eoWidgetLayers.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_eoWidgetLayers_t {
    QByteArrayData data[15];
    char stringdata0[182];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_eoWidgetLayers_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_eoWidgetLayers_t qt_meta_stringdata_eoWidgetLayers = {
    {
QT_MOC_LITERAL(0, 0, 14), // "eoWidgetLayers"
QT_MOC_LITERAL(1, 15, 18), // "onSelectionChanged"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 14), // "QItemSelection"
QT_MOC_LITERAL(4, 50, 8), // "selected"
QT_MOC_LITERAL(5, 59, 10), // "deselected"
QT_MOC_LITERAL(6, 70, 18), // "removeCurrentLayer"
QT_MOC_LITERAL(7, 89, 11), // "addNewLayer"
QT_MOC_LITERAL(8, 101, 11), // "moveLayerUp"
QT_MOC_LITERAL(9, 113, 13), // "moveLayerDown"
QT_MOC_LITERAL(10, 127, 11), // "treeClicked"
QT_MOC_LITERAL(11, 139, 5), // "index"
QT_MOC_LITERAL(12, 145, 15), // "changedBlending"
QT_MOC_LITERAL(13, 161, 14), // "changedOpacity"
QT_MOC_LITERAL(14, 176, 5) // "value"

    },
    "eoWidgetLayers\0onSelectionChanged\0\0"
    "QItemSelection\0selected\0deselected\0"
    "removeCurrentLayer\0addNewLayer\0"
    "moveLayerUp\0moveLayerDown\0treeClicked\0"
    "index\0changedBlending\0changedOpacity\0"
    "value"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_eoWidgetLayers[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    2,   54,    2, 0x08 /* Private */,
       6,    0,   59,    2, 0x08 /* Private */,
       7,    0,   60,    2, 0x08 /* Private */,
       8,    0,   61,    2, 0x08 /* Private */,
       9,    0,   62,    2, 0x08 /* Private */,
      10,    1,   63,    2, 0x08 /* Private */,
      12,    1,   66,    2, 0x08 /* Private */,
      13,    1,   69,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 3,    4,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,   11,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Int,   14,

       0        // eod
};

void eoWidgetLayers::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        eoWidgetLayers *_t = static_cast<eoWidgetLayers *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onSelectionChanged((*reinterpret_cast< const QItemSelection(*)>(_a[1])),(*reinterpret_cast< const QItemSelection(*)>(_a[2]))); break;
        case 1: _t->removeCurrentLayer(); break;
        case 2: _t->addNewLayer(); break;
        case 3: _t->moveLayerUp(); break;
        case 4: _t->moveLayerDown(); break;
        case 5: _t->treeClicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 6: _t->changedBlending((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->changedOpacity((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QItemSelection >(); break;
            }
            break;
        }
    }
}

const QMetaObject eoWidgetLayers::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_eoWidgetLayers.data,
      qt_meta_data_eoWidgetLayers,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *eoWidgetLayers::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *eoWidgetLayers::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_eoWidgetLayers.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int eoWidgetLayers::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
