# eoImagine Document File Format

## Signature

3 Byte signature. Characters: "EOI"

## Canvas Header

+ **UINT32** Width
+ **UINT32** Height
+ **ENUM32** Pixel Format (ex. RGBA, ARGB, etc.)

## Layer Header

+ Layer Signature: **"L"**
+ **INT32** Attributes
+ **NULL TERMINATED STRING** Name

## Layer Specific Data

**Raster Layer** - Raw Image Data

+ **FLOAT32** Width
+ **FLOAT32** Height
+ **DATA** Raw Image Data

### Modifiers




