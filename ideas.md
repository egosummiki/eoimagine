# This file contains ideas for eoImaginé.

## Targets (Layer Types)

+ **Raster Layer** - A bitmap. Exists after importing an image, or loading baking other layer.
+ **Shape** - Any vector shape.
+ **Text** - Text.
+ **Masks** - Single channel bitmap masking prevoius layer.

## Layer Modifier

### Vertex Transform

+ **Identity** (Auto) - Default transform
+ **Translate**
+ **Scale**
+ **Rotate**

### Fills

+ **Copy Source** (Auto) - If no other fill component present, will fill the image the pixels under it.
+ **Color** - Fills the target with the color parameter.
+ **Gradient** - Fill the target with gradient.
+ **Texture** - Fill the target with texture.

### Blendings

+ **Normal** (Auto) - Normal blending with opacity param.
+ **Overlay**, **Multiply**, **Screen**, etc.

### Fragment Shader

+ **Color Equation** - Performs a color equation on the target given in the parmeter.
+ **Blur** - Performs blur on the target.

### Effect

+ **Drop Shadow** - Renders drop shadow.
