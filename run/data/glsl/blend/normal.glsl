#version 330 core

in vec2 Texcoord;
out vec4 color;

uniform sampler2D   tex;
uniform sampler2D   source_tex;
uniform float       opacity;

void main()
{
    vec4 destination = texture(tex, Texcoord);
    vec4 source = texture(source_tex, Texcoord);

    float target_opacity = opacity * destination.a;

    color = vec4(destination.rgb*(target_opacity) + source.rgb*(1-target_opacity), clamp(target_opacity + source.a, 0, 1));
}
