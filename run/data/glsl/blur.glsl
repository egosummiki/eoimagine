#version 330

in      vec2        Texcoord;
out     vec4        color;
uniform sampler2D   tex;
uniform float       amount;
uniform vec2        canvas;


void main() {

    vec2 canvasCoord = Texcoord*canvas;
    vec4 resultColor = vec4(0,0,0,0);
    float sumDiv = 0;

    for(float y = canvasCoord.y - amount; y < canvasCoord.y + amount; y++) {
        for(float x = canvasCoord.x - amount; x < canvasCoord.x + amount; x++) {

            float dist = pow(clamp(1 - (distance(vec2(x,y), canvasCoord) / amount), 0, 1), 2);
            resultColor += dist*texture(tex, vec2(x,y)/canvas);
            sumDiv += dist;
        }
    }

    color = resultColor / sumDiv;
}
