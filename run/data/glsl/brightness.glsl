#version 330

in      vec2        Texcoord;
out     vec4        color;
uniform sampler2D   tex;
uniform float       amount;


void main() {
    vec4 colorIn = texture(tex, Texcoord);
    color = vec4(clamp(colorIn.rgb + amount/255.0, 0, 1), colorIn.w);
}
