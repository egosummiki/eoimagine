#version 330 core

in vec2 Texcoord;
out vec4 color;

uniform sampler2D tex;
uniform vec2 dim;
uniform int variant;


void main()
{
    switch(variant) {
        
        case 1:
            color = vec4(1.0, 1.0, 1.0, 1.0);
        return;
        case 2:
            color = vec4(0.0, 0.0, 0.0, 1.0);
        return;
        case 3:
            color = vec4(1.0, 0.0, 1.0, 1.0);
        return;
        case 4:
            color = vec4(0.0, 1.0, 0.0, 1.0);
        return;

        default: break;
    
    }

    vec2 checkerMat = Texcoord * dim / vec2(20.0, 20.0);

    if( mod(floor(checkerMat.x) + floor(checkerMat.y), 2) == 0) {

        color = vec4(1, 0.95, 0.95, 1);

    } else {

        color = vec4(0.6, 0.6, 1, 1);

    }

}
