#version 330 core

in vec2 Texcoord;
out vec4 color;
uniform float hue;

void main()
{
    float h,s,v;
    
    h = hue;
    s = Texcoord.x;
    v = Texcoord.y;

    float c = v * s;
    float x = c * (1 - abs(mod(h/60, 2) - 1));
    float m = v - c;

    if(h < 60)
        color = vec4(c+m, x+m, m, 1.0);
    else if(h < 120)
        color = vec4(x+m, c+m, m, 1.0);
    else if(h < 180)
        color = vec4(m, c+m, x+m, 1.0);
    else if(h < 240)
        color = vec4(m, x+m, c+m, 1.0);
    else if(h < 300)
        color = vec4(x+m, m, c+m, 1.0);
    else
        color = vec4(c+m, m, x+m, 1.0);

}
