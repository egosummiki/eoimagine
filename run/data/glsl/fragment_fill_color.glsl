#version 330 core

in vec2 Texcoord;
out vec4 color;
uniform vec4 fill_color;
uniform sampler2D tex;
uniform sampler2D selection;

void main()
{
    color           = texture(tex, Texcoord);
    vec4 color_sel  = texture(selection, Texcoord);

    color = fill_color * color_sel.r + color * (1 - color_sel.r);
}
