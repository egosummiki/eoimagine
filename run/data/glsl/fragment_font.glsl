#version 330 core

in vec2 Texcoord;
out vec4 color;
uniform sampler2D tex;
uniform vec4 textColor;

void main()
{
    color = vec4(textColor.rgb, texture(tex, Texcoord).r);
}
