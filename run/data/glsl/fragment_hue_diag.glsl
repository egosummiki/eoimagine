#version 330 core

in vec2 Texcoord;
out vec4 color;

void main()
{
    float h,s,l;
    
    h = Texcoord.y * 6;
    s = 1;
    l = 0.5;

    float x = 1 - abs(mod(h, 2) - 1);

    if(h < 1)
        color = vec4(1, x, 0, 1.0);
    else if(h < 2)
        color = vec4(x, 1, 0, 1.0);
    else if(h < 3)
        color = vec4(0, 1, x, 1.0);
    else if(h < 4)
        color = vec4(0, x, 1, 1.0);
    else if(h < 5)
        color = vec4(x, 0, 1, 1.0);
    else
        color = vec4(1, 0, x, 1.0);

}
