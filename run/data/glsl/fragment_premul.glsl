#version 330 core

in vec2 Texcoord;
out vec4 color;
uniform sampler2D tex;

void main()
{
    color = texture(tex, Texcoord);
    color = vec4(color.rgb * color.a, color.a);
}
