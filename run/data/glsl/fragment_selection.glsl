#version 330 core

in vec2 Texcoord;
out vec4 color;
uniform sampler2D tex;
uniform float blink;
uniform float zoom;
uniform vec2 canvasMiddle;

void main() {

    color = texture(tex, Texcoord);

    if(color.r == 0) {

        vec2 canvasCoord = Texcoord * canvasMiddle;
        float sum = 0.0;

        for(int y = -2; y < 3; y++) {
            for(int x = -2; x < 3; x++) {

                if(x == 0 && y == 0)
                    continue;

                if(texture(tex, (canvasCoord + (1/zoom)*vec2(x, y))/canvasMiddle).r != 0) {
                            
                    if( mod( floor(canvasCoord.x / 10 * zoom + blink) + floor(canvasCoord.y / 10 * zoom + blink), 2) == 0 ) {

                        color = vec4(9.0f, 9.0f, 1.0f, 0.9f);
                    } else {

                        color = vec4(0.1f, 0.1f, 0.1f, 0.6f);
                    }
                    
                    return;
                }
            }
        }
    }

    color = vec4(0.0f, 0.0f, 0.0f, 0.0f);
}
