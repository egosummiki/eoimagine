#version 330 core

out vec4 color;
uniform float strength;

void main()
{
    color = vec4(strength, strength, strength, 1.0f);
}
