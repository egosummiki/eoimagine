#version 330

in      vec2        Texcoord;
out     vec4        color;
uniform sampler2D   tex;
uniform float       deviation;
uniform vec2        canvas;

#define TAU 6.283185
#define E   2.718282

float gauss(float x, float y) {

    return 1 / (TAU*deviation*deviation) * pow(E, - (x*x + y*y) / (2*deviation*deviation));
}

void main() {

    vec2 canvasCoord = Texcoord*canvas;
    color = vec4(0,0,0,0);

    float maxValue = ceil(3*deviation);

    for(float y = -maxValue; y <= maxValue; y++) {
        for(float x = -maxValue; x <= maxValue; x++) {

            color += gauss(x, y) * texture(tex, (canvasCoord + vec2(x,y))/canvas);
        }
    }
}
