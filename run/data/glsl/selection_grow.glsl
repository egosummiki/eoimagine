#version 330 core

in vec2 Texcoord;
out vec4 color;
uniform sampler2D tex;
uniform vec2 canvasMiddle;
uniform float radius;

void main()
{
    color = texture(tex, Texcoord);

    if(radius > 0.0) {
        if(color.r == 0.0) {
            
            vec2 canvasCoord = Texcoord * canvasMiddle;

            for(float y = -radius; y <= radius; y++) {
                for(float x = -radius; x <= radius; x++) {

                    vec2 pos = vec2(x, y);
                    
                    if(distance(pos, vec2(0.0, 0.0)) < radius) {

                        if(texture(tex, (canvasCoord + pos)/canvasMiddle).r != 0) {

                            color = vec4(1.0f, 1.0f, 1.0f, 1.0f);
                            return;
                        }
                    }

                }
            }

        }
    } else {

        if(color.r != 0.0) {
            
            vec2 canvasCoord = Texcoord * canvasMiddle;

            for(float y = radius; y <= -radius; y++) {
                for(float x = radius; x <= -radius; x++) {

                    vec2 pos = vec2(x, y);
                    
                    if(distance(pos, vec2(0.0, 0.0)) < -radius) {

                        if(texture(tex, (canvasCoord + pos)/canvasMiddle).r == 0) {

                            color = vec4(0.0f, 0.0f, 0.0f, 1.0f);
                            return;
                        }
                    }

                }
            }

        }
    }

}
