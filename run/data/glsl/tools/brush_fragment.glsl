#version 330

in vec2 Texcoord;
out vec4 color;

uniform sampler2D   backTexture;

void main() {
    vec3 brushColor = vec3(0, 0, 0);
    color = vec4(brushColor, 1 - distance(Texcoord.x, 0.5)*2);
}
