#version 330

in vec4 Texcoord;
out vec4 color;

uniform sampler2D   backTexture;
uniform sampler2D   selection;
uniform vec2        canvasMiddle;
uniform vec4        targetColor;
uniform float       hardness;

float brushFunc(vec2 pos) {

    float radius = distance(pos, vec2(0.5, 0.5));

    if(radius > 0.5)
        return 0.0;

    return 1 - pow(2*radius, hardness);
} 

void main() {

    vec4 blend = texture(backTexture, Texcoord.zw / canvasMiddle / 2 + vec2(0.5, 0.5));
    vec4 sele = texture(selection, Texcoord.zw / canvasMiddle / 2 + vec2(0.5, 0.5));
    float output_alpha = targetColor.a * brushFunc(Texcoord.xy) * sele.r;
    
    //color = vec4((1 - blend.a*(1-targetColor.a))*(targetColor.rgb) + blend.a*(1-targetColor.a)*blend.a, targetColor.a*brushFunc(Texcoord.xy));
    //color = vec4(output_alpha * targetColor.rgb + (1 - output_alpha) * blend.rgb, output_alpha);
    
    color = vec4(targetColor.rgb * output_alpha, output_alpha);
}
