#version 150 core

layout(lines_adjacency) in;
layout(triangle_strip, max_vertices = 18) out;

out vec2 Texcoord;

//uniform mat3 transform;

//uniform float brush_size;

/*

point 0 -> point 0, 0', 0''
point 1 -> point 1, 1', 1''


*/

#define PI 3.14159265f
uniform vec2 canvasMiddle; // vec2(400.0, 300.0)

vec4 place(vec2 pos)
{
    mat3 transform = mat3 (

    1,0,0,
    0,1,0,
    0,0,1

    );

    return vec4(transform * vec3(pos, 1) / vec3(canvasMiddle, 1), 1);
}

void emit(vec2 a, vec2 at, vec2 b, vec2 bt, vec2 c, vec2 ct)
{
        gl_Position = place(a); Texcoord = at; EmitVertex();
        gl_Position = place(b); Texcoord = bt; EmitVertex();
        gl_Position = place(c); Texcoord = ct; EmitVertex();

        EndPrimitive();
}

void main()
{
        float brush_size = 20.0;

        vec2 point_ap = gl_in[0].gl_Position.xy;
        vec2 point_0  = gl_in[1].gl_Position.xy;
        vec2 point_1  = gl_in[2].gl_Position.xy;
        vec2 point_an = gl_in[3].gl_Position.xy;

        vec2 point_np0 = point_ap - point_0;
        vec2 point_n01 = point_0 - point_1;
        vec2 point_s0 = normalize(point_np0 + point_n01);
        vec2 point_s0_ccw = point_0 + brush_size*vec2(-point_s0.y, point_s0.x);
        vec2 point_s0_cw = point_0 + brush_size*vec2(point_s0.y, -point_s0.x);

        vec2 point_nn1 = point_an - point_1;
        vec2 point_s1 = normalize(point_nn1 - point_n01);
        vec2 point_s1_ccw = point_1 + brush_size*vec2(-point_s1.y, point_s1.x);
        vec2 point_s1_cw = point_1 + brush_size*vec2(point_s1.y, -point_s1.x);

        emit(
          point_s0_ccw, vec2(1,0),
          point_s0_cw, vec2(0,0),
          point_s1_cw, vec2(1,0)
        );

        emit(
          point_s0_cw, vec2(0,0),
          point_s1_ccw, vec2(0,0),
          point_s1_cw, vec2(1,0)
        );
}
