/*
 * * eoImaginé - Brush Geometry shader *
 *
 * Purpose:
 *
 *  Change each two points points
 *  into array of squares of given size
 *  seperated by given offset.
 *
 * Parameters:
 *
 *  canvasMiddle - Half size of the canvas.
 *  size - Brush size. Dimension of a square.
 *  seperation - Space between each square.
 *
 *  author:     Mikołaj Bednarek
 *  license:    GPL v3
 * */

#version 330 core

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

/* Pi definition */
#define PI 3.14159265f

/* UV texture output */
out vec4 Texcoord;

/* Uniforms */
uniform vec2 canvasMiddle;
uniform float brushSize;

/* Pixel to screen space */
vec4
pixel_to_screen(vec2 pixel) {
    
    return vec4(vec3(pixel, 1) / vec3(canvasMiddle, 1), 1);
}

/* Emit a single vertex */
void
emit_vertex(vec2 point, vec4 uv) {

    gl_Position = pixel_to_screen(point);
    Texcoord = uv;
    EmitVertex();
}

/* Emit square function */
void
emit_square(vec2 top_left, vec2 bottom_right) {

    /* TL TR BL, BL TR BR */

    // Top left
    emit_vertex(top_left, vec4(0, 1, top_left));

    // Top right
    emit_vertex(vec2(bottom_right.x, top_left.y), vec4(1, 1, vec2(bottom_right.x, top_left.y)));

    // Bottom left
    emit_vertex(vec2(top_left.x, bottom_right.y), vec4(0, 0, vec2(top_left.x, bottom_right.y)));

    // Bottom right
    emit_vertex(bottom_right, vec4(1, 0, bottom_right));

    EndPrimitive();

}

/* Main function */
void
main() {

    vec2 midSquare = vec2(brushSize, brushSize);
    vec2 pos = gl_in[0].gl_Position.xy;

    emit_square(pos - midSquare, pos + midSquare);
}
