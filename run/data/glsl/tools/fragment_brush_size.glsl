#version 330 core

in vec2 Texcoord;
out vec4 color;
uniform float radius;
uniform int variant;

void main()
{
    float discen = distance(Texcoord, vec2(0.5, 0.5));
    
    if(2*discen*radius < radius-1) {

        if(variant == 1) {
            color = vec4(0.4, 0.0, 0.0, 0.4);
        } else {
            color = vec4(0.0, 0.0, 0.0, 0.0);
        }
    } else if(discen <= 0.5) {

        if(variant == 1) {
            color = vec4(1.0, 1.0, 1.0, 0.95);
        } else {
            color = vec4(0.9, 0.9, 0.9, 0.8);
        }
    } else {

        color = vec4(0.0, 0.0, 0.0, 0.0);
    }
}
