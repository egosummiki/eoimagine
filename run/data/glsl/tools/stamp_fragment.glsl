#version 330 core

in vec2 Texcoord;
out vec4 color;

uniform sampler2D tex;
uniform vec2 pointer_pos;
uniform vec2 stamp_offset;
uniform vec2 canvas_size;
uniform float brush_size;
uniform float brush_hardness;

void main()
{
    color = mix(    texture(tex, clamp(Texcoord + stamp_offset, 0, 1)),
                    texture(tex, Texcoord),
                    pow(    clamp(distance( pointer_pos, Texcoord)/brush_size, 0, 1),
                            brush_hardness  ));
}