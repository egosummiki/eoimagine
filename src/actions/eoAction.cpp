//
// Created by ego on 11/29/17.
//

#include "eoAction.h"

eoAction::eoAction(const char* name) : name(name) {

}

void eoAction::perform() {
    printf("Action %s performed\n", name.c_str());
}

void eoAction::neglect() {
    printf("Action neglected\n");
}

const std::string& eoAction::getName() {
    
    return name;
}

void eoAction::freeWhenOld() {
    
}

void eoAction::freeWhenAbandoned() {
    
}
