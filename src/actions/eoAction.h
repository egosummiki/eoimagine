//
// Created by ego on 11/29/17.
//

#ifndef GFX_PROTOTYPE_EOACTION_H
#define GFX_PROTOTYPE_EOACTION_H

#include <string>

class eoAction {
    std::string name;

public:
    eoAction(const char* name);
    virtual void perform();
    virtual void neglect();
    virtual void freeWhenOld();
    virtual void freeWhenAbandoned();

    const std::string& getName();
};


#endif //GFX_PROTOTYPE_EOACTION_H
