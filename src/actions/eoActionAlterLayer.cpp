//
// Created by ego on 26/09/18.
//
#include "eoActionAlterLayer.h"

eoActionAlterLayer::eoActionAlterLayer(char* name, eoLayerRaster* rasterLayer, eoFrameBuffer* preAlter, eoFrameBuffer* postAlter)
    : eoAction(name), rasterLayer(rasterLayer), preAlter(preAlter), postAlter(postAlter)
{
}

void eoActionAlterLayer::perform() {

    rasterLayer->setFrontBuffer(postAlter);
}

void eoActionAlterLayer::neglect() {

    rasterLayer->setFrontBuffer(preAlter);
}

void eoActionAlterLayer::freeWhenOld() {
    
    delete preAlter;
}

void eoActionAlterLayer::updatePostAlter(eoFrameBuffer* buffer) {
    
    postAlter = buffer;
}
