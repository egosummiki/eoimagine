#pragma once
//
// Created by ego on 26/09/18.
//

#include "eoAction.h"
#include "../eoCanvas.h"
#include "../eoLayerRaster.h"

class eoActionAlterLayer : public eoAction {

    eoLayerRaster* rasterLayer;
    eoFrameBuffer* preAlter;
    eoFrameBuffer* postAlter;

public:
    eoActionAlterLayer(char* name, eoLayerRaster* rasterLayer, eoFrameBuffer* preAlter, eoFrameBuffer* postAlter);

    void perform() override;
    void neglect() override;
    void freeWhenOld() override;
    void updatePostAlter(eoFrameBuffer* buffer);
};
