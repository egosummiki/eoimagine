//
// Created by ego on 28/08/18.
//

#include "eoActionAlterModuleFloat.h"
#include "../eoLayer.h"

eoActionAlterModuleFloat::eoActionAlterModuleFloat
(const char* name, eoCanvas* canvas, eoModifierFill* modifier, std::string property, float value, int layerID)
    : eoAction(name), canvas(canvas), modifier(modifier), property(std::move(property)), value(value), previousValue(0.0f), layerID(layerID)
{
}

void eoActionAlterModuleFloat::perform() {
    
    canvas->performOnRender([this] (eoCanvas* canvas) {

        auto module = canvas->getLayer(layerID)->getFillModule(modifier);
        previousValue = module->getFloat(property);
        module->setArgument(property, value);
        canvas->getLayer(layerID)->updateFill();
    });
}

void eoActionAlterModuleFloat::neglect() {
    
    canvas->performOnRender([this] (eoCanvas* canvas) {

        auto module = canvas->getLayer(layerID)->getFillModule(modifier);
        module->setArgument(property, previousValue);
        canvas->getLayer(layerID)->updateFill();
    });
}
