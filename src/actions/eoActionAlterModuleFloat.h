#pragma once
//
// Created by ego on 28/08/18.
//

#include "eoAction.h"
#include "../eoCanvas.h"
#include "../modifiers/eoModifierFill.h"

class eoActionAlterModuleFloat : public eoAction {

private:
    eoCanvas* canvas;

    int layerID;

    eoModifierFill* modifier;
    std::string property;

    float value;
    float previousValue;

public:
    eoActionAlterModuleFloat(const char* name, eoCanvas* canvas, eoModifierFill* modifier, std::string property, float value, int layerID);

    void perform() override;
    void neglect() override;

};
