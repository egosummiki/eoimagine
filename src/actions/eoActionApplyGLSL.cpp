//
// Created by ego on 7/31/18.
//

#include "eoActionApplyGLSL.h"
#include "../eoLayer.h"
#include "../eoLayerRaster.h"

eoActionApplyGLSL::eoActionApplyGLSL(char *name, eoCanvas *canvas, eoGLProgram *program)
    : eoAction(name), canvas(canvas), program(program)
{

    buffer = eoVertexBuffer::square(canvas->getWidth() / 2.0f, canvas->getHeight() / 2.0f);
}

void eoActionApplyGLSL::perform() {
    if(canvas->getCurrentLayer()->hasAttribute(eoLayer::RASTER)) {
        auto currentLayer = (eoLayerRaster*) canvas->getCurrentLayer();

        currentLayer->alterLayerOnce([this] (GLuint previousTexture) {

            program->use();
            program->setTransform(eoMatrix());
            callback(canvas, program);
            glBindTexture(GL_TEXTURE_2D, previousTexture);
            buffer->render();

        });
    }
}

void eoActionApplyGLSL::neglect() {
    eoAction::neglect();
}

void eoActionApplyGLSL::onProgramUsed(std::function<void(eoCanvas *, eoGLProgram *)> _callback) {

    callback = std::move(_callback);
}
