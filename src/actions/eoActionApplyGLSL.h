//
// Created by ego on 7/31/18.
//

#ifndef GFX_PROTOTYPE_EOACTIONAPPLYGLSL_H
#define GFX_PROTOTYPE_EOACTIONAPPLYGLSL_H

#include "eoAction.h"
#include "../eoGLProgram.h"
#include "../eoCanvas.h"
#include "../eoVertexBuffer.h"

#include <functional>

class eoActionApplyGLSL : public eoAction {

    eoCanvas*   canvas;
    eoGLProgram*    program;

    eoVertexBuffer*     buffer;

    std::function<void(eoCanvas* canvas, eoGLProgram*)> callback;

public:
    eoActionApplyGLSL(char* name, eoCanvas* canvas, eoGLProgram* program);

    void onProgramUsed(std::function<void(eoCanvas *, eoGLProgram *)> _callback);

    void perform() override;
    void neglect() override;
};


#endif //GFX_PROTOTYPE_EOACTIONAPPLYGLSL_H
