//
// Created by ego on 11/30/17.
//

#include "eoActionCallbacks.h"

eoActionCallbacks::eoActionCallbacks(const char *name, std::function<void()> perform_callback,
                                     std::function<void()> neglect_callback)
        : eoAction(name), perform_callback(std::move(perform_callback)), neglect_callback(std::move(neglect_callback)) {}

void eoActionCallbacks::perform() {
    perform_callback();
}

void eoActionCallbacks::neglect() {
    neglect_callback();
}
