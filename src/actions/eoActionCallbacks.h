//
// Created by ego on 11/30/17.
//

#ifndef GFX_PROTOTYPE_EOACTIONCALLBACKS_H
#define GFX_PROTOTYPE_EOACTIONCALLBACKS_H

#include "eoAction.h"
#include <functional>

class eoActionCallbacks : eoAction {
    std::function<void(void)> perform_callback;
    std::function<void(void)> neglect_callback;

public:
    eoActionCallbacks(const char *name, std::function<void()> perform_callback,
                          std::function<void()> neglect_callback);

    void perform() override;
    void neglect() override;

};


#endif //GFX_PROTOTYPE_EOACTIONCALLBACKS_H
