//
// Created by ego on 08/10/18.
//
#include "eoActionFillColor.h"
#include "../eoCanvas.h"
#include "../eoLayerRaster.h"
#include "../shaders/eoShaderFile.h"
#include "../eoVertexBuffer.h"

eoActionFillColor::eoActionFillColor(char* name, eoCanvas *canvas, eoColor fillColor)
    :   eoAction(name),
        canvas(canvas),
        program(new eoGLProgram()),
        fillColor(fillColor)
{

    auto fragment = new eoShaderFile(GL_FRAGMENT_SHADER, "data/glsl/fragment_fill_color.glsl");

    program->attachShader(eoShader::VERTEX_TRIVIAL);
    program->attachShader(fragment);
    program->link();

    delete fragment;

}

void eoActionFillColor::perform() {

    if(!canvas)
        return;

    auto layer = canvas->getCurrentLayer();

    if(!layer || !layer->hasAttribute(eoLayer::RASTER))
        return;
    
    auto rasterLayer = static_cast<eoLayerRaster*>(layer);

    rasterLayer->alterLayerOnce([this] (int previousTexture) {

        glDisable(GL_BLEND);
        
        program->use();
        program->setTransform(eoMatrix());
        program->setTexture("tex", 0);
        program->setTexture("selection", 1);
        program->setColor("fill_color", fillColor);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, previousTexture);
        canvas->getSelectionBuffer()->bind(GL_TEXTURE1);
        eoVertexBuffer::FULL_DIMENSION->render();
            
    });

}

void eoActionFillColor::neglect() {
}
