#pragma once
//
// Created by ego on 08/10/18.
//

#include "eoAction.h"
#include "../eoCanvas.h"
#include "../eoColor.h"

class eoActionFillColor : public eoAction {

    eoCanvas*       canvas;
    eoGLProgram*    program = nullptr;
    eoColor         fillColor;

public:
    eoActionFillColor(char* name, eoCanvas* canvas, eoColor fillColor);

    void perform() override;
    void neglect() override;
};
