//
// Created by ego on 19/09/18.
//
#include "eoActionLoadImage.h"
#include "../eoLayerRaster.h"
#include "../eoTexture.h"

eoActionLoadImage::eoActionLoadImage(eoCanvas *canvas, const char* _fileName)
    : eoAction("Load Image Document"), canvas(canvas), fileName(new char[strlen(_fileName)+1])
{
    strcpy(fileName, _fileName);
}

eoActionLoadImage::~eoActionLoadImage() {
    
}

void eoActionLoadImage::perform() {

    canvas->performOnRender([this] (eoCanvas* canvas) {
        auto texture = eoTexture::load(fileName);
        layer = new eoLayerRaster(canvas, QString(basename(fileName)), texture);
        canvas->addLayer(layer);
        delete texture;
    });
}

void eoActionLoadImage::neglect() {

    if(!layer)
        return;

    layer->remove();
}
