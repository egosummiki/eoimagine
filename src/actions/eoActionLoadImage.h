#pragma once
//
// Created by ego on 19/09/18.
//

#include "eoAction.h"
#include "../eoCanvas.h"
#include <string>


class eoActionLoadImage : public eoAction {

    eoCanvas*   canvas;
    char* fileName;
    eoLayer* layer = nullptr;

public:
    eoActionLoadImage(eoCanvas* canvas, const char* _fileName);
    ~eoActionLoadImage();

    void perform() override;
    void neglect() override;
};
