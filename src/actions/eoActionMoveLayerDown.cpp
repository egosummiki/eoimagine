//
// Created by ego on 19/10/18.
//
#include "eoActionMoveLayerDown.h"

eoActionMoveLayerDown::eoActionMoveLayerDown(eoCanvas *canvas)
    : eoAction("Move current layer down"), canvas(canvas)
{
}

void eoActionMoveLayerDown::perform() {

    canvas->moveCurrentLayerDown();
}

void eoActionMoveLayerDown::neglect() {

    canvas->moveCurrentLayerUp();
}
