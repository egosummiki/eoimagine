#pragma once
//
// Created by ego on 19/10/18.
//

#include "eoAction.h"
#include "../eoCanvas.h"

class eoActionMoveLayerDown : public eoAction {

    eoCanvas*   canvas;

public:
    eoActionMoveLayerDown(eoCanvas* canvas);

    void perform() override;
    void neglect() override;
};
