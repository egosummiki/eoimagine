//
// Created by ego on 19/10/18.
//
#include "eoActionMoveLayerUp.h"

eoActionMoveLayerUp::eoActionMoveLayerUp(eoCanvas *canvas)
    : eoAction("Move current layer up"), canvas(canvas)
{
}

void eoActionMoveLayerUp::perform() {

    canvas->moveCurrentLayerUp();
}

void eoActionMoveLayerUp::neglect() {

    canvas->moveCurrentLayerDown();
}
