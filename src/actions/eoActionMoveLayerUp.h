#pragma once
//
// Created by ego on 19/10/18.
//

#include "eoAction.h"
#include "../eoCanvas.h"

class eoActionMoveLayerUp : public eoAction {

    eoCanvas*   canvas;

public:
    eoActionMoveLayerUp(eoCanvas* canvas);

    void perform() override;
    void neglect() override;
};
