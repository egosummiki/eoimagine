//
// Created by ego on 18/09/18.
//

#include "eoActionRotate.h"
#include "../eoLayer.h"

eoActionRotate::eoActionRotate(eoCanvas *canvas, float rotation, float saved)
    : eoAction("Rotation"), canvas(canvas), rotation(rotation), saved(saved)
{
}

void alterLayerRotation(eoLayer* layer, float rotation) {
    layer->getRotateModule()->setArgument("rotation", rotation);
    layer->updateTransform();
}

void eoActionRotate::perform() {
    alterLayerRotation(canvas->getCurrentLayer(), rotation);
}

void eoActionRotate::neglect() {
    alterLayerRotation(canvas->getCurrentLayer(), saved);
}
