#pragma once
//
// Created by ego on 18/09/18.
//

#include "eoAction.h"
#include "../eoCanvas.h"

class eoActionRotate : public eoAction {

    eoCanvas*   canvas;
    float rotation, saved;

public:
    eoActionRotate(eoCanvas* canvas, float rotation, float saved);

    void perform() override;
    void neglect() override;
};
