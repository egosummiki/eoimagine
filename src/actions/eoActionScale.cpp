//
// Created by ego on 29/09/18.
//
#include "eoActionScale.h"
#include "../eoLayer.h"

eoActionScale::eoActionScale(eoCanvas* canvas, eoVector2 scale, eoVector2 saved)
    : eoAction("Scale"), canvas(canvas), scale(scale), saved(saved)
{
}

void alterLayerScale(eoLayer* layer, eoVector2 scale) {

    layer->getScaleModule()->setArgument("scale", scale);
    layer->updateTransform();
}


void eoActionScale::perform() {

    alterLayerScale(canvas->getCurrentLayer(), scale);
}

void eoActionScale::neglect() {

    alterLayerScale(canvas->getCurrentLayer(), saved);
}
