#pragma once
//
// Created by ego on 29/09/18.
//

#include "eoAction.h"
#include "../eoCanvas.h"
#include "../eoVector2.h"

class eoActionScale : public eoAction {

    eoCanvas*   canvas;
    eoVector2   scale, saved;

public:
    eoActionScale(eoCanvas* canvas, eoVector2 scale, eoVector2 saved);

    void perform() override;
    void neglect() override;
};
