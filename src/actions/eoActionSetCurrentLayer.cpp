//
// Created by ego on 19/10/18.
//
#include "eoActionSetCurrentLayer.h"

eoActionSetCurrentLayer::eoActionSetCurrentLayer(eoCanvas* canvas, eoLayer *layer)
    : eoAction("Active Layer Change"), canvas(canvas), layer(layer)
{
}

void eoActionSetCurrentLayer::perform() {

    layerSave = canvas->getCurrentLayer();
    canvas->setCurrentLayer(layer);
}

void eoActionSetCurrentLayer::neglect() {

    canvas->setCurrentLayer(layerSave);
}
