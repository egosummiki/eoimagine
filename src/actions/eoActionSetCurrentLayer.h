#pragma once
//
// Created by ego on 19/10/18.
//

#include "eoAction.h"
#include "../eoCanvas.h"
#include "../eoLayer.h"

class eoActionSetCurrentLayer : public eoAction {

    eoCanvas*   canvas;
    eoLayer*    layer;
    eoLayer*    layerSave = nullptr;

public:
    eoActionSetCurrentLayer(eoCanvas* canvas, eoLayer *layer);

    void perform() override;
    void neglect() override;
};
