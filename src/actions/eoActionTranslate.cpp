//
// Created by ego on 18/09/18.
//

#include "eoActionTranslate.h"
#include "../eoLayer.h"

eoActionTranslate::eoActionTranslate(eoCanvas* canvas, eoVector2 position, eoVector2 saved)
    : canvas(canvas), eoAction("Translation"), position(position), saved(saved)
{
}

void alterLayerTranslation(eoLayer* layer, eoVector2& translation) {

    layer->getTranslateModule()->setArgument("position", translation);
    layer->updateTransform();
}

void eoActionTranslate::perform() {

    alterLayerTranslation(canvas->getCurrentLayer(), position);
}

void eoActionTranslate::neglect() {

    alterLayerTranslation(canvas->getCurrentLayer(), saved);
}
