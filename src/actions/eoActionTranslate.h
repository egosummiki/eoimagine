#pragma once
//
// Created by ego on 18/09/18.
//

#include "eoAction.h"
#include "../eoCanvas.h"

class eoActionTranslate : public eoAction {

    eoCanvas*   canvas;
    eoVector2   position;
    eoVector2   saved;

public:
    eoActionTranslate(eoCanvas* canvas, eoVector2 position, eoVector2 saved);

    void perform() override;
    void neglect() override;
};
