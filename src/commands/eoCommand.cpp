//
// Created by ego on 7/30/18.
//

#include "eoCommand.h"

eoCommand::eoCommand(QString name)
    : name(name)
{

}

bool eoCommand::check(QString command) {
    return command.toLower().compare(name) == 0;
}

void eoCommand::execute(QString arguments) {

}
