//
// Created by ego on 7/30/18.
//

#ifndef GFX_PROTOTYPE_EOCOMMAND_H
#define GFX_PROTOTYPE_EOCOMMAND_H

#include <QString>

class eoCommand {

    QString name;

public:
    explicit eoCommand(QString name);
    bool check(QString command);
    virtual void execute(QString arguments);

};


#endif //GFX_PROTOTYPE_EOCOMMAND_H
