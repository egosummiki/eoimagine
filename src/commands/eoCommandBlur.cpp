//
// Created by ego on 8/9/18.
//

#include "eoCommandBlur.h"
#include "../shaders/eoShaderFile.h"
#include "../shaders/eoShaderCanvasDimension.h"
#include "../eoGLProgram.h"
#include "../actions/eoActionApplyGLSL.h"


eoCommandBlur::eoCommandBlur(eoCanvas *canvas)
    : eoCommand("blur"), canvas(canvas)
{

    auto fragment   = new eoShaderFile(GL_FRAGMENT_SHADER, "data/glsl/gblur.glsl");
    auto vertex     = new eoShaderCanvasDimension(canvas);

    program = new eoGLProgram();
    program->attachShader(fragment);
    program->attachShader(vertex);
    program->link();

    delete fragment;
    delete vertex;
}

void eoCommandBlur::execute(QString arguments) {
    auto action = new eoActionApplyGLSL(const_cast<char *>("Apply Blur"), canvas, program);
    action->onProgramUsed([arguments] (eoCanvas* canvas, eoGLProgram* program) {
        //program->setFloat("amount", std::stof(arguments.toStdString()));
        program->setFloat("deviation", std::stof(arguments.toStdString()));
        program->setVec2("canvas", canvas->getDimension());
    });
    canvas->getActionManager()->perform(action);
}

