//
// Created by ego on 8/9/18.
//

#ifndef GFX_PROTOTYPE_EOCOMMANDBLUR_H
#define GFX_PROTOTYPE_EOCOMMANDBLUR_H

#include "eoCommand.h"
#include "../eoCanvas.h"

class eoCommandBlur : public eoCommand {

private:

    eoCanvas* canvas;
    eoGLProgram* program;

public:
    eoCommandBlur(eoCanvas* canvas);

    void execute(QString arguments) override;
};


#endif //GFX_PROTOTYPE_EOCOMMANDBLUR_H
