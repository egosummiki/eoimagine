//
// Created by ego on 8/4/18.
//

#include "eoCommandBrightness.h"
#include "../actions/eoActionAlterModuleFloat.h"
#include "../eoLayer.h"

eoCommandBrightness::eoCommandBrightness(eoCanvas *canvas)
    : eoCommand("b"), canvas(canvas)
{
}

void eoCommandBrightness::execute(QString arguments) {

    canvas->getActionManager()->perform(
        new eoActionAlterModuleFloat(
            "Change Brightness",
            canvas,
            eoModifier::BRIGHTNESS,
            "brightness",
            std::stof(arguments.toStdString()),
            canvas->getCurrentLayer()->getId()
        )
    );
}

