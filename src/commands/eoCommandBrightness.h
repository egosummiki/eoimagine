//
// Created by ego on 8/4/18.
//

#ifndef GFX_PROTOTYPE_EOCOMMANDBRIGHTNESS_H
#define GFX_PROTOTYPE_EOCOMMANDBRIGHTNESS_H

#include "eoCommand.h"
#include "../eoCanvas.h"
#include "../eoGLProgram.h"

class eoCommandBrightness : public eoCommand {

    eoCanvas* canvas;

public:
    explicit eoCommandBrightness(eoCanvas* canvas);

    void execute(QString arguments) override;

};


#endif //GFX_PROTOTYPE_EOCOMMANDBRIGHTNESS_H
