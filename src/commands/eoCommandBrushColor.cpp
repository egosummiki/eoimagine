//
// Created by ego on 28/09/18.
//
#include "eoCommandBrushColor.h"
#include "../tools/eoToolBrush.h"
#include "../eoMessageBox.h"

eoCommandBrushColor::eoCommandBrushColor(eoCanvas *canvas)
    : eoCommand("bc"), canvas(canvas)
{
}

void eoCommandBrushColor::execute(QString arguments) {

    eoTool::t.BRUSH->setBrushColor(eoColor(arguments.toStdString()));
}
  
