#pragma once
//
// Created by ego on 28/09/18.
//

#include "eoCommand.h"
#include "../eoCanvas.h"

class eoCommandBrushColor : public eoCommand {

    eoCanvas* canvas;

public:
    explicit eoCommandBrushColor(eoCanvas* canvas);

    void execute(QString arguments) override;
};
