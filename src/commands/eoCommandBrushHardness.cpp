//
// Created by ego on 28/09/18.
//
#include "eoCommandBrushHardness.h"
#include "../tools/eoToolBrush.h"

eoCommandBrushHardness::eoCommandBrushHardness(eoCanvas *canvas)
    : eoCommand("bh"), canvas(canvas)
{
}

void eoCommandBrushHardness::execute(QString arguments) {

    eoTool::t.BRUSH->setBrushHardness(std::stof(arguments.toStdString()));
}

