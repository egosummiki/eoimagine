#pragma once
//
// Created by ego on 28/09/18.
//

#include "eoCommand.h"
#include "../eoCanvas.h"

class eoCommandBrushHardness : public eoCommand {

    eoCanvas* canvas;

public:
    explicit eoCommandBrushHardness(eoCanvas* canvas);

    void execute(QString arguments) override;
};
