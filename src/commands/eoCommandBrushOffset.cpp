//
// Created by ego on 28/09/18.
//
#include "eoCommandBrushOffset.h"
#include "../tools/eoToolBrush.h"

eoCommandBrushOffset::eoCommandBrushOffset(eoCanvas *canvas)
    : eoCommand("bo"), canvas(canvas)
{
}

void eoCommandBrushOffset::execute(QString arguments) {

    eoTool::t.BRUSH->setBrushOffset(std::stof(arguments.toStdString()));
}

