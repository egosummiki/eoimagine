//
// Created by ego on 28/09/18.
//
#include "eoCommandBrushSize.h"
#include "../tools/eoToolBrush.h"

eoCommandBrushSize::eoCommandBrushSize(eoCanvas *canvas)
    : eoCommand("bs"), canvas(canvas)
{
}

void eoCommandBrushSize::execute(QString arguments) {

    eoTool::t.BRUSH->setBrushSize(std::stof(arguments.toStdString()));
}

