#pragma once
//
// Created by ego on 28/09/18.
//

#include "eoCommand.h"
#include "../eoCanvas.h"

class eoCommandBrushSize : public eoCommand {

    eoCanvas* canvas;

public:
    explicit eoCommandBrushSize(eoCanvas* canvas);

    void execute(QString arguments) override;
};
