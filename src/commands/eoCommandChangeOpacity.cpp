//
// Created by ego on 03/10/18.
//
#include "eoCommandChangeOpacity.h"
#include "../eoLayerRaster.h"

eoCommandChangeOpacity::eoCommandChangeOpacity(eoCanvas *canvas)
    : eoCommand("op"), canvas(canvas)
{
}

void eoCommandChangeOpacity::execute(QString arguments) {

    auto layer = canvas->getCurrentLayer();

    if(layer && layer->hasAttribute(eoLayer::RASTER)) {
        
        auto rasterLayer = static_cast<eoLayerRaster*>(layer);
        rasterLayer->setOpacity(std::stof(arguments.toStdString()));
    }
}

