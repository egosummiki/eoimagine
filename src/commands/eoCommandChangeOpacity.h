#pragma once
//
// Created by ego on 03/10/18.
//

#include "eoCommand.h"
#include "../eoCanvas.h"

class eoCommandChangeOpacity : public eoCommand {

    eoCanvas* canvas;

public:
    explicit eoCommandChangeOpacity(eoCanvas* canvas);

    void execute(QString arguments) override;
};
