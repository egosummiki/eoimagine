//
// Created by ego on 8/22/18.
//

#include "eoCommandContrast.h"
#include "../actions/eoActionAlterModuleFloat.h"
#include "../eoLayer.h"

eoCommandContrast::eoCommandContrast(eoCanvas *canvas) : eoCommand("c"), canvas(canvas) {}

void eoCommandContrast::execute(QString arguments) {

    canvas->getActionManager()->perform(
        new eoActionAlterModuleFloat(
            "Change Contrast",
            canvas,
            eoModifier::CONTRAST,
            "contrast",
            std::stof(arguments.toStdString()),
            canvas->getCurrentLayer()->getId()
        )
    );
}
