//
// Created by ego on 8/22/18.
//

#ifndef EOIMAGINE_EOCOMMANDCONTRAST_H
#define EOIMAGINE_EOCOMMANDCONTRAST_H

#include "eoCommand.h"
#include "../eoCanvas.h"

class eoCommandContrast : public eoCommand {

    eoCanvas* canvas;

public:
    eoCommandContrast(eoCanvas *canvas);

private:
    void execute(QString arguments) override;
};


#endif //EOIMAGINE_EOCOMMANDCONTRAST_H
