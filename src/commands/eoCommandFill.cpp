//
// Created by ego on 08/10/18.
//
#include "eoCommandFill.h"
#include "../actions/eoActionFillColor.h"

eoCommandFill::eoCommandFill(eoCanvas *canvas)
    : eoCommand("fill"), canvas(canvas)
{
}

void eoCommandFill::execute(QString arguments) {

    if(canvas) {

        eoColor color(arguments.toStdString());

        canvas->performOnRender([color] (eoCanvas *canvas) {
            canvas->getActionManager()->perform(new eoActionFillColor("Fill color", canvas, color));
        });
    }
}

