#pragma once
//
// Created by ego on 08/10/18.
//

#include "eoCommand.h"
#include "../eoCanvas.h"

class eoCommandFill : public eoCommand {

    eoCanvas* canvas;

public:
    explicit eoCommandFill(eoCanvas* canvas);

    void execute(QString arguments) override;
};
