//
// Created by ego on 7/30/18.
//

#include "eoCommandMessage.h"
#include "../eoMessageBox.h"

eoCommandMessage::eoCommandMessage()
    : eoCommand(QString("message"))
{

}

void eoCommandMessage::execute(QString arguments) {

    eoMessageBox::show(arguments);

}

