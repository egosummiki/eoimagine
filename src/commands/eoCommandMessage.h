//
// Created by ego on 7/30/18.
//

#ifndef GFX_PROTOTYPE_EOCOMMANDMESSAGE_H
#define GFX_PROTOTYPE_EOCOMMANDMESSAGE_H

#include "eoCommand.h"

class eoCommandMessage : public eoCommand {

public:
    eoCommandMessage();

    void execute(QString arguments) override;

};


#endif //GFX_PROTOTYPE_EOCOMMANDMESSAGE_H
