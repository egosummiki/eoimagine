//
// Created by ego on 03/10/18.
//
#include "eoCommandSetBlending.h"
#include "../eoLayerRaster.h"
#include "../shaders/eoShaderModifiers.h"

eoCommandSetBlending::eoCommandSetBlending(eoCanvas *canvas)
    : eoCommand("blend"), canvas(canvas)
{
}

void setBlendingMode(eoCanvas* canvas, eoShaderModifiers::BlendingMode mode) {

    canvas->performOnRender([mode] (eoCanvas* canvas) {

        auto layer = canvas->getCurrentLayer();

        if(layer && layer->hasAttribute(eoLayer::RASTER)) {
            
            auto rasterLayer = static_cast<eoLayerRaster*>(layer);
            rasterLayer->setBlendingMode(mode);
        }
    });
}

void eoCommandSetBlending::execute(QString arguments) {

    eoShaderModifiers::BlendingMode mode;

    if(arguments.compare("darken") == 0)
        mode = eoShaderModifiers::DARKEN;
    else if(arguments.compare("mult") == 0)
        mode = eoShaderModifiers::MULTIPLY;
    else if(arguments.compare("burn") == 0)
        mode = eoShaderModifiers::COLOR_BURN;
    else if(arguments.compare("linburn") == 0)
        mode = eoShaderModifiers::LINEAR_BURN;
    else if(arguments.compare("light") == 0)
        mode = eoShaderModifiers::LIGHTEN;
    else if(arguments.compare("screen") == 0)
        mode = eoShaderModifiers::SCREEN;
    else if(arguments.compare("dodge") == 0)
        mode = eoShaderModifiers::COLOR_DODGE;
    else if(arguments.compare("lindodge") == 0)
        mode = eoShaderModifiers::LINEAR_DODGE;
    else if(arguments.compare("overlay") == 0)
        mode = eoShaderModifiers::OVERLAY;
    else if(arguments.compare("softlight") == 0)
        mode = eoShaderModifiers::SOFT_LIGHT;
    else if(arguments.compare("hardlight") == 0)
        mode = eoShaderModifiers::HARD_LIGHT;
    else if(arguments.compare("vividlight") == 0)
        mode = eoShaderModifiers::HARD_LIGHT;
    else if(arguments.compare("linlight") == 0)
        mode = eoShaderModifiers::LINEAR_LIGHT;
    else if(arguments.compare("pinlight") == 0)
        mode = eoShaderModifiers::PIN_LIGHT;
    else if(arguments.compare("diff") == 0)
        mode = eoShaderModifiers::DIFFERENCE;
    else if(arguments.compare("excl") == 0)
        mode = eoShaderModifiers::EXCLUSION;
    else
        mode = eoShaderModifiers::NORMAL;

    setBlendingMode(canvas, mode);
}

