//
// Created by ego on 8/8/18.
//

#ifndef GFX_PROTOTYPE_EOCOMMANDSWAPLAYERS_H
#define GFX_PROTOTYPE_EOCOMMANDSWAPLAYERS_H

#include "eoCommand.h"

class eoCommandSwapLayers : eoCommand {

public:
    eoCommandSwapLayers();

private:
    void execute(QString arguments) override;

};


#endif //GFX_PROTOTYPE_EOCOMMANDSWAPLAYERS_H
