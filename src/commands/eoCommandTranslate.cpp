//
// Created by ego on 8/22/18.
//

#include "eoCommandTranslate.h"
#include "../eoLayer.h"

eoCommandTranslate::eoCommandTranslate(eoCanvas *canvas)
    : eoCommand("move"), canvas(canvas)
{

}

void eoCommandTranslate::execute(QString arguments) {
    auto list = arguments.split(",");

    if(list.count() < 2)
        return;

    auto module = canvas->getCurrentLayer()->getTranslateModule();
    module->setArgument("position", eoVector2(std::stof(list[0].toStdString()), std::stof(list[1].toStdString())));

    canvas->getCurrentLayer()->updateTransform();
}
