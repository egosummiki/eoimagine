//
// Created by ego on 8/22/18.
//

#ifndef EOIMAGINE_EOCOMMANDTRANSLATE_H
#define EOIMAGINE_EOCOMMANDTRANSLATE_H

#include "eoCommand.h"
#include "../eoCanvas.h"

class eoCommandTranslate : public eoCommand {

    eoCanvas* canvas;

public:

    eoCommandTranslate(eoCanvas* canvas);

    void execute(QString arguments) override;


};


#endif //EOIMAGINE_EOCOMMANDTRANSLATE_H
