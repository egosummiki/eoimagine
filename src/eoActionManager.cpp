//
// Created by ego on 11/29/17.
//

#include "eoActionManager.h"
#include "eoHistoryModel.h"

eoActionManager::eoActionManager() = default;

void eoActionManager::perform(eoAction *action) {

    silentPerform(action);
    action->perform();
}

void eoActionManager::neglectLastAction() {

    if(!historyModel)
        return;

    if(actions.empty())
        return;

    auto last = actions.back();
    actions.pop_back();
    last->neglect();
    neglectedActions.emplace(neglectedActions.begin(), last);

    emit historyModel->dataChanged(
            historyModel->index(actions.size(), 0),
            historyModel->index(actions.size(), 0));
}

void eoActionManager::retrieveLastAction() {

    if(!historyModel)
        return;

    if(neglectedActions.empty())
        return;

    eoAction* last = neglectedActions.front();
    neglectedActions.erase(neglectedActions.begin());
    last->perform();
    actions.emplace_back(last);

    emit historyModel->dataChanged(
            historyModel->index(actions.size() - 1, 0),
            historyModel->index(actions.size() - 1, 0));
}

void eoActionManager::silentPerform(eoAction* action) {
    
    if(!historyModel)
        return;

    while(actions.size() >= 32) {
        auto oldAction = actions[0];
        oldAction->freeWhenOld();
        delete oldAction;

        actions.erase(actions.begin());
    }

    for (auto &&neglectedAction : neglectedActions) {

        neglectedAction->freeWhenAbandoned();
        delete neglectedAction;
    }
    neglectedActions.clear();

    historyModel->beginInsertAction();
    actions.emplace_back(action);
    historyModel->endInsertAction();
}

eoAction* eoActionManager::rowToAction(int row) {

    int actionsSize, negSize;

    actionsSize = actions.size();
    negSize     = neglectedActions.size();

    if(row >= actionsSize) {
        row -= actionsSize;
         
        if(row >= negSize)
            goto invalid_row;

        return neglectedActions[row];
    }

    if(row < 0)
        goto invalid_row;

    return actions[row];
    
invalid_row:
    return nullptr;

}

int eoActionManager::getRowCount() {
    
    return actions.size() + neglectedActions.size();
}

void eoActionManager::setHistoryModel(eoHistoryModel* historyModel) {
    
    this->historyModel = historyModel;
}

bool eoActionManager::isRowNeglected(int row) {
    
    return row >= actions.size();
}
