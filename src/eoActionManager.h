//
// Created by ego on 11/29/17.
//

#ifndef GFX_PROTOTYPE_EOACTIONMANAGER_H
#define GFX_PROTOTYPE_EOACTIONMANAGER_H

#include "actions/eoAction.h"
#include <vector>

class eoHistoryModel;

class eoActionManager {

    friend eoHistoryModel;

private:

    std::vector<eoAction*> actions;
    std::vector<eoAction*> neglectedActions;
    eoHistoryModel* historyModel = nullptr;

    void setHistoryModel(eoHistoryModel* historyModel);

public:
    eoActionManager();

    void perform(eoAction* action);
    void silentPerform(eoAction* action);
    void neglectLastAction();
    void retrieveLastAction();

    eoAction* rowToAction(int row);
    int getRowCount();
    bool isRowNeglected(int row);

};


#endif //GFX_PROTOTYPE_EOACTIONMANAGER_H
