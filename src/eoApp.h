//
// Created by ego on 11/23/17.
//

#ifndef GFX_PROTOTYPE_EOAPP_H
#define GFX_PROTOTYPE_EOAPP_H

#include <QApplication>

class eoApp {
    QApplication* application;

    int argc;
    char** argv;
public:
    eoApp(int _argc, char** _argv);
    int run();
};


#endif //GFX_PROTOTYPE_EOAPP_H
