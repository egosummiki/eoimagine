//
// Created by ego on 02/11/18.
//

#include "eoBlending.h"
#include "eoRender.h"

void eoBlending::apply(Type type) {

    glEnable(GL_BLEND);
    
    switch(type) {

        case NONE:
            disable();
            return;
        case STANDARD:
            glBlendEquation(GL_FUNC_ADD);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            return;
        case PREMUL:
            glBlendEquation(GL_FUNC_ADD);
            glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
            return;
        case ERASE:
            glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_REVERSE_SUBTRACT);
            glBlendFuncSeparate(GL_ONE, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE);
            return;
        case MAX:
            glBlendEquation(GL_MAX);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            return;
    }

}

void eoBlending::disable() {
    
    glDisable(GL_BLEND);
}
