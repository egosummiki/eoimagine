#pragma once
//
// Created by ego on 02/11/18.
//

class eoBlending {

public:

    enum Type {
        NONE,
        STANDARD,
        PREMUL,
        ERASE,
        MAX
    };

    static void apply(Type type);
    static void disable();
};
