//
// Created by ego on 11/28/17.
//

#include <libgen.h>

#include "eoCanvas.h"
#include "ui/eoWindowMain.h"
#include "eoFrameBuffer.h"
#include "eoGLProgram.h"
#include "eoVertexBuffer.h"
#include "shaders/eoShaderScreenDimension.h"
#include "eoLayerZero.h"
#include "eoLayerRaster.h"
#include "eoLayerText.h"
#include "ui/eoWidgetLayers.h"
#include "eoHistoryModel.h"
#include "eoRender.h"
#include "tools/eoTool.h"
#include "eoException.h"
#include "eoFontManager.h"

eoCanvas::eoCanvas(eoRender* render, eoVector2 dimension, eoWindowMain *mainWindow, eoKeyBind *keyBind, int id)
        :   eoLayerManager(mainWindow, render, dimension),
            eoSelection(dimension),
            dimension(dimension),
            id(id),
            render(render),
            middleDimension(dimension / 2.0f),
            screenDimension(mainWindow->getGlArea()->width(), mainWindow->getGlArea()->height()),
            mainWindow(mainWindow),
            keyBind(keyBind),
            input(new eoInput(mainWindow)),
            commandParser(new eoCommandParser(this)),
            actionManager(new eoActionManager()),
            historyModel(new eoHistoryModel(actionManager))
{

    program = new eoGLProgram();
    program->attachShader(eoShader::VERTEX_SCREENDIM);
    program->attachShader(eoShader::FRAGMENT_TRIVIAL);
    program->link();

    applyClearColor();

    /* An OpenGL program used to draw a checherboard pattern. */
    checkerboardProgram = new eoGLProgram();
    checkerboardProgram->attachShader(eoShader::VERTEX_SCREENDIM);
    checkerboardProgram->attachShader(eoShader::FRAGMENT_CHECKERBOARD);
    checkerboardProgram->link();

    vertexBuffer = eoVertexBuffer::square(dimension);

    addLayer(new eoLayerZero(this));

    bindZoomTransformations(input, mainWindow);
}

eoCanvas::~eoCanvas() {

    /* Delete all the objects 
     * dependent on the canvas */
    delete vertexBuffer;
    delete program;
    delete commandParser;
    delete actionManager;
}

/*
 * Canvas render procedure.
 */
void eoCanvas::onRender() {


    /* Render layers */
    eoLayerManager::onRender();

    /* We need OpenGL blending for canvas background and selection. */
    glEnable(GL_BLEND);

    /* Render the checkerboard */
    checkerboardProgram->use();
    checkerboardProgram->setTransform(getZoomMatrix());
    checkerboardProgram->setVec2("dim", dimension);
    checkerboardProgram->setInt("variant", background);
    vertexBuffer->render();

    /* Render the whole canvas with the zoom transform */
    program->use();
    program->setTransform(getZoomMatrix());
    getFrameBuffer()->bind();
    vertexBuffer->render();

    /* Render selection */
    renderSelection(vertexBuffer, dynamic_cast<eoZoom*>(this));

    glDisable(GL_BLEND);
}


void eoCanvas::onResize(int screenWidth, int screenHeight) {
    screenDimension = eoVector2(screenWidth, screenHeight);
    input->onResize(screenWidth / 2.0f, screenHeight / 2.0f);
}

void eoCanvas::applyClearColor() {
    glClearColor(0.1f, 0.1f, 0.11f, 1.0f);
}


const float eoCanvas::getWidth() {
    return dimension.getX();
}

const float eoCanvas::getHeight() {
    return dimension.getY();
}

eoInput *eoCanvas::getInput() {
    return input;
}

eoWindowMain *eoCanvas::getMainWindow() const {
    return mainWindow;
}

eoCommandParser *eoCanvas::getCommandParser() const {
    return commandParser;
}

eoActionManager *eoCanvas::getActionManager() const {
    return actionManager;
}

float eoCanvas::getMiddleWidth() const {
    return middleDimension.getX();
}

float eoCanvas::getMiddleHeight() const {
    return middleDimension.getY();
}

bool eoCanvas::isInside(float x, float y) {
    return x > -getMiddleWidth() && y > -getMiddleHeight() && x < getMiddleWidth() && y < getMiddleHeight();
}

bool eoCanvas::isInside(eoMouseEvent *event) {
    return isInside(event->getCanvasX(), event->getCanvasY());
}

const eoVector2 &eoCanvas::getDimension() const {
    return dimension;
}

void eoCanvas::performOnRender(std::function<void(eoCanvas *)> callback) {

    render->performOnRender(callback);
}

void eoCanvas::requestRedraw() const {
    
    mainWindow->getGlArea()->update();
}


std::string& eoCanvas::getDocumentTitle() {
    
    return documentName;
}

void eoCanvas::setDocumentTitle(std::string name) {
    
    documentName = name;
}

void eoCanvas::setCurrent() {
    
    render->setCurrentCanvas(this);

    mainWindow->getGlArea()->update();
}

int eoCanvas::getId() {
    
    return id;
}

/* Kill canvas */
void eoCanvas::kill() {
    
    render->killCanvas(this);
}

/* EVENT: When canvas is chosen. */
void eoCanvas::onSetCurrent() {

    eoLayerManager::onSetCurrent();

    mainWindow->getHistoryWidget()->getListView()->setModel(historyModel);

    QObject::connect(historyModel, &QAbstractItemModel::rowsInserted, [this] () {
            mainWindow->getHistoryWidget()->getListView()->scrollToBottom();
    });
}

void eoCanvas::onLoseCurrent() {
    
    mainWindow->getLayerWidget()->disconnectSelectionSignal();
    historyModel->disconnect(historyModel);
}

float eoLayerRaster::getOpacity() {
    
    return opacity;
}

eoVertexBuffer *eoCanvas::getVertexBuffer() const {

    return vertexBuffer;
}

eoGLProgram *eoCanvas::getProgram() const {
    
    return program;
}

void eoCanvas::setBackground(Background back) {
    
    background = back;
}
