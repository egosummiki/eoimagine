//
// Created by ego on 11/28/17.
//

#ifndef GFX_PROTOTYPE_EOCANVAS_H
#define GFX_PROTOTYPE_EOCANVAS_H

#include "eoKeyBind.h"
#include "eoMatrix.h"
#include "eoCommandParser.h"
#include "eoActionManager.h"
#include "eoLayerManager.h"
#include "eoSelection.h"
#include "eoZoom.h"

class eoLayer;
class eoToolRepository;
class eoWindowMain;
class eoFrameBuffer;
class eoGLProgram;
class eoVertexBuffer;
class eoInput;
class eoMouseEvent;
class eoHistoryModel;
class eoRender;

class eoCanvas : public eoLayerManager, public eoSelection, public eoZoom {

public:

    enum Background :int {
        CHECKERBOARD = 0,
        WHITE = 1,
        BLACK = 2,
        PINK = 3,
        GREEN = 4
    };

    const eoVector2 &getDimension() const;

private:

    std::string documentName = std::string("Untitled Document");
    std::string filePath;

    Background background = CHECKERBOARD;

    eoGLProgram* program;
    eoGLProgram* checkerboardProgram;
    
    eoVertexBuffer* vertexBuffer;

    eoVector2 dimension;
    eoVector2 middleDimension;
    eoVector2 screenDimension;

    int id = -1;

    eoKeyBind* keyBind;
    eoInput* input;
    eoRender* render;
    eoWindowMain* mainWindow;
    eoCommandParser* commandParser;
    eoActionManager* actionManager;
    eoHistoryModel* historyModel;

public:
    eoCanvas(eoRender* render, eoVector2 dimension, eoWindowMain *mainWindow, eoKeyBind *keyBind, int id);
    ~eoCanvas();

    void onRender();
    void onResize(int screenWidth, int screenHeight);

    void applyClearColor();

    const float getWidth();
    const float getHeight();

    void setCurrent();
    eoVertexBuffer *getVertexBuffer() const;
    eoGLProgram *getProgram() const;

    eoInput* getInput();
    eoWindowMain *getMainWindow() const;
    eoActionManager *getActionManager() const;
    void requestRedraw() const;

    bool isInside(float x, float y);
    bool isInside(eoMouseEvent* event);

    float getMiddleWidth() const;
    float getMiddleHeight() const;

    eoCommandParser *getCommandParser() const;

    void performOnRender(std::function<void(eoCanvas*)> callback);

    void setDocumentTitle(std::string name);
    std::string& getDocumentTitle();
    int getId();
    void kill();

    void onSetCurrent();
    void onLoseCurrent();

    void setBackground(Background back);
};


#endif //GFX_PROTOTYPE_EOCANVAS_H

