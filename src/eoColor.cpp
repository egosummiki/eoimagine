//
// Created by ego on 8/11/18.
//

#include "eoColor.h"
#include <cmath>

eoColor eoColor::WHITE          = eoColor(1.0f, 1.0f, 1.0f, 1.0f);
eoColor eoColor::TRANSPARENT    = eoColor(0.0f, 0.0f, 0.0f, 0.0f);
eoColor eoColor::BLACK          = eoColor(0.0f, 0.0f, 0.0f, 1.0f);


eoColor::eoColor(float r, float g, float b, float a)
: r(r), g(g), b(b), a(a)
{}

eoColor::eoColor(uint rgbaColor) {
    
    auto colorValues = reinterpret_cast<uchar*>(&rgbaColor);
    r = static_cast<float>(colorValues[3]) / 255.0f;
    g = static_cast<float>(colorValues[2]) / 255.0f;
    b = static_cast<float>(colorValues[1]) / 255.0f;
    a = static_cast<float>(colorValues[0]) / 255.0f;
}

float hexToFloat(std::string hexValue, float divBy) {

    return static_cast<float>(std::stol(hexValue, 0, 16))/divBy;
}


eoColor::eoColor(std::string hexColor) {

    for(char elem :hexColor) {

        if((elem < '0' || elem > '9') && (elem < 'a' || elem > 'f') && (elem < 'A' || elem > 'F')) {

            r = g = b = 0.0f;
            a = 1.0f;
            return;
        }
    }

    switch(hexColor.size()) {
        
        case 0:
            r = g = b = 0.0f;
            a = 1.0f;
            return;
        case 1:
            r = g = b = hexToFloat(hexColor, 15.0f);
            a = 1.0f;
            return;
        case 2:
            r = g = b = hexToFloat(hexColor, 255.0f);
            a = 1.0f;
            return;
        case 3:
            r = hexToFloat(hexColor.substr(0, 1), 15.0f);
            g = hexToFloat(hexColor.substr(1, 1), 15.0f);
            b = hexToFloat(hexColor.substr(2, 1), 15.0f);
            a = 1.0f;
            return;
        case 4:
            r = hexToFloat(hexColor.substr(0, 1), 15.0f);
            g = hexToFloat(hexColor.substr(1, 1), 15.0f);
            b = hexToFloat(hexColor.substr(2, 1), 15.0f);
            a = hexToFloat(hexColor.substr(3, 1), 15.0f);
            return;
        case 5:
            r = hexToFloat(hexColor.substr(0, 2), 255.0f);
            g = hexToFloat(hexColor.substr(2, 2), 255.0f);
            b = hexToFloat(hexColor.substr(4, 1), 15.0f);
            a = 1.0f;
            return;
        case 6:
            r = hexToFloat(hexColor.substr(0, 2), 255.0f);
            g = hexToFloat(hexColor.substr(2, 2), 255.0f);
            b = hexToFloat(hexColor.substr(4, 2), 255.0f);
            a = 1.0f;
            return;
        case 7:
            r = hexToFloat(hexColor.substr(0, 2), 255.0f);
            g = hexToFloat(hexColor.substr(2, 2), 255.0f);
            b = hexToFloat(hexColor.substr(4, 2), 255.0f);
            a = hexToFloat(hexColor.substr(6, 1), 15.0f);
            return;
        default:
            r = hexToFloat(hexColor.substr(0, 2), 255.0f);
            g = hexToFloat(hexColor.substr(2, 2), 255.0f);
            b = hexToFloat(hexColor.substr(4, 2), 255.0f);
            a = hexToFloat(hexColor.substr(6, 2), 255.0f);

    }
}

float eoColor::getR() const {
    return r;
}

void eoColor::setR(float r) {
    eoColor::r = r;
}

float eoColor::getG() const {
    return g;
}

void eoColor::setG(float g) {
    eoColor::g = g;
}

float eoColor::getB() const {
    return b;
}

void eoColor::setB(float b) {
    eoColor::b = b;
}

float eoColor::getA() const {
    return a;
}

void eoColor::setA(float a) {
    eoColor::a = a;
}

void eoColor::setAsClearColor() {

    glClearColor(r, g, b, a);

}

QColor eoColor::toQColor() {
    
    return QColor(r * 255, g *255, b * 255);
}

float eoColor::getL() const {
    
    float max = r > g ? (r > b ? r : b) : (g > b ? g : b);
    float min = r < g ? (r < b ? r : b) : (g < b ? g : b);

    return (max + min) / 2.0f;
}

float eoColor::getV() const {
    
    return r > g ? (r > b ? r : b) : (g > b ? g : b);
}

float eoColor::getH() const {
    
    float delta;
    float min = r < g ? (r < b ? r : b) : (g < b ? g : b);

    if(r > g) {
        if(r > b) {
            delta = r - min;

            if(!delta)
                return 0.0f;

            return fmod((g - b)/delta, 6);
        } 
    } else {
        if(g > b) {
            delta = g - min;

            if(!delta)
                return 0.0f;

            return (b-r)/delta + 2.0f;
        } 
    }

    delta = b - min;
    if(!delta)
        return 0.0f;

    return (r-g)/delta + 4.0f;
}

float eoColor::getS() const {
    

    float max = r > g ? (r > b ? r : b) : (g > b ? g : b);
    float min = r < g ? (r < b ? r : b) : (g < b ? g : b);

    float delta = max - min;

    if(!delta)
        return 0.0f;

    return delta / (1.0f - abs(max + min - 1.0f));
}

eoColor::HSV eoColor::toHSV() const {
    
    float delta;
    float min = r < g ? (r < b ? r : b) : (g < b ? g : b);

    if(r > g) {
        if(r > b) {
            delta = r - min;

            if(!delta)
                return (HSV) {0.0f, 0.0f, r};

            return (HSV) {static_cast<float>(fmod((g - b)/delta, 6)), delta / (1.0f - abs(r + min - 1.0f)), r};
        } 
    } else {
        if(g > b) {
            delta = g - min;

            if(!delta)
                return (HSV) {0.0f, 0.0f, g};

            return (HSV) {(b-r)/delta + 2.0f, delta / (1.0f - abs(g + min - 1.0f)), g};
        } 
    }

    delta = b - min;
    if(!delta)
        return (HSV) {0.0f, 0.0f, b};

    return (HSV) {(r-g)/delta + 4.0f, delta / (1.0f - abs(b + min - 1.0f)), b};
}

eoColor::HSL eoColor::toHSL() const {
    
    float delta;
    float min = r < g ? (r < b ? r : b) : (g < b ? g : b);

    if(r > g) {
        if(r > b) {
            delta = r - min;

            if(!delta)
                return (HSL) {0.0f, 0.0f, (r+min)/2.0f};

            return (HSL) {static_cast<float>(fmod((g - b)/delta, 6)), delta / (1.0f - abs(r + min - 1.0f)), (r+min)/2.0f};
        } 
    } else {
        if(g > b) {
            delta = g - min;

            if(!delta)
                return (HSL) {0.0f, 0.0f, (g+min)/2.0f};

            return (HSL) {(b-r)/delta + 2.0f, delta / (1.0f - abs(g + min - 1.0f)), (g+min)/2.0f};
        } 
    }

    delta = b - min;
    if(!delta)
        return (HSL) {0.0f, 0.0f, (b+min)/2.0f};

    return (HSL) {(r-g)/delta + 4.0f, delta / (1.0f - abs(b + min - 1.0f)), (b+min)/2.0f};
}
