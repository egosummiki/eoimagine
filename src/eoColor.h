//
// Created by ego on 8/11/18.
//

#ifndef GFX_PROTOTYPE_EOCOLOR_H
#define GFX_PROTOTYPE_EOCOLOR_H

#include <GL/gl.h>
#include <string>
#include <QColor>

class eoColor {

public:

    struct HSV {
        float h,s,v;
    };

    struct HSL {
        float h,s,l;
    };

private:
    float r,g,b,a;
public:
    eoColor(float r, float g, float b, float a);
    eoColor(std::string hexColor);
    eoColor(uint rgbaColor);

    float getR() const;
    void setR(float r);
    float getG() const;
    void setG(float g);
    float getB() const;
    void setB(float b);
    float getA() const;
    void setA(float a);

    HSV toHSV() const;
    HSL toHSL() const;
    float getL() const;
    float getV() const;
    float getH() const;
    float getS() const;

    QColor toQColor();

    void setAsClearColor();

    static eoColor WHITE;
    static eoColor TRANSPARENT;
    static eoColor BLACK;

};


#endif //GFX_PROTOTYPE_EOCOLOR_H
