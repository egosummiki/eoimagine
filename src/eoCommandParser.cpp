//
// Created by ego on 7/30/18.
//

#include "eoCommandParser.h"
#include "eoMessageBox.h"

#include "commands/eoCommandMessage.h"
#include "commands/eoCommandBrightness.h"
#include "commands/eoCommandBlur.h"
#include "commands/eoCommandContrast.h"
#include "commands/eoCommandTranslate.h"
#include "commands/eoCommandBrushSize.h"
#include "commands/eoCommandBrushColor.h"
#include "commands/eoCommandBrushOffset.h"
#include "commands/eoCommandBrushHardness.h"
#include "commands/eoCommandChangeOpacity.h"
#include "commands/eoCommandSetBlending.h"
#include "commands/eoCommandFill.h"

eoCommandParser::eoCommandParser(eoCanvas *canvas)
    : canvas(canvas)
{
    registerCommand(new eoCommandMessage());
    registerCommand(new eoCommandBrightness(canvas));
    registerCommand(new eoCommandContrast(canvas));
    registerCommand(new eoCommandBlur(canvas));
    registerCommand(new eoCommandTranslate(canvas));
    registerCommand(new eoCommandBrushSize(canvas)); 
    registerCommand(new eoCommandBrushColor(canvas));
    registerCommand(new eoCommandBrushOffset(canvas)); 
    registerCommand(new eoCommandBrushHardness(canvas)); 
    registerCommand(new eoCommandChangeOpacity(canvas)); 
    registerCommand(new eoCommandSetBlending(canvas)); 
    registerCommand(new eoCommandFill(canvas)); 
}

void eoCommandParser::parse(QString string) {

   QString commandName;

   int cursor = 0;
   while((++cursor) < string.count() &&
        ((string.at(cursor) >= 'a' && string.at(cursor) <= 'z') ||
         (string.at(cursor) >= 'A' && string.at(cursor) <= 'Z'))) {

       commandName.append(string.at(cursor));

   }

   for(auto&& command : commands) {

       if(command->check(commandName)) {
           if(string.at(cursor) == " ") {
               ++cursor;
           } 
           command->execute(string.right(string.count() - cursor));
           break;
       }

   }
}

eoCommandParser::~eoCommandParser() {

    for(auto&& command : commands) {
        delete command;
    }

}

void eoCommandParser::registerCommand(eoCommand *command) {

    commands.emplace_back(command);
}
