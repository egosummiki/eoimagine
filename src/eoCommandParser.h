//
// Created by ego on 7/30/18.
//

#ifndef GFX_PROTOTYPE_EOCOMMANDPARSER_H
#define GFX_PROTOTYPE_EOCOMMANDPARSER_H

#include <QString>
#include <vector>
#include "commands/eoCommand.h"

class eoCanvas;

class eoCommandParser {

    eoCanvas* canvas;
    std::vector<eoCommand*> commands;

public:

    explicit eoCommandParser(eoCanvas* canvas);
    ~eoCommandParser();
    void parse(QString string);
    void registerCommand(eoCommand* command);

};


#endif //GFX_PROTOTYPE_EOCOMMANDPARSER_H
