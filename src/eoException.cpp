//
// Created by ego on 11/24/17.
//

#include <cstring>
#include "eoException.h"
#include "eoMessageBox.h"

const char* eoException::stringsGeneral[]{
        "Nada"
};
const char* eoException::stringsShader[]{
        "Cannot open shader file",
        "Compile error",
        "Link error"
};
const char* eoException::stringsTexture[]{
        "Cannot open image file",
        "Problem with image format"
};
const char* eoException::stringsFramebuffer[]{
        "Framebuffer is undefined",
        "Framebuffer attachment is incomplete",
};
const char* eoException::stringsFont[]{
        "Cannot load font"
};
const char* eoException::stringsTypes[]{
        "General Exception",
        "Shader Exception",
        "Texture Exception",
        "Framebuffer Exception",
        "Font Exception"
};
const char** eoException::stringsMessages[]{
        stringsGeneral,
        stringsShader,
        stringsTexture,
        stringsFramebuffer,
        stringsFont
};

eoException::eoException(unsigned int type, unsigned int id, QString info) : type(type), id(id), info(info) {}

void eoException::showMessageBox() {

    if(info.isEmpty()) {

        eoMessageBox::showException(stringsTypes[type], stringsMessages[type][id]);
    } else {

        eoMessageBox::showException(stringsTypes[type], stringsMessages[type][id], info);
    }

    exit(1);
}
