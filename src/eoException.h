//
// Created by ego on 11/24/17.
//

#ifndef GFX_PROTOTYPE_EOEXCEPTION_H
#define GFX_PROTOTYPE_EOEXCEPTION_H

#include <exception>
#include <QString>

class eoException : std::exception {
    static const char* stringsGeneral[];
    static const char* stringsShader[];
    static const char* stringsTexture[];
    static const char* stringsFramebuffer[];
    static const char* stringsFont[];
    static const char* stringsTypes[];
    static const char** stringsMessages[];

    unsigned type;
    unsigned id;
    QString info;
public:
    eoException(unsigned int type, unsigned int id, QString info);
    void showMessageBox();
};

#define eoExceptionShaderNoFile(info) eoException(1,0,info)
#define eoExceptionShaderCompile(info) eoException(1,1,info)
#define eoExceptionShaderLink(info) eoException(1,2,info)

#define eoExceptionTextureNoFile eoException(2,0,"")
#define eoExceptionTextureConvert(info) eoException(2,1,info)

#define eoExceptionFramebufferUndefined eoException(3,0,"")
#define eoExceptionFramebufferIncomplete eoException(3,1,"")

#define eoExceptionFontNoFile(info) eoException(4,0,info)

#endif //GFX_PROTOTYPE_EOEXCEPTION_H
