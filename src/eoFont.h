#pragma once
//
// Created by ego on 27/10/18.
//
#include <ft2build.h>
#include FT_FREETYPE_H  
#include <string>
#include <vector>
#include "eoRender.h"
#include "eoVertexBuffer.h"

#define EO_UNICODE_BLOCKS_NUM 291
#define EO_UNICODE_BASICLATIN 0

class eoFont {

private:

    struct UnicodeBlock {

        ulong begin, end;
    };

    struct Glyph {
        GLuint texture;
        eoVertexBuffer *vertexBuffer;
        float width, height;
        float offsetX, offsetY;
        float advance;
    };

    struct LoadedBlock {
        
        uint    blockId;
        Glyph  *glyphs;
    };

    FT_Face face;
    eoRender *render;
    std::vector<LoadedBlock> loadedBlocks;
    eoGLProgram *program = nullptr;
    int size;

public:
    eoFont(eoRender *render, std::string fontFileName, int size=48);
    ~eoFont();

    void setSize(int size);
    void draw(std::string text, eoMatrix trans, eoColor color, int sepChar = 0, int sepSpace = 0, int sepLine = 0);
    void loadGlyph(uint loadedBlock, ulong character);
    void loadUnicodeBlock(uint blockId);
    uint charToBlock(ulong character);
    LoadedBlock &findLoadedBlock(uint blockID);
    void release();
    void onCanvasChange(eoCanvas *canvas);

    static UnicodeBlock unicodeBlocks[EO_UNICODE_BLOCKS_NUM];
};
