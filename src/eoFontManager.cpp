//
// Created by ego on 16/11/18.
//

#include "eoFontManager.h"
#include "eoMessageBox.h"
#include "eoFont.h"
#include <iostream>

eoFontManager::eoFontManager(eoRender *render)
    : render(render)
{
    int i, failedFonts = 0;
    
    /* Initialisation of the freetype library */
    EO_ASSERT(!FT_Init_FreeType(&fontLibrary), "Failed to init the freetype library.");

    /* Initialisation of the font config library */
    FcConfig *fontConfig = FcInitLoadConfigAndFonts();

    FcPattern *pat = FcPatternCreate();
    FcObjectSet *os = FcObjectSetBuild (FC_FAMILY, FC_STYLE, FC_LANG, FC_FILE, (char *) 0);
    FcFontSet *fs = FcFontList(fontConfig, pat, os);

    numFonts = fs->nfont;
    numFamilies = numFonts;
    
    EO_ASSERT(numFonts > 0, "Invalid font number.");
    EO_ASSERT(fs, "Couldn't load font object.");

    fontBase = new FontInfo[numFonts];

    for (i = 0; i < fs->nfont; ++i) {

       FcPattern* font = fs->fonts[i];
       FcChar8 *file, *style, *family;

       if(FcPatternGetString(font, FC_FILE, 0, &file) != FcResultMatch ||
          FcPatternGetString(font, FC_FAMILY, 0, &family) != FcResultMatch ||
          FcPatternGetString(font, FC_STYLE, 0, &style) != FcResultMatch) {
       
           failedFonts++;
           continue;
       }

       fontBase[i-failedFonts].filename = std::string(reinterpret_cast<char*>(file));
       fontBase[i-failedFonts].style = QString(reinterpret_cast<char*>(style));
       fontBase[i-failedFonts].family = QString(reinterpret_cast<char*>(family));
    }

    numFonts -= failedFonts;

    if (fs)
        FcFontSetDestroy(fs);

    sortFonts(fontBase, fontBase+numFonts);

    for(i = numFonts-1; i > 0; --i) {

        if(fontBase[i].family.value() == fontBase[i-1].family.value()) {
            fontBase[i].family = std::nullopt;
            --numFamilies;
        }

    }

}

FT_Library &eoFontManager::getFontLibrary() {
    
    return fontLibrary;
}

eoFontManager::~eoFontManager() {
   delete[] fontBase; 
}

void eoFontManager::sortFonts(FontInfo *values, FontInfo *limit) {
    
    FontInfo *pivot, *compare, *swap_down;

    /* Standard quick sort */

    pivot = values;

    for(compare = pivot+1; compare != limit; ++compare)

        if(pivot->family.value().compare(compare->family.value(), Qt::CaseInsensitive) > 0) {

            for(swap_down = compare; swap_down != pivot; --swap_down)
                std::swap<FontInfo>(*(swap_down-1), *(swap_down));

            ++pivot;
        } 

    if(pivot-values > 1)
        sortFonts(values, pivot);

    ++pivot;

    if(limit-pivot > 1)
        sortFonts(pivot, limit);
}

eoFont *eoFontManager::getFont(int fontID, int fontSize) {

    for(auto &info :fonts) {

        if(info.fontID == fontID && info.size == fontSize) {
            ++info.referenceCount;
            return info.font;
        }
    }
    
    auto font = new eoFont(render, fontBase[fontID].filename, fontSize);
    fonts.emplace_back((FontLoadInfo) {font, fontID, fontSize, 1});

    return font;
}


void eoFontManager::freeFont(eoFont *font) {
    
    for(int i = 0; i < fonts.size(); ++i) {

        if(fonts[i].font == font) {

            if(!(--fonts[i].referenceCount)) {
                delete font;
                fonts.erase(fonts.begin() + i);
            }

            return;
        }
    }
}

int eoFontManager::findFont(QString family, QString style) {

    int i;
    
    for(i = 0; i < numFonts; ++i) {

        if(fontBase[i].family != std::nullopt) {
            if(fontBase[i].family.value() == family) {

                if(fontBase[i].style == style)
                    return i;

                ++i;

                break;
            }
        }
    }

    for(; i < numFonts; ++i) {

            if(fontBase[i].family != std::nullopt)
                return -1;

            if(fontBase[i].style == style)
                return i;
    }

    return -1;
}

int eoFontManager::findFont(int familyID, int style) {
    
    int i, family;
    
    for(i = 0, family = -1; i < numFonts; ++i) {

        if(fontBase[i].family != std::nullopt) {
            ++family;
            if(family == familyID) {

                while(style--)
                    ++i;

                return i;
            }
        }
    }

    return -1;
}

eoFont *eoFontManager::getFont(QString family, QString style, int fontSize) {

    int id = findFont(family, style);

    if(id == -1)
        return nullptr;
    
    return getFont(id, fontSize);
}

eoFont *eoFontManager::getFont(int familyID, int style, int fontSize) {
    
    int id = findFont(familyID, style);

    if(id == -1)
        return nullptr;
    
    return getFont(id, fontSize);
}

eoFontManager::FontInfo eoFontManager::getFontInfo(int fontID) {

    return fontBase[fontID];
}

int eoFontManager::getFontAmount() {
    
    return numFonts;
}

std::vector<QString> eoFontManager::getFamilyStyles(int familyID) {
    
    int i, family;
    std::vector<QString> result;
    
    for(i = 0, family = -1; i < numFonts; ++i) {

        if(fontBase[i].family != std::nullopt) {
            ++family;

            if(family == familyID) {
                result.emplace_back(fontBase[i].style);
                ++i;
                break;
            }
        }
    }

    for(; i < numFonts; ++i) {

        if(fontBase[i].family != std::nullopt) 
            break;

        result.emplace_back(fontBase[i].style);
    }

    return result;
}

void eoFontManager::onCanvasChange(eoCanvas *canvas) {
    
    for(auto &info :fonts) {
        info.font->onCanvasChange(canvas);
    }
}
