#pragma once
//
// Created by ego on 16/11/18.
//
#include <ft2build.h>
#include FT_FREETYPE_H  
#include <fontconfig.h>
#include <QString>
#include <string>
#include <vector>
#include <optional>
#include "eoRender.h"

class eoFont;

class eoFontManager {

public:

    struct FontInfo {

        std::string filename; // Filename is for file operations so std::string
        std::optional<QString> family; // Family and style for displaying so QString
        QString style;
    };

    struct FontLoadInfo {

        eoFont *font;
        int fontID, size, referenceCount;
    };


private:

    FT_Library fontLibrary;

    int numFonts;
    int numFamilies;
    FontInfo *fontBase;
    eoRender *render;
    std::vector<FontLoadInfo> fonts;

    void sortFonts(FontInfo *value, FontInfo *limit);

public:

    eoFontManager(eoRender *render);
    ~eoFontManager();
    FT_Library &getFontLibrary();
    eoFont *getFont(int fontID, int fontSize);
    eoFont *getFont(QString family, QString style, int fontSize);
    eoFont *getFont(int familyID, int style, int fontSize);
    int findFont(QString family, QString style);
    int findFont(int familyID, int style);
    void freeFont(eoFont *font);
    FontInfo getFontInfo(int fontID);
    int getFontAmount();
    std::vector<QString> getFamilyStyles(int familyID);
    void onCanvasChange(eoCanvas *canvas);
};
