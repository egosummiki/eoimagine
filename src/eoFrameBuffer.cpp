//
// Created by ego on 12/2/17.
//


#include <cstdlib>
#include "eoFrameBuffer.h"
#include "eoException.h"
#include "eoGLProgram.h"
#include "eoVertexBuffer.h"
#include "eoMessageBox.h"


eoFrameBuffer::eoFrameBuffer(eoVector2 dimension, GLenum attachment, GLint internalFormat, GLenum format)
    : dimension(dimension), colorBuffer(attachment), format(format)
{

    try {
        glGenFramebuffers(1, &buffer);
        glBindFramebuffer(GL_FRAMEBUFFER, buffer);

        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);

        glTexImage2D(
            GL_TEXTURE_2D,
            0,
            internalFormat,
            static_cast<GLsizei>(dimension.getX()),
            static_cast<GLsizei>(dimension.getY()),
            0,
            format,
            GL_UNSIGNED_BYTE,
            nullptr);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, texture, 0);

        GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

        switch(status) {
            case GL_FRAMEBUFFER_UNDEFINED:
                throw eoExceptionFramebufferUndefined;
            case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
                throw eoExceptionFramebufferIncomplete;

            default: return;
        }

    } catch (eoException& exception)
    {
        exception.showMessageBox();
        exit(1);
    }
}


void eoFrameBuffer::begin() {

    /* Save up previous framebuffer data */
    glGetIntegerv(GL_VIEWPORT, previousViewport);
    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &previousBuffer);
    glGetIntegerv(GL_DRAW_BUFFER, &previousDrawBuffer);

    /* Bind the framebuffer and adjust proper viewport */
    glBindFramebuffer(GL_FRAMEBUFFER, buffer);
    glViewport(0, 0, static_cast<GLsizei>(dimension.getX()), static_cast<GLsizei>(dimension.getY()));

    glDrawBuffer(colorBuffer);
}

void eoFrameBuffer::end() {

    /* Bind the previous framebuffer and set its viewport and draw buffer */
    glBindFramebuffer(GL_FRAMEBUFFER, static_cast<GLuint>(previousBuffer));
    glViewport(previousViewport[0], previousViewport[1], previousViewport[2], previousViewport[3]);
    glDrawBuffer(previousDrawBuffer);
}

float eoFrameBuffer::getWidth() {
    return dimension.getX();
}

float eoFrameBuffer::getHeight() {
    return dimension.getY();
}

GLuint eoFrameBuffer::getTexture() {
    return texture;
}

GLuint eoFrameBuffer::getBuffer() {
    return buffer;
}

void eoFrameBuffer::bind(GLenum textureId) {
    
    glActiveTexture(textureId);
    glBindTexture(GL_TEXTURE_2D, texture);
}

void imageCleanUpHandler(void *info) {
    auto data = static_cast<uchar*>(info);
    delete[] data;
}

QImage eoFrameBuffer::toQImage() {
    
    auto width  = static_cast<int>(dimension.getX());
    auto height = static_cast<int>(dimension.getY());

    auto data = new uchar[width * height * sizeof(uint)];

    glBindFramebuffer(GL_FRAMEBUFFER, buffer);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    //glReadnPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, width*height*sizeof(uint), data);
    glReadnPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, width*height*sizeof(uint), data);

    GLenum err;
    while((err = glGetError()) != GL_NO_ERROR) {

        EO_FAIL(QString("OpenGL error reading raw framebuffer data. Error code: 0x%1").arg(err, 0, 16));
    }

    return QImage(data, width, height, QImage::Format_RGBA8888, imageCleanUpHandler, data);
}

uint *eoFrameBuffer::getRawData() {
    
    auto width  = static_cast<int>(dimension.getX());
    auto height = static_cast<int>(dimension.getY());

    auto data = new uint[width * height];

    glBindFramebuffer(GL_FRAMEBUFFER, buffer);

    //glReadnPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, width*height*sizeof(uint), data);
    glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, data);

    GLenum err;
    while((err = glGetError()) != GL_NO_ERROR) {

        EO_FAIL(QString("OpenGL error reading raw framebuffer data. Error code: 0x%1").arg(err, 0, 16));
    }


    return data;
}

eoFrameBuffer* eoFrameBuffer::clone() {

    auto clonedBuffer = createPersevering(dimension, GL_COLOR_ATTACHMENT1);

    clonedBuffer->begin();
    {
        eoGLProgram::TRIVIAL->use();
        eoGLProgram::TRIVIAL->setTransform(eoMatrix());
        bind();

        eoVertexBuffer::FULL_DIMENSION->render();
    }
    clonedBuffer->end();


    return clonedBuffer;    
}

eoFrameBuffer::~eoFrameBuffer() {
    
    glDeleteFramebuffers(1, &buffer);
    glDeleteTextures(1, &texture);
}

eoFrameBuffer* eoFrameBuffer::createPersevering(eoVector2 dimension, GLenum attachment, GLint internalFormat, GLenum format) {

    eoFrameBuffer* freshBruffer;
    GLint prevBuffer;

    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &prevBuffer);
    freshBruffer = new eoFrameBuffer(dimension, attachment, internalFormat, format);
    glBindFramebuffer(GL_FRAMEBUFFER, static_cast<GLuint>(prevBuffer));

    return freshBruffer;
}
