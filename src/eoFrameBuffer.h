//
// Created by ego on 12/2/17.
//

#ifndef GFX_PROTOTYPE_EOFRAMEBUFFER_H
#define GFX_PROTOTYPE_EOFRAMEBUFFER_H

#include <GL/gl.h>
#include <GLES3/gl32.h>
#include <QImage>
#include "eoVector2.h"

class eoFrameBuffer {
    GLuint texture;
    GLint previousBuffer;
    GLint previousViewport[4];
    GLint previousDrawBuffer;
    GLuint buffer;
    GLuint depthBuffer;
    GLuint stencilBuffer;
    GLenum colorBuffer;
    GLenum format;

    eoVector2 dimension;

public:
    eoFrameBuffer(eoVector2 dimension, GLenum attachment = GL_COLOR_ATTACHMENT0, GLint internalFormat = GL_RGBA, GLenum format = GL_RGBA);
    ~eoFrameBuffer();
    void begin();
    void end();

    float getWidth();
    float getHeight();
    void bind(GLenum textureId = GL_TEXTURE0);
    GLuint getTexture();
    GLuint getBuffer();
    eoFrameBuffer* clone();

    QImage toQImage();
    uint *getRawData();

    static eoFrameBuffer* createPersevering(eoVector2 dimension, GLenum attachment = GL_COLOR_ATTACHMENT0, GLint internalFormat = GL_RGBA, GLenum format = GL_RGBA);
};


#endif //GFX_PROTOTYPE_EOFRAMEBUFFER_H
