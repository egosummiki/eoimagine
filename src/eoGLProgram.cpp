//
// Created by ego on 11/24/17.
//

#include "eoGLProgram.h"
#include "eoException.h"

eoGLProgram::eoGLProgram() {
    id = glCreateProgram();
}

eoGLProgram::~eoGLProgram() {
    
    glDeleteProgram(id);
}

void eoGLProgram::attachShader(eoShader *shader) {
    shaders.emplace_back(shader);
    glAttachShader(id, shader->getId());
}

void eoGLProgram::link() {

    try {

        glLinkProgram(id);

        GLint result = GL_FALSE;
        int infoLogLength;

        glGetProgramiv(id, GL_LINK_STATUS, &result);
        glGetProgramiv(id, GL_INFO_LOG_LENGTH, &infoLogLength);

        if(infoLogLength > 0) {

            auto infoLog = new char[infoLogLength + 1];
            glGetProgramInfoLog(id, infoLogLength, nullptr, infoLog);
            throw eoExceptionShaderLink(infoLog);
        }

        for(auto &shader :shaders) {

            if(shader->onUseReady())
                onUseCallbacks.emplace_back(shader->onUseCallback);
        }

        while(!shaders.empty()) {

            detachShader(0);
        }

    } catch (eoException& exception) {

        exception.showMessageBox();
    }

}

void eoGLProgram::detachShader(int shader_id) {

    glDetachShader(id, shaders[shader_id]->getId());
    shaders.erase(shaders.begin() + shader_id);
}

void eoGLProgram::use() {

    glUseProgram(id);
    for (auto &callback :onUseCallbacks) {
        callback(this);
    }
}

GLint eoGLProgram::getAttribute(const char *attribute) {
    return glGetAttribLocation(id, attribute);
}

GLint eoGLProgram::getUniform(const char *uniform) {
    return glGetUniformLocation(id, uniform);
}


GLint eoGLProgram::getUniform(std::string s) {
    return glGetUniformLocation(id, s.c_str());
}

void eoGLProgram::setTransform(eoMatrix mat) {
    glUniformMatrix3fv(getUniform("transform"), 1, GL_FALSE, mat.getData());
}

void eoGLProgram::setFloat(std::string uniform, float value) {
    glUniform1f(getUniform(uniform), value);
}

void eoGLProgram::setVec2(std::string uniform, float x, float y) {
    glUniform2f(getUniform(uniform), x, y);
}

void eoGLProgram::setVec2(std::string uniform, const eoVector2 vector) {
    glUniform2f(getUniform(uniform), vector.getX(), vector.getY());
}

void eoGLProgram::setVec3(const char *uniform, float x, float y, float z) {
    glUniform3f(getUniform(uniform), x, y, z);
}

void eoGLProgram::setVec4(const char *uniform, float x, float y, float z, float w) {
    glUniform4f(getUniform(uniform), x, y, z, w);
}

eoGLProgram* eoGLProgram::TRIVIAL;
eoGLProgram* eoGLProgram::PREMUL;

void eoGLProgram::loadStaticPrograms() {

    TRIVIAL = new eoGLProgram();
    TRIVIAL->attachShader(eoShader::VERTEX_TRIVIAL);
    TRIVIAL->attachShader(eoShader::FRAGMENT_TRIVIAL);
    TRIVIAL->link();

    PREMUL = new eoGLProgram();
    PREMUL->attachShader(eoShader::VERTEX_TRIVIAL);
    PREMUL->attachShader(eoShader::FRAGMENT_PREMUL);
    PREMUL->link();
}

void eoGLProgram::setVec2(const char *uniform, const eoVector2 vector) {

    glUniform2fv(getUniform(uniform), 1, reinterpret_cast<const GLfloat*>(&vector));
}

void eoGLProgram::setColor(const char* uniform, eoColor color) {
    
    glUniform4fv(getUniform(uniform), 1, reinterpret_cast<const GLfloat*>(&color));
}

void eoGLProgram::setTexture(const char* uniform, int location) {
    
    glUniform1i(getUniform(uniform), location);
}

void eoGLProgram::setInt(const char* uniform, int value) {
    
    glUniform1i(getUniform(uniform), value);
}
