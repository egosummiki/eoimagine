//
// Created by ego on 11/24/17.
//

#ifndef GFX_PROTOTYPE_EOGLPROGRAM_H
#define GFX_PROTOTYPE_EOGLPROGRAM_H

#include "shaders/eoShader.h"
#include "eoMatrix.h"

#include <GL/gl.h>
#include <GLES3/gl32.h>
#include <vector>
#include <functional>
#include "eoColor.h"

class eoGLProgram {
    GLuint id;
    std::vector<eoShader*> shaders;
    std::vector<std::function<void(void*)>> onUseCallbacks;
public:
    eoGLProgram();
    ~eoGLProgram();
    void attachShader(eoShader* shader);
    void detachShader(int shader_id);
    GLint getAttribute(const char* attribute);
    GLint getUniform(const char* uniform);
    GLint getUniform(std::string s);
    void setTransform(eoMatrix mat);
    void setFloat(std::string uniform, float value);
    void setInt(const char* uniform, int value);
    void setVec2(std::string uniform, float x, float y);
    void setVec2(const char* uniform, const eoVector2 vector);
    void setVec2(std::string uniform, const eoVector2 vector);
    void setVec3(const char* uniform, float x, float y, float z);
    void setVec4(const char* uniform, float x, float y, float z, float w);
    void setColor(const char* uniform, eoColor color);
    void setTexture(const char* uniform, int location);
    void link();
    void use();

    static eoGLProgram *TRIVIAL;
    static eoGLProgram *PREMUL;
    static void loadStaticPrograms();
};


#endif //GFX_PROTOTYPE_EOGLPROGRAM_H
