//
// Created by ego on 18/09/18.
//

#include "eoHistoryModel.h"
#include <QBrush>

eoHistoryModel::eoHistoryModel(eoActionManager* actionManager)
    : actionManager(actionManager)
{
    actionManager->setHistoryModel(this);
}

QModelIndex eoHistoryModel::index(int row, int column, const QModelIndex& parent) const {

    if(parent != QModelIndex())
        return QModelIndex();

    return createIndex(row, column, actionManager->rowToAction(row));
}

int eoHistoryModel::rowCount(const QModelIndex& parent) const {
    
    return actionManager->getRowCount();
}

int eoHistoryModel::columnCount(const QModelIndex&) const {
    
    return 1;
}

QVariant eoHistoryModel::data(const QModelIndex &index, int role) const {

    if(!index.isValid())
        return QVariant();

    auto action = static_cast<eoAction*>(index.internalPointer());

    if(!action)
        return QVariant();

    if(role == Qt::DisplayRole) {
        return QVariant(action->getName().c_str());
    } else if(role == Qt::ForegroundRole) {
        if(actionManager->isRowNeglected(index.row()))
            return QBrush(Qt::gray);
    }


    return QVariant();
}

QModelIndex eoHistoryModel::parent(const QModelIndex& index) const {
    
    return QModelIndex();
}

void eoHistoryModel::beginInsertAction() {

    beginInsertRows(QModelIndex(), actionManager->getRowCount(), actionManager->getRowCount());
}

void eoHistoryModel::endInsertAction() {
    
    endInsertRows();
}

Qt::ItemFlags eoHistoryModel::flags(const QModelIndex &index) const {

    if(actionManager->isRowNeglected(index.row()))
        return Qt::ItemIsSelectable;
    
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}
