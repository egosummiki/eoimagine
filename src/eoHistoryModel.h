#pragma once
//
// Created by ego on 18/09/18.
//
#include <QAbstractItemModel>
#include "eoActionManager.h"

class eoHistoryModel : public QAbstractItemModel {

    Q_OBJECT

private:
    
    eoActionManager* actionManager;

public:

    eoHistoryModel(eoActionManager* actionManager);
    QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex& index) const override;
    int rowCount(const QModelIndex& parent=QModelIndex()) const override;
    int columnCount(const QModelIndex&) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;

    void beginInsertAction();
    void endInsertAction();
            
};
