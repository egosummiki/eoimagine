//
// Created by ego on 11/26/17.
//

#include <QtGui/QKeyEvent>
#include "eoInput.h"
#include "eoVector2.h"
#include "eoCanvas.h"
#include "eoMessageBox.h"

eoInput::eoInput(eoWindowMain* mainWindow)
    : mainWindow(mainWindow),
      screenMiddleWidth (mainWindow->getGlArea()->size().width()  / 2.0f),
      screenMiddleHeight(mainWindow->getGlArea()->size().height() / 2.0f) 
{
}

void eoInput::keyReleaseHandle(QKeyEvent *event) {
    if(!autoRepeat && event->isAutoRepeat())
        return;

    switch(event->key()) {
        
        case Qt::Key_Alt:
            altPressed = false;
        break;
        case Qt::Key_Shift:
            shiftPressed = false;
        break;
    }

    for (auto &&item : keyRelease) {
        item(event->key());
    }

    mainWindow->getCurrentCanvas()->requestRedraw();
}

void eoInput::keyPressHandle(QKeyEvent *event) {
    if(!autoRepeat && event->isAutoRepeat())
        return;

    switch(event->key()) {
        
        case Qt::Key_Alt:
            altPressed = true;
        break;
        case Qt::Key_Shift:
            shiftPressed = true;
        break;
    }

    for (auto &&item : keyPress) {
        item(event->key());
    }

    for (auto &&item : text) {
        item(event->text());
    }

    mainWindow->getCurrentCanvas()->requestRedraw();
}

void eoInput::buttonReleaseHandle(QMouseEvent *event) {
    if(!mainWindow->getCurrentCanvas())
        return;

    if(event->button() == Qt::LeftButton) {
        leftButtonHeld = false;
    }

    auto mouseEvent = new eoMouseEvent(event, mainWindow->getCurrentCanvas(), screenMiddleWidth, screenMiddleHeight);

    for (auto &&item : buttonRelease) {
        item(mouseEvent);
    }

    delete mouseEvent;
    mainWindow->getCurrentCanvas()->requestRedraw();
}

void eoInput::buttonPressHandle(QMouseEvent *event) {
    if(!mainWindow->getCurrentCanvas())
        return;

    if(event->button() == Qt::LeftButton) {
        leftButtonHeld = true;
    }

    auto mouseEvent = new eoMouseEvent(event, mainWindow->getCurrentCanvas(), screenMiddleWidth, screenMiddleHeight);

    for (auto &&item : buttonPress) {
        item(mouseEvent);
    }

    delete mouseEvent;
    mainWindow->getCurrentCanvas()->requestRedraw();
}

void eoInput::motionHandle(QMouseEvent *event) {
    if(!mainWindow->getCurrentCanvas())
        return;

    auto mouseEvent = new eoMouseEvent(event, mainWindow->getCurrentCanvas(), screenMiddleWidth, screenMiddleHeight);

    for (auto &item :motion) {
        item(mouseEvent);
    }

    delete mouseEvent;
    previousMousePosition = eoVector2(mouseEvent->getCanvasPosition());

    mainWindow->getCurrentCanvas()->requestRedraw();
}

int eoInput::onKeyRelease(std::function<void(const int)> callback) {
    keyRelease.emplace_back(callback);
    return static_cast<int>(keyRelease.size() - 1);
}

int eoInput::onKeyPress(std::function<void(const int)> callback) {
    keyPress.emplace_back(callback);
    return static_cast<int>(keyPress.size() - 1);
}

int eoInput::onButtonRelease(std::function<void(eoMouseEvent*)> callback) {
    buttonRelease.emplace_back(callback);
    return static_cast<int>(buttonRelease.size() - 1);
}

int eoInput::onButtonPress(std::function<void(eoMouseEvent*)> callback) {
    buttonPress.emplace_back(callback);
    return static_cast<int>(buttonPress.size() - 1);
}

int eoInput::onMotion(std::function<void(eoMouseEvent*)> callback) {
    motion.emplace_back(callback);
    return static_cast<int>(motion.size() - 1);
}

void eoInput::removeEvent(eoInputType type, int id) {
    switch (type) {
        case eoKeyRelease:
            EO_ASSERT(id < keyRelease.size(), "Removing key release event, no such id.");
            keyRelease.erase(keyRelease.begin() + id);
            break;
        case eoKeyPress:
            EO_ASSERT(id < keyPress.size(), "Removing key press event, no such id.");
            keyPress.erase(keyPress.begin() + id);
            break;
        case eoButtonRelease:
            EO_ASSERT(id < buttonRelease.size(), "Removing button release event, no such id.");
            buttonRelease.erase(buttonRelease.begin() + id);
            break;
        case eoButtonPress:
            EO_ASSERT(id < buttonPress.size(), "Removing button press event, no such id.");
            buttonPress.erase(buttonPress.begin() + id);
            break;
        case eoMotion:
            EO_ASSERT(id < motion.size(), "Removing mouse motion event, no such id.");
            motion.erase(motion.begin() + id);
            break;
        case eoText:
            EO_ASSERT(id < text.size(), "Removing text event, no such id.");
            text.erase(text.begin() + id);
            break;
    }
}

bool eoInput::isAltPressed() {
    return altPressed;
}

void eoInput::onResize(double screenWidthHalf, double screenHeightHalf) {
    this->screenMiddleWidth = static_cast<float>(screenWidthHalf);
    this->screenMiddleHeight = static_cast<float>(screenHeightHalf);

}

void eoInput::wheelHandle(QWheelEvent *event) {
    for(auto &&item : wheel) {
        item(event);
    }

}

int eoInput::onWheel(std::function<void(QWheelEvent *)> callback) {
    wheel.emplace_back(callback);
    return static_cast<int>(wheel.size() - 1);
}

int eoInput::onText(std::function<void(QString)> callback) {
    
    text.emplace_back(callback);
    return static_cast<int>(text.size() - 1);
}

bool eoInput::isLeftButtonHeld() const {
    return leftButtonHeld;
}

const eoVector2 &eoInput::getPreviousMousePosition() const {
    return previousMousePosition;
}

void eoInput::disableAutoRepeat() {
    autoRepeat = false;
}

void eoInput::enableAutoRepeat() {
    autoRepeat = true;
}

bool eoInput::isShiftPressed() {
    return shiftPressed;
}
