//
// Created by ego on 11/26/17.
//

#ifndef GFX_PROTOTYPE_EOINPUT_H
#define GFX_PROTOTYPE_EOINPUT_H

#include <vector>
#include <functional>
#include <QtGui/QKeyEvent>
#include "eoMouseEvent.h"
#include "ui/eoWindowMain.h"

enum eoInputType {
    eoKeyRelease, eoKeyPress, eoButtonRelease, eoButtonPress, eoMotion, eoText
};

class eoInput {

    std::vector<std::function<void(const int)>> keyRelease;
    std::vector<std::function<void(const int)>> keyPress;
    std::vector<std::function<void(QString)>> text;
    std::vector<std::function<void(eoMouseEvent*)>> buttonRelease;
    std::vector<std::function<void(eoMouseEvent*)>> buttonPress;
    std::vector<std::function<void(eoMouseEvent*)>> motion;
    std::vector<std::function<void(QWheelEvent*)>> wheel;

    float screenMiddleWidth;
    float screenMiddleHeight;

    eoWindowMain* mainWindow;

    bool altPressed = false;
    bool shiftPressed = false;
    bool leftButtonHeld = false;
    bool autoRepeat = true;

    eoVector2 previousMousePosition = eoVector2(0,0);
public:
    bool isLeftButtonHeld() const;

public:
    eoInput(eoWindowMain* mainWindow);

    void keyReleaseHandle(QKeyEvent *event);
    void keyPressHandle(QKeyEvent *event);
    void buttonReleaseHandle(QMouseEvent *event);
    void buttonPressHandle(QMouseEvent *event);
    void motionHandle(QMouseEvent *event);
    void wheelHandle(QWheelEvent *event);

    int onKeyRelease(std::function<void(const int)> callback);
    int onKeyPress(std::function<void(const int)> callback);
    int onButtonRelease(std::function<void(eoMouseEvent*)> callback);
    int onButtonPress(std::function<void(eoMouseEvent*)> callback);
    int onMotion(std::function<void(eoMouseEvent*)> callback);
    int onWheel(std::function<void(QWheelEvent*)> callback);
    int onText(std::function<void(QString)> callback);

    void onResize(double screenWidthHalf, double screenHeightHalf);
    void disableAutoRepeat();
    void enableAutoRepeat();

    void removeEvent(eoInputType type, int id);

    bool isAltPressed();
    bool isShiftPressed();

    const eoVector2 &getPreviousMousePosition() const;
};


#endif //GFX_PROTOTYPE_EOINPUT_H
