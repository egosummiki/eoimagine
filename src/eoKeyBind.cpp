//
// Created by ego on 11/30/17.
//

#include "eoKeyBind.h"
#include "ui/eoWindowMain.h"
#include <string>
#include <utility>

eoKeyBind::eoKeyBind(eoWindowMain* windowMain) : windowMain(windowMain) {
}

void eoKeyBind::setBinding(const char *binding, std::function<void(void)> callback) {
    auto shortcut = new QShortcut(QString(binding), windowMain);
    QObject::connect(shortcut, &QShortcut::activated, callback);

    bindings.emplace_back(shortcut);
}
