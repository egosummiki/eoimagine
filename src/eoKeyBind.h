//
// Created by ego on 11/30/17.
//

#ifndef GFX_PROTOTYPE_EOKEYSHORTCUT_H
#define GFX_PROTOTYPE_EOKEYSHORTCUT_H

#include <QShortcut>
#include <QString>
#include <vector>
#include <functional>

class eoWindowMain;

class eoKeyBind {
    eoWindowMain* windowMain;
    std::vector<QShortcut*> bindings;

public:

    explicit eoKeyBind(eoWindowMain* windowMain);
    void setBinding(const char* binding, std::function<void(void)> callback);
};


#endif //GFX_PROTOTYPE_EOKEYSHORTCUT_H
