#include <utility>

//
// Created by ego on 11/24/17.
//

#include "eoLayer.h"


eoLayer::eoLayer(eoCanvas *canvas, QString name, eoLayerAttributeSet attributes)
    : canvas(canvas), attributes(attributes), name(std::move(name)), widgetItem(nullptr), id(-1)
{

}

eoLayer::eoLayer(eoCanvas *canvas, QString name) : eoLayer(canvas, std::move(name), eoLayer::NONE)
{
}

eoLayer::~eoLayer() {

    delete widgetItem;
}

void eoLayer::onRender()
{

}

bool eoLayer::hasAttribute(eoLayerAttributeSet attribute) {
    return (attributes & attribute) != 0;
}

const QString &eoLayer::getName() const {
    return name;
}

QTreeWidgetItem *eoLayer::getWidgetItem() const {
    return widgetItem;
}

void eoLayer::setWidgetItem(QTreeWidgetItem *widgetItem) {
    eoLayer::widgetItem = widgetItem;
}

void eoLayer::setCurrent() {
    canvas->setCurrentLayer(this);

}

void eoLayer::remove() {
    canvas->removeLayer(this);

}

int eoLayer::getId() {
    return id;
}

void eoLayer::setId(int id) {
    this->id = id;
}

void eoLayer::setAttribute(eoLayerAttributeSet attribute) {
   attributes |= attribute; 
}

void eoLayer::removeAttribute(eoLayerAttributeSet attribute) {
   attributes &= ~attribute; 
}

QModelIndex& eoLayer::getModelIndex() {
    
    return modelIndex;
}

QModelIndex& eoLayer::setModelIndex(QModelIndex index) {
    
    return modelIndex = index;
}
