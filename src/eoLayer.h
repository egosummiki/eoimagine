//
// Created by ego on 11/24/17.
//

#ifndef GFX_PROTOTYPE_EOLAYER_H
#define GFX_PROTOTYPE_EOLAYER_H

#include "eoGLProgram.h"
#include "eoCanvas.h"
#include "eoModuleTarget.h"
#include <QTreeWidgetItem>

typedef unsigned int
    eoLayerAttributeSet;

class eoLayer : public eoModuleTarget {

    friend eoLayerManager;

    int id;

protected:
    eoLayerAttributeSet attributes;
    QString name;
    QTreeWidgetItem* widgetItem;
    eoCanvas* canvas;
    QModelIndex modelIndex = QModelIndex();

public:

    eoLayer(eoCanvas *canvas, QString name, eoLayerAttributeSet attributes);
    explicit eoLayer(eoCanvas *canvas, QString name);
    ~eoLayer();

    virtual void onRender();
    bool hasAttribute(eoLayerAttributeSet attribute);
    void setAttribute(eoLayerAttributeSet attribute);
    void removeAttribute(eoLayerAttributeSet attribute);
    const QString &getName() const;
    QTreeWidgetItem *getWidgetItem() const;
    void setWidgetItem(QTreeWidgetItem *widgetItem);
    void setCurrent();
    void remove();
    int getId();
    QModelIndex& getModelIndex();
    QModelIndex& setModelIndex(QModelIndex index);
    void setId(int id);

    enum eoLayerAttribute : unsigned int {
        NONE        = 0,
        HIDDEN      = (1 << 0),
        RASTER      = (1 << 1),
        IRREMOVABLE = (1 << 2),
        INVISIBLE   = (1 << 3),
        TEXT        = (1 << 4),
    };

private:



};


#endif //GFX_PROTOTYPE_EOLAYER_H
