//
// Created by ego on 15/10/18.
//

#include "eoLayerRaster.h"
#include "tools/eoTool.h"
#include "eoLayerManager.h"
#include "eoLayerTreeModel.h"
#include "ui/eoWindowMain.h"
#include "ui/eoWidgetLayers.h"
#include "eoMessageBox.h"


eoLayerManager::eoLayerManager(eoWindowMain *mainWindow, eoRender *render, eoVector2 canvasDimension)
    :   treeModel(new eoLayerTreeModel(this, &layers)),
        mainWindow(mainWindow), 
        render(render),
        frontFrameBuffer(new eoFrameBuffer(canvasDimension)),
        backFrameBuffer(new eoFrameBuffer(canvasDimension))
{
}

eoLayerManager::~eoLayerManager() {
    
    /* Delete all the layers */
    for(auto &layer :layers)
        delete layer;

    /* Clear the layers vector */
    layers.clear();

    delete frontFrameBuffer;
    delete backFrameBuffer;
}

void eoLayerManager::onRender() {
    
    /* Layer render */
    for(auto &layer :layers)
    {
        /* Skip invisible layers. */
        if(layer->hasAttribute(eoLayer::INVISIBLE))
            continue;
        
        /* Render the texture to the backbuffer */
        backFrameBuffer->begin(); {

            /* Render the old buffer first */
            eoGLProgram::TRIVIAL->use(); 
            eoGLProgram::TRIVIAL->setTransform(eoMatrix());
            frontFrameBuffer->bind();
            eoVertexBuffer::FULL_DIMENSION->render(); 

            /* On top of the old buffer draw the layer contents. */
            layer->onRender();
        }
        backFrameBuffer->end();

        /* Swap canvas buffers */
        swapBuffers();
    }
}

/*
 * Add new layer to canvas.
 */
void eoLayerManager::addLayer(eoLayer *layer) {
    // Layer null check.
    EO_ASSERT(layer, "Attempted to add a null layer.");

    // Let newly added layer know its id.
    layer->setId(layers.size());

    if(layer->hasAttribute(eoLayer::HIDDEN)) {
        
        // If the layer is hidden just insert it.
        treeModel->insert(layer, nullptr);
    } else
    { 
        // If layer is shown increase the layer count.
        // Insert the layer into the tree model and assign it a model index.
        treeModel->insert(layer, &visibleCount);
        treeModel->index(0, 0);
    } 

    // Set the layer as current.
    setCurrentLayer(layer);
}


eoLayer *eoLayerManager::getCurrentLayer() {
    return currentLayer;
}

/*
 * Sets the given layer as current and informs the widget to update selection.
 * */
void eoLayerManager::setCurrentLayer(eoLayer *layer) {

    // No need to change anything if the layer is already current.
    if(currentLayer == layer)
        return;

    currentLayer = layer;

    // If canvas is current, we need to inform the ui about it.
    if(render->getCanvas() == this) {

        auto layerWidget = mainWindow->getLayerWidget();

        layerWidget->selectRow(layer->modelIndex.row());
        
        if(layer->hasAttribute(eoLayer::RASTER)) {

            auto rasterLayer = static_cast<eoLayerRaster*>(layer);

            layerWidget->getBlendingCombo()
                ->setCurrentIndex(rasterLayer->getBlendingMode());

            layerWidget->getOpacitySlider()
                ->setValue(rasterLayer->getOpacity()*100.0f);
        }

    }

    auto tool = render->getCurrentTool();

    if(tool)
        tool->onLayerChange();
}

unsigned long eoLayerManager::getLayerCount() {
    return layers.size();
}

eoLayer *eoLayerManager::getLayer(int index) {

    EO_ASSERT_NPTR(index >= 0 && index < getLayerCount(), "eoLayerManager::getLayer(int) - No such layer.");

    return layers[index];
}

void eoLayerManager::removeLayer(eoLayer *layer) {
    int id;

    // Layer null check.
    EO_ASSERT(layer, "Attempted to remove a null layer.");

    // Can't remove irremovable layer.
    if(layer->hasAttribute(eoLayer::IRREMOVABLE))
        return;

    id = layer->getId();

    // If layer is shown decrease the layer count.
    // Remove the layer either case
    if(layer->hasAttribute(eoLayer::HIDDEN))  {

        treeModel->remove(layer, nullptr);
    } else {

        treeModel->remove(layer, &visibleCount);
        reassignIndexes();
    }
    delete layer;

    setCurrentLayer(layers[id-1]);
}

void eoLayerManager::moveCurrentLayerUp() {

    treeModel->moveUp(currentLayer);
}

void eoLayerManager::moveCurrentLayerDown() {

    treeModel->moveDown(currentLayer);
}

int eoLayerManager::getVisibleCount() {
   
    return visibleCount;
}

/* Returns a layer for a given row number */
eoLayer* eoLayerManager::rowToLayer(int row) {

    int index;
    // Rows are displayed upside down
    // in regard to display queue.
    index = visibleCount - row;
    
    for(auto &layer :layers) {

        // Skip the hidden layers.
        if(layer->hasAttribute(eoLayer::HIDDEN))
            continue;

        // Decrease the index, if 0 return layer.
        if(!(--index))
            return layer;

    }

    // Row higher than number of shown layers?
    EO_FAIL("eoLayerManager::rowToLayer(int) - Wrong layer count. Higher row than shown layers.");
    return nullptr;
}

void eoLayerManager::onSetCurrent() {

    mainWindow->getLayerWidget()->getLayerTree()->setModel(treeModel);
    mainWindow->getLayerWidget()->connectSelectionSignal();

    if(visibleCount) {

        emit mainWindow->getLayerWidget()->getLayerTree()
                ->dataChanged(treeModel->index(0, 0), treeModel->index(visibleCount-1, 1));
    }

    if(currentLayer)
        mainWindow->getLayerWidget()->selectRow(currentLayer->getModelIndex().row());
}

void eoLayerManager::swapBuffers() {
    
    std::swap(frontFrameBuffer, backFrameBuffer);
}

eoFrameBuffer *eoLayerManager::getFrameBuffer() {
    
    return frontFrameBuffer;
}

void eoLayerManager::toggleLayerVisibility(eoLayer *layer) {
    
    if(layer->hasAttribute(eoLayer::INVISIBLE)) {

        layer->removeAttribute(eoLayer::INVISIBLE);
    } else {

        layer->setAttribute(eoLayer::INVISIBLE);
    }

    mainWindow->getGlArea()->update();
    emit mainWindow->getLayerWidget()->getLayerTree()->dataChanged(layer->getModelIndex(), layer->getModelIndex());
}

void eoLayerManager::reassignIndexes() {

    int i;
    
    for(i = 0; i < visibleCount; ++i) {

        treeModel->index(i, 0);
    }
}
