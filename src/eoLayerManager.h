#pragma once
//
// Created by ego on 15/10/18.
//
#include <vector>
#include "eoFrameBuffer.h"
#include "eoVector2.h"


class eoLayerTreeModel;
class eoWindowMain;
class eoLayer;
class eoRender;

class eoLayerManager {

private:

    eoLayer* currentLayer;
    eoLayerTreeModel* treeModel;
    int visibleCount = 0;
    eoWindowMain *mainWindow;
    eoRender *render;
    eoFrameBuffer* frontFrameBuffer;
    eoFrameBuffer* backFrameBuffer;
    std::vector<eoLayer*> layers;

public:

    eoLayerManager(eoWindowMain *mainWindow, eoRender *render, eoVector2 canvasDimension);
    ~eoLayerManager();

    void onRender();
    void addLayer(eoLayer* layer);
    unsigned long getLayerCount();
    eoLayer* getCurrentLayer();
    void setCurrentLayer(eoLayer* layer);
    eoLayer* getLayer(int index);
    void removeLayer(eoLayer* layer);
    void moveCurrentLayerUp();
    void moveCurrentLayerDown();
    eoLayer* rowToLayer(int row);
    int getVisibleCount();
    void onSetCurrent();
    void swapBuffers();
    void toggleLayerVisibility(eoLayer *layer);
    void reassignIndexes();

    eoFrameBuffer *getFrameBuffer();
};
