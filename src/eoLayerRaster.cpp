//
// Created by ego on 12/7/17.
//

#include <unistd.h>
#include <cstring>
#include "eoLayerRaster.h"
#include "eoInput.h"
#include "shaders/eoShaderFile.h"
#include "shaders/eoShaderVertexTrivial.h"
#include "shaders/eoShaderCanvasDimension.h"
#include "eoTexture.h"
#include "actions/eoActionAlterLayer.h"
#include "eoBlending.h"

eoLayerRaster::eoLayerRaster(eoCanvas *canvas, eoColor fill, QString name) :
    eoLayerRaster(
            canvas,
            std::move(name),
            fill,
            canvas->getWidth(),
            canvas->getHeight())
{
}

eoLayerRaster::eoLayerRaster(eoCanvas *canvas, QString name, eoColor fill, float width, float height)
    : eoLayerTrivial(canvas, std::move(name), eoLayer::RASTER, width, height),
      backFrameBuffer   (eoFrameBuffer::createPersevering(canvas->getDimension(), GL_COLOR_ATTACHMENT1)),
      frontFrameBuffer  (eoFrameBuffer::createPersevering(canvas->getDimension(), GL_COLOR_ATTACHMENT1))
{
    eoBlending::disable();

    fill.setAsClearColor();

    frontFrameBuffer->begin();
    glClear(GL_COLOR_BUFFER_BIT);
    frontFrameBuffer->end();

    backFrameBuffer->begin();
    glClear(GL_COLOR_BUFFER_BIT);
    backFrameBuffer->end();

    canvas->applyClearColor();

    updateFill();
}

eoLayerRaster::eoLayerRaster(eoCanvas *canvas, QString name, eoTexture *texture)
        : eoLayerTrivial(canvas, std::move(name), eoLayer::RASTER, texture->getWidth(), texture->getHeight()),
          backFrameBuffer(eoFrameBuffer::createPersevering(getDimension(), GL_COLOR_ATTACHMENT1)),
          frontFrameBuffer(eoFrameBuffer::createPersevering(getDimension(), GL_COLOR_ATTACHMENT1))
{

    eoBlending::apply(eoBlending::NONE);

    frontFrameBuffer->begin(); {

        eoGLProgram::PREMUL->use();
        eoGLProgram::PREMUL->setTransform(eoMatrix());
        texture->bind();
        eoVertexBuffer::FULL_DIMENSION->render();
    }
    frontFrameBuffer->end();

    backFrameBuffer->begin(); {

        eoGLProgram::PREMUL->use();
        eoGLProgram::PREMUL->setTransform(eoMatrix());
        texture->bind();
        eoVertexBuffer::FULL_DIMENSION->render();
    }
    backFrameBuffer->end();


    updateFill();

    //frontFrameBuffer->toQImage().save("/home/ego/.tmp/log.front.png");
    //backFrameBuffer->toQImage().save("/home/ego/.tmp/log.back.png");
}

eoLayerRaster::~eoLayerRaster() {
    delete backFrameBuffer;
    delete frontFrameBuffer;
    delete program;
} 

void eoLayerRaster::onRender() {

    eoBlending::apply(eoBlending::NONE);

    switch(drawingMode) {
    
        case DEFAULT:
            modeDefault();
            return;

        case ALTER_ONCE:
            modeOnce();
            return;

        case HOLD:
            modeHold();
            return;

        case RELEASE:
            modeRelease();
            return;

        case ALTERNATIONS:
            modeAlternations();
    
        default: return;
    }
}

void eoLayerRaster::alterBackBuffer() const {

    backFrameBuffer->begin(); {

        glEnable(GL_BLEND);
        alterLayerCallback(frontFrameBuffer->getTexture());
        glDisable(GL_BLEND);
    }
    backFrameBuffer->end();
}

void eoLayerRaster::swapBuffers() {

    std::swap(backFrameBuffer, frontFrameBuffer);

}

void eoLayerRaster::onAlterLayer(std::function<void(GLuint previousTexture)> callback) {
    alterLayerCallback = std::move(callback);
}

void eoLayerRaster::clearAlterLayer() {
    alterLayerCallback = nullptr;
    alterations = 0;
    drawingMode = DEFAULT;
}

void eoLayerRaster::update() {
    drawingMode = ALTERNATIONS;
    alterations++;
}

void eoLayerRaster::begin() {
    drawingMode = HOLD;
}

void eoLayerRaster::end() {
    drawingMode = RELEASE;
}

void eoLayerRaster::alterLayerOnce(std::function<void(GLuint previousTexture)> callback) {
    
    alterLayerOnceCallback = std::move(callback);
    drawingMode = ALTER_ONCE;
}


void eoLayerRaster::updateFill() {

    auto fragment   = generateShader(blendingMode);
    auto vertex     = new eoShaderCanvasDimension(canvas);

    delete program;

    program = new eoGLProgram();
    program->attachShader(fragment);
    program->attachShader(vertex);
    program->link();

    delete fragment;
    delete vertex;
}

void eoLayerRaster::updateTransform() {
    
    transform = generateMatrix();
}

eoMatrix& eoLayerRaster::getTransformMatrix() {
    
    return transform;
}

void eoLayerRaster::renderBuffer(eoFrameBuffer *drawBuffer) {
    program->use();
    program->setTransform(transform);
    program->setTexture("source_texture", 0);
    program->setTexture("dest_texture", 1);
    program->setFloat("opacity", opacity);
    program->setVec2("canvasMid", canvas->getDimension() / 2.0f);
    program->setVec2("sizeMid", getDimension() / 2.0f);
    program->setVec2("position", getTranslateModule()->getVector2("position"));
    program->setVec2("scale", getScaleModule()->getVector2("scale"));
    program->setFloat("rotation", getRotateModule()->getFloat("rotation"));

    drawBuffer->bind();
    canvas->getFrameBuffer()->bind(GL_TEXTURE1);

    renderModules(program);
    eoLayerTrivial::getVertexBuffer()->render();
    
}

void eoLayerRaster::modeOnce() {
    
    drawingMode = DEFAULT;

    if(!alterLayerOnceCallback)
        return;

    backFrameBuffer->begin(); {

        glEnable(GL_BLEND);
        alterLayerOnceCallback(frontFrameBuffer->getTexture());
        glDisable(GL_BLEND);
    }
    backFrameBuffer->end();
    swapBuffers();

    renderBuffer(frontFrameBuffer);
}

void eoLayerRaster::modeHold() {

    backFrameBuffer->begin(); {

        /* For the hold mode, we need to render the old buffer each time. */
        eoGLProgram::TRIVIAL->use();
        eoGLProgram::TRIVIAL->setTransform(eoMatrix());
        frontFrameBuffer->bind();
        eoVertexBuffer::FULL_DIMENSION->render();

        glEnable(GL_BLEND);
        alterLayerCallback(frontFrameBuffer->getTexture());
        glDisable(GL_BLEND);
    }
    backFrameBuffer->end();

    renderBuffer(backFrameBuffer);
}

char drawActionName[]
    = "Brush Stroke";

void eoLayerRaster::modeRelease() {

    eoFrameBuffer *oldBuffer;

    drawingMode = DEFAULT; 
    oldBuffer   = frontFrameBuffer->clone();

    if(lastAlteration)
        lastAlteration->updatePostAlter(oldBuffer);

    swapBuffers();

    lastAlteration = new eoActionAlterLayer(drawActionName, this, oldBuffer, frontFrameBuffer);
    canvas->getActionManager()->silentPerform(lastAlteration);

    renderBuffer(frontFrameBuffer);
}

void eoLayerRaster::modeAlternations() {

    while(alterations--) {

        alterBackBuffer();
        swapBuffers();
    }
    
    renderBuffer(frontFrameBuffer);
}

void eoLayerRaster::modeDefault() {

    renderBuffer(frontFrameBuffer);
}

/* Very dangerous method, this shell be only used in eoActionAlterLayer. */
void eoLayerRaster::setFrontBuffer(eoFrameBuffer* replacment) {
    
    frontFrameBuffer = replacment;
}

void eoLayerRaster::setOpacity(float opacity) {
    
    this->opacity = opacity;
}

void eoLayerRaster::setBlendingMode(eoShaderModifiers::BlendingMode blending) {
    
    blendingMode = blending;
    updateFill();
}

eoShaderModifiers::BlendingMode eoLayerRaster::getBlendingMode() {
   
    return blendingMode;
}

eoFrameBuffer *eoLayerRaster::getFrontBuffer() {
    
    return frontFrameBuffer;
}
