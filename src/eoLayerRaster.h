//
// Created by ego on 12/7/17.
//

#ifndef GFX_PROTOTYPE_EOLAYERRASTER_H
#define GFX_PROTOTYPE_EOLAYERRASTER_H


#include <GL/gl.h>
#include <functional>
#include "eoLayerTrivial.h"
#include "eoFrameBuffer.h"
#include "eoTexture.h"
#include "eoColor.h"
#include "eoModuleTarget.h"

class eoActionAlterLayer;


class eoLayerRaster : public eoLayerTrivial {

public:
    
    enum DrawingMode {
        DEFAULT,
        ALTER_ONCE,
        HOLD,
        RELEASE,
        ALTERNATIONS
    };


private:

    std::function<void(GLuint previousTexture)> alterLayerCallback;
    std::function<void(GLuint previousTexture)> alterLayerOnceCallback;

    eoFrameBuffer* frontFrameBuffer;
    eoFrameBuffer* backFrameBuffer;

    int alterations = 0;

    eoGLProgram *program = nullptr;
    eoMatrix transform;

    float opacity = 1.0f;
    eoShaderModifiers::BlendingMode blendingMode = eoShaderModifiers::NORMAL;

    DrawingMode drawingMode;

    eoActionAlterLayer *lastAlteration = nullptr;

public:


    eoLayerRaster(eoCanvas *canvas, eoColor fill, QString name);
    ~eoLayerRaster();
    eoLayerRaster(eoCanvas *canvas, QString name, eoColor fill, float width, float height);
    eoLayerRaster(eoCanvas *canvas, QString name, eoTexture *texture);
    void onRender() override;
    void onAlterLayer(std::function<void(GLuint previousTexture)> callback);
    void alterLayerOnce(std::function<void(GLuint previousTexture)> callback);
    void begin();
    void end();
    void update();
    void clearAlterLayer();
    eoMatrix& getTransformMatrix();

    void swapBuffers();

    void alterBackBuffer() const;

    void setFrontBuffer(eoFrameBuffer* replacment);

    void updateFill() override;
    void updateTransform() override;

    void renderBuffer(eoFrameBuffer *drawBuffer);
    void setOpacity(float opacity);
    void setBlendingMode(eoShaderModifiers::BlendingMode blending);
    eoShaderModifiers::BlendingMode getBlendingMode();
    float getOpacity();
    eoFrameBuffer *getFrontBuffer();

    void modeDefault();
    void modeOnce();
    void modeHold();
    void modeRelease();
    void modeAlternations();
};


#endif //GFX_PROTOTYPE_EOLAYERRASTER_H
