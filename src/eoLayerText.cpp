//
// Created by ego on 30/10/18.
//

#include "eoLayerText.h"

eoLayerText::eoLayerText(eoCanvas *canvas, QString name, eoFont *font) 
    : eoLayer(canvas, std::move(name), eoLayer::TEXT), font(font)
{
    
}

void eoLayerText::onRender() {
    
    font->draw(text, transform, textColor, sepChar, sepSpace, sepLine);
}

void eoLayerText::setText(std::string value) {
    
    text = std::move(value);
}

void eoLayerText::updateTransform() {
    
    transform = generateMatrix();
}

void eoLayerText::appendText(std::string addition) {
    
    text.append(addition);
}

void eoLayerText::popChar() {

    if(text.empty())
        return;

    while((text.back() & 0b11000000) == 0b10000000)
        text.pop_back();
    
    text.pop_back();
}

void eoLayerText::setFont(eoFont* replace) {
    
    font->release();
    font = replace;
}

void eoLayerText::setColor(eoColor color) {
    
    textColor = color;
}

void eoLayerText::setSeparation(int sc, int ss, int sl) {
    
    sepChar     = sc;
    sepLine     = sl;
    sepSpace    = ss;
}
