#pragma once
//
// Created by ego on 30/10/18.
//
#include "eoMatrix.h"
#include "eoLayer.h"
#include "eoCanvas.h"
#include "eoFont.h"
#include <string>

class eoLayerText : public eoLayer {

    std::string text;
    eoFont *font;
    eoMatrix transform;
    eoColor textColor = eoColor::BLACK;
    int sepChar = 0, sepLine = 0, sepSpace = 0;

public:
    
    eoLayerText(eoCanvas *canvas, QString name, eoFont *font);
    
    void onRender() override;
    void setText(std::string value);
    void appendText(std::string addition);
    void popChar();
    void updateTransform() override;
    void setFont(eoFont* replace);
    void setColor(eoColor color);
    void setSeparation(int sc, int ss, int sl);
};
