//
// Created by ego on 11/09/18.
//

#include "eoLayerTreeDelegate.h"
#include "ui/eoWidgetLayers.h"
#include <QToolButton>

eoLayerTreeDelegate::eoLayerTreeDelegate(QObject *parent) 
    : QStyledItemDelegate(parent)
{
    
}

void eoLayerTreeDelegate::paint(QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index) const {

    QIcon icon;
    eoLayer* layer;
    int width, height;
    QRect rect;

    layer = static_cast<eoLayer*>(index.internalPointer());

    if(!layer) {
        QStyledItemDelegate::paint(painter, option, index);
        return;
    }

    width   = option.rect.width();
    height  = option.rect.height();

    if(layer->hasAttribute(eoLayer::INVISIBLE))
        icon = eoWidgetLayers::iconInvisible;
    else
        icon = eoWidgetLayers::iconVisible;

    rect = QRect(
            option.rect.x() + width / 2 - height / 2,
            option.rect.y(),
            height,
            height );

    painter->save();
    painter->setRenderHint(QPainter::SmoothPixmapTransform);

    if (option.state & QStyle::State_Selected)
                painter->fillRect(option.rect, option.palette.highlight());

    painter->drawPixmap(rect, icon.pixmap(QSize(64, 64)));
    painter->restore();
    
}
