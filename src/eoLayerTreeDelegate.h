#pragma once
//
// Created by ego on 11/09/18.
//
#include <QStyledItemDelegate>
#include <QWidget>

class eoLayerTreeDelegate : public QStyledItemDelegate {

public:
    eoLayerTreeDelegate(QObject *parent = nullptr);

    void paint(QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index) const override;

};
