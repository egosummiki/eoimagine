//
// Created by ego on 01/09/18.
//

#include "eoLayerTreeModel.h"
#include "ui/eoWidgetLayers.h"
#include "eoLayer.h"

eoLayerTreeModel::eoLayerTreeModel(eoLayerManager* canvas, vector<eoLayer*>* layers)
    : canvas(canvas), layers(layers)
{
}

QModelIndex eoLayerTreeModel::index(int row, int column, const QModelIndex& parent) const  {

    eoLayer* layer;

    // No parents and children so far.
    if(parent != QModelIndex())
        return QModelIndex();
    
    // Calculate which layer coresponds to a given row.
    layer = canvas->rowToLayer(row);

    // This should not happen.
    if(!layer)
        return QModelIndex();

    // Assign the model index to a layer and return it.
    return layer->setModelIndex(createIndex(row, column, layer));
}

QModelIndex eoLayerTreeModel::parent(const QModelIndex& index) const  {
    
    return QModelIndex();
}

int eoLayerTreeModel::rowCount(const QModelIndex& parent) const  {
    
    if(parent != QModelIndex())
        return 0;

    return canvas->getVisibleCount();
}

int eoLayerTreeModel::columnCount(const QModelIndex&) const  {
    return 2;
}

QVariant eoLayerTreeModel::data(const QModelIndex &index, int role) const  {

    /* Model data:
     *  
     *  COLUMN 0 - Visibility button - Custom delegate.
     *  COLUMN 1 - Layer name.
     *
     * */

    if(!index.isValid())
        return QVariant();

    switch(index.column()){
        
        case 1:
            if(role == Qt::DisplayRole) {
                if(!index.internalPointer())
                    return QVariant();
                
                return QVariant(static_cast<eoLayer*>(index.internalPointer())->getName());
            }
        default: break;
    }

    return QVariant();
}


void eoLayerTreeModel::insert(eoLayer* layer, int *counter) {
    
    // Inform the model that the layer is going to be inserted and perform emplacment.
    // The tree view displays the elements in reverse order. 
    // So emplacing back will insert the row in the model at 0.

    beginInsertRows(QModelIndex(), 0, 0);
    layers->emplace_back(layer);
    counter && ++(*counter);
    endInsertRows();
}


void eoLayerTreeModel::remove(eoLayer *layer, int *counter) {

    // Likewise as insert.

    int row = layer->getModelIndex().row();
 
    beginRemoveRows(QModelIndex(), row, row);
    layers->erase(layers->begin() + layer->getId());
    counter && --(*counter);
    endRemoveRows();
}

void eoLayerTreeModel::moveUp(eoLayer* layer) {
    
    int row, rowOther, id, idOther;
    eoLayer* other;

    // Get the row of the layer
    row = layer->getModelIndex().row();

    // Check if it can be moved down
    if(row <= 0)
        return;

    id = layer->getId();                    // Id of the layer
    rowOther = row-1;                       // Row of the layer above
    other = canvas->rowToLayer(rowOther);   // Get layer out of the row
    idOther = other->getId();               // Id of the layer above.

    // Inform Qt that the layers are going to be moved
    beginMoveRows(QModelIndex(), row, row, QModelIndex(), rowOther);
    {
        // And swap them
        layers->at(id) = other;
        other->setId(id);
        layers->at(idOther) = layer;
        layer->setId(idOther);
    }
    endMoveRows();
}

void eoLayerTreeModel::moveDown(eoLayer* layer) {

    int row, rowOther, id, idOther;
    eoLayer* other;

    // Get the row of the layer
    row = layer->getModelIndex().row();
    // Row of the layer above
    rowOther = row+1;                       

    // Check if it can be moved up
    if(rowOther >= canvas->getVisibleCount())
        return;

    id = layer->getId();                    // Id of the layer
    other = canvas->rowToLayer(rowOther);   // Get layer out of the row
    idOther = other->getId();               // Id of the layer above.

    // Inform Qt that the layers are going to be moved
    beginMoveRows(QModelIndex(), rowOther, rowOther, QModelIndex(), row);
    {
        // And swap them
        layers->at(id) = other;
        other->setId(id);
        layers->at(idOther) = layer;
        layer->setId(idOther);
    }
    endMoveRows();
}

Qt::ItemFlags eoLayerTreeModel::flags(const QModelIndex &index) const {
    
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}
