#pragma once
//
// Created by ego on 01/09/18.
//

#include <QAbstractItemModel>
#include <vector>
#include "ui/eoWindowMain.h"
#include "eoLayer.h"
#include "eoLayerManager.h"

using std::vector;

class eoLayerTreeModel : public QAbstractItemModel {

    Q_OBJECT

private:

    eoLayerManager* canvas;
    vector<eoLayer*>* layers;

public:

    eoLayerTreeModel(eoLayerManager* canvas, vector<eoLayer*>* layers);

    QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex& index) const override;
    int rowCount(const QModelIndex& parent=QModelIndex()) const override;
    int columnCount(const QModelIndex&) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    void insert(eoLayer* layer, int *counter);
    void remove(eoLayer* layer, int *counter);
    void moveUp(eoLayer* layer);
    void moveDown(eoLayer* layer);

};
