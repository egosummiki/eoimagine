#include <utility>

//
// Created by ego on 11/26/17.
//

#include "eoLayerTrivial.h"
#include "shaders/eoShaderFragmentTrivial.h"
#include "shaders/eoShaderCanvasDimension.h"

eoLayerTrivial::eoLayerTrivial(eoCanvas *canvas, QString name, eoLayerAttributeSet attributes)
    : eoLayerTrivial(canvas, std::move(name), attributes, canvas->getWidth(), canvas->getHeight())
{
}

eoLayerTrivial::eoLayerTrivial(eoCanvas *canvas, QString name, eoLayerAttributeSet attributes, float width,
                               float height)
        : eoLayer(canvas, std::move(name), attributes), dimension(width, height)
{

    vertexBuffer = eoVertexBuffer::square(dimension);

    auto vertex = new eoShaderCanvasDimension(canvas);

    program = new eoGLProgram();
    program->attachShader(eoShader::FRAGMENT_NORMAL);
    program->attachShader(vertex);
    program->link();

    delete vertex;
}

eoLayerTrivial::~eoLayerTrivial() {

    delete vertexBuffer;
    delete program;

}

void eoLayerTrivial::onRender() {
    program->use();
    program->setTransform(eoMatrix());
    vertexBuffer->render();
}

eoVector2 eoLayerTrivial::toLayerSpace(eoVector2 point) {

    return traceBack(point * dimension / canvas->getDimension());
}

float eoLayerTrivial::getWidth() const {
    return dimension.getX();
}

float eoLayerTrivial::getHeight() const {
    return dimension.getY();
}

eoVertexBuffer *eoLayerTrivial::getVertexBuffer() const {
    return vertexBuffer;
}

eoGLProgram *eoLayerTrivial::getProgram() const {
    return program;
}

const eoVector2 &eoLayerTrivial::getDimension() const {
    return dimension;
}


