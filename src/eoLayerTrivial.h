//
// Created by ego on 11/26/17.
//

#ifndef GFX_PROTOTYPE_EOLAYERTRIVIAL_H
#define GFX_PROTOTYPE_EOLAYERTRIVIAL_H

#include "eoLayer.h"
#include "eoVertexBuffer.h"
#include "eoGLProgram.h"

class eoLayerTrivial : public eoLayer {

private:
    eoVertexBuffer* vertexBuffer;
    eoGLProgram* program;
    eoVector2 dimension;

public:
    eoLayerTrivial(eoCanvas *canvas, QString name, eoLayerAttributeSet attributes);
    eoLayerTrivial(eoCanvas *canvas, QString name, eoLayerAttributeSet attributes, float width,
                       float height);
    ~eoLayerTrivial();

    float getWidth() const;
    float getHeight() const;

    void onRender() override;
    eoVector2 toLayerSpace(eoVector2 point);

    eoVertexBuffer *getVertexBuffer() const;
    eoGLProgram *getProgram() const;
    const eoVector2 &getDimension() const;

};


#endif //GFX_PROTOTYPE_EOLAYERTRIVIAL_H
