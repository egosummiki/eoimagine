//
// Created by ego on 11/24/17.
//

#include "eoLayerZero.h"
#include "eoRender.h"
#include "shaders/eoShaderVertexTrivial.h"
#include "shaders/eoShaderFile.h"
#include "shaders/eoShaderCanvasDimension.h"

/*
 * This layer is designed to fill the canvas buffer with transparent color.
 * */

eoLayerZero::eoLayerZero(eoCanvas *canvas, eoLayerAttributeSet attributes)
    : eoLayerTrivial(canvas, "Zero Layer", eoLayer::HIDDEN | eoLayer::IRREMOVABLE | attributes)
{}


eoLayerZero::eoLayerZero(eoCanvas* canvas) : eoLayerZero(canvas, eoLayer::NONE)
{}

void eoLayerZero::onRender() {

    eoColor::TRANSPARENT.setAsClearColor();
    glClear(GL_COLOR_BUFFER_BIT);
    eoRender::setFillColor();
}

