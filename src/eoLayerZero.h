//
// Created by ego on 11/24/17.
//

#ifndef GFX_PROTOTYPE_EOLAYERZERO_H
#define GFX_PROTOTYPE_EOLAYERZERO_H

#include "eoLayerTrivial.h"

class eoLayerZero : public eoLayerTrivial {
public:
    explicit eoLayerZero(eoCanvas* canvas);
    eoLayerZero(eoCanvas *canvas, eoLayerAttributeSet attributes);

    void onRender() override;
};


#endif //GFX_PROTOTYPE_EOLAYERZERO_H
