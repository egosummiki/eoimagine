//
// Created by ego on 8/13/18.
//

#include "eoMap.h"

eoMap::eoMap()
    : head(nullptr)
{
}

void eoMap::add(eoMap::Item* item) {

    Item** current = &head;

    while(*current)
        current = &((*current)->next);

    *current = item;
}

void eoMap::add(std::string uniform, float value) {
    add(new ItemFloat(std::move(uniform), value));

}

void eoMap::add(std::string uniform, eoVector2 value) {
    add(new ItemVector2(std::move(uniform), value));
}

eoMap::Item *eoMap::operator[](int id) {

    Item* item = head;

    while(id-- && item) {
       item = item->next;
    }

    return item;
}

eoMap::Item *eoMap::operator[](std::string name) {

    Item* item = head;

    while(item) {
        if(item->name == name) {
            return item;
        }

        item = item->next;
    }

    return nullptr;
}

void eoMap::applyToProgram(eoGLProgram *program) {

    Item* item = head;

    while(item) {
        item->applyToProgram(program);
        item = item->next;
    }
}

void eoMap::set(std::string name, float value) {

    Item* item = operator[](std::move(name));
    if(!item || item->getType() != eoType::FLOAT)
        return;

    dynamic_cast<ItemFloat*>(item)->setValue(value);
}

void eoMap::set(std::string name, eoVector2 value) {

    Item* item = operator[](std::move(name));
    if(!item || item->getType() != eoType::VEC2)
        return;

    dynamic_cast<ItemVector2*>(item)->setValue(value);
}

void eoMap::set(int index, float value) {
    
    Item* item = operator[](index);
    if(!item || item->getType() != eoType::FLOAT)
        return;

    dynamic_cast<ItemFloat*>(item)->setValue(value);
}

void eoMap::set(int index, eoVector2 value) {
    
    Item* item = operator[](index);
    if(!item || item->getType() != eoType::VEC2)
        return;

    dynamic_cast<ItemVector2*>(item)->setValue(value);
}

eoMap::Item::Item(eoType type, std::string uniform)
    : type(type), name(std::move(uniform)), next(nullptr)
{

}

void eoMap::Item::applyToProgram(eoGLProgram *program)
{
}

eoType eoMap::Item::getType() const {
    return type;
}

eoMap::ItemFloat::ItemFloat(std::string uniform, float value)
    : Item(eoType::FLOAT, std::move(uniform)), value(value)
{
}

void eoMap::ItemFloat::applyToProgram(eoGLProgram *program) {
    program->setFloat(name.c_str(), value);
}

void eoMap::ItemFloat::setValue(float value) {
    ItemFloat::value = value;
}

float eoMap::ItemFloat::getValue() {
    return value;
}

eoMap::ItemVector2::ItemVector2(std::string uniform, eoVector2 value)
        : Item(eoType::VEC2, std::move(uniform)), value(value)
{
}

void eoMap::ItemVector2::applyToProgram(eoGLProgram *program) {
    program->setVec2(name.c_str(), value);
}

void eoMap::ItemVector2::setValue(const eoVector2 &value) {
    ItemVector2::value = value;
}

eoVector2 eoMap::ItemVector2::getValue() {
    return value;
}

