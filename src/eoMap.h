//
// Created by ego on 8/13/18.
//

#ifndef GFX_PROTOTYPE_EOLINKEDLIST_H
#define GFX_PROTOTYPE_EOLINKEDLIST_H

#include "eoType.h"
#include "eoVector2.h"
#include "eoGLProgram.h"

class eoMap {

public:

    class Item {
        friend eoMap;

    private:
        eoType type;
        Item* next;
        std::string name;
    public:
        Item(eoType type, std::string uniform);
        virtual void applyToProgram(eoGLProgram *program);
        eoType getType() const;
    };

    class ItemFloat : public Item {
    private:
        float value;
    public:
        ItemFloat(std::string uniform, float value);
        void applyToProgram(eoGLProgram *program) override;
        void setValue(float value);
        float getValue();
    };

    class ItemVector2 : public Item {
    private:
        eoVector2 value;
    public:
        ItemVector2(std::string uniform, eoVector2 value);
        void applyToProgram(eoGLProgram *program) override;

        void setValue(const eoVector2 &value);
        eoVector2 getValue();
    };

private:
    Item* head;

public:

    eoMap();

    void add(Item* item);
    void add(std::string uniform, float value);
    void add(std::string uniform, eoVector2 value);

    Item* operator[](int);
    Item* operator[](std::string);

    void set(std::string name, float value);
    void set(std::string name, eoVector2 value);

    void set(int index, float value);
    void set(int index, eoVector2 value);

    void applyToProgram(eoGLProgram* program);

};


#endif //GFX_PROTOTYPE_EOLINKEDLIST_H

