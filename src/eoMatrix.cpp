//
// Created by ego on 11/25/17.
//

#include "eoMatrix.h"
#include <math.h>
#include <cstring>

eoMatrix::eoMatrix() {
    setIdentity();
}

void eoMatrix::setIdentity() {
    data[0] = 1; data[3] = 0; data[6] = 0;
    data[1] = 0; data[4] = 1; data[7] = 0;
    data[2] = 0; data[5] = 0; data[8] = 1;
}


eoMatrix& eoMatrix::mult(eoMatrix mat) {
    eoMatrix result;

    for (int c = 0; c < 3; ++c) {
        for (int l = 0; l < 3; ++l) {

            result.setValue(l, c,
                getValue(l, 0) * mat.getValue(0, c) +
                getValue(l, 1) * mat.getValue(1, c) +
                getValue(l, 2) * mat.getValue(2, c)
            );
        }
    }

    memcpy(data, result.getData(), sizeof(float)*9);

    return *this;
}

inline float eoMatrix::getValue(int l, int c) {
    return data[l + c*3];
}

void eoMatrix::setValue(int l, int c, float v) {
    data[l + c*3] = v;
}

float *eoMatrix::getData() {
    return data;
}

eoMatrix& eoMatrix::rotate(float a) {
    eoMatrix rot;
    float* rdat = rot.getData();
    rdat[0] = cos(a); rdat[3] = -sin(a);
    rdat[1] = sin(a); rdat[4] =  cos(a);
    return mult(rot);
}

eoMatrix& eoMatrix::scale(float x, float y) {
    eoMatrix scale;
    float* sdat = scale.getData();
    sdat[0] = x;
    sdat[4] = y;
    return mult(scale);
}

eoMatrix &eoMatrix::scale(float x) {
    return scale(x, x);
}

eoMatrix& eoMatrix::scale(eoVector2 vec) {
    
    return scale(vec.getX(), vec.getY());
}

eoMatrix& eoMatrix::translate(float x, float y) {
    eoMatrix trans;
    float* tdat = trans.getData();
    tdat[6] = x;
    tdat[7] = y;
    return mult(trans);
}

eoVector2 eoMatrix::passVector(eoVector2 vec) {
    return eoVector2(data[0]*vec.getX() + data[3]*vec.getY() + data[6], data[1]*vec.getX() + data[4]*vec.getY() + data[7]);
}

eoMatrix &eoMatrix::translate(eoVector2 vector) {
    eoMatrix trans;
    *(reinterpret_cast<eoVector2*>(trans.getData() + 6)) = vector;
    return mult(trans);
}

