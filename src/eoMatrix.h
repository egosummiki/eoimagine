//
// Created by ego on 11/25/17.
//

#ifndef GFX_PROTOTYPE_EOMATRIX_H
#define GFX_PROTOTYPE_EOMATRIX_H

#include "eoVector2.h"

class eoMatrix {
    float data[9];
public:
    eoMatrix();
    void setIdentity();
    inline float getValue(int l , int c);
    void setValue(int l , int c, float v);
    eoVector2 passVector(eoVector2 vec);
    float* getData();
    eoMatrix& rotate(float a);
    eoMatrix& scale(float x, float y);
    eoMatrix& scale(eoVector2 vec);
    eoMatrix& scale(float x);
    eoMatrix& translate(float x, float y);
    eoMatrix& translate(eoVector2 vector);
    eoMatrix& mult(eoMatrix mat);
};


#endif //GFX_PROTOTYPE_EOMATRIX_H
