//
// Created by ego on 11/29/17.
//

#include "eoMessageBox.h"

eoWindowMain* eoMessageBox::mainWindow;

void eoMessageBox::show(QString info) {
    auto messageBox = new QMessageBox(mainWindow);
    messageBox->setText(info);
    messageBox->setIcon(QMessageBox::Information);
    messageBox->exec();
}

void eoMessageBox::showException(QString type, QString message, QString info) {
    auto messageBox = new QMessageBox(mainWindow);
    messageBox->setWindowTitle(type);
    messageBox->setText(message);
    messageBox->setInformativeText(info);
    messageBox->setIcon(QMessageBox::Critical);
    messageBox->exec();
}

void eoMessageBox::showException(QString type, QString message) {
    auto messageBox = new QMessageBox(mainWindow);
    messageBox->setWindowTitle(type);
    messageBox->setText(message);
    messageBox->setIcon(QMessageBox::Critical);
    messageBox->exec();
}
