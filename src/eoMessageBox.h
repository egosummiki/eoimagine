//
// Created by ego on 11/29/17.
//

#ifndef GFX_PROTOTYPE_EOMESSAGEBOX_H
#define GFX_PROTOTYPE_EOMESSAGEBOX_H

#include <QMessageBox>
#include "ui/eoWindowMain.h"
#include <csignal>

class eoMessageBox {
public:
    static eoWindowMain* mainWindow;
    static void show(QString info);
    static void showException(QString type, QString message, QString info);
    static void showException(QString type, QString message);
};

#define EO_ASSERT(cond, message) \
    if(!(cond)) { \
        eoMessageBox::show(QString("ASSERTION FAILED: ") % message); \
        std::raise(SIGINT); \
        return; \
    }

#define EO_ASSERT_NPTR(cond, message) \
    if(!(cond)) { \
        eoMessageBox::show(QString("ASSERTION FAILED: ") % message); \
        std::raise(SIGINT); \
        return nullptr; \
    }

#define EO_FAIL(message) \
    eoMessageBox::show(QString("ASSERTION FAILED: ") % message); \
    std::raise(SIGINT);


#endif //GFX_PROTOTYPE_EOMESSAGEBOX_H
