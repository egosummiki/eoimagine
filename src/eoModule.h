//
// Created by ego on 8/13/18.
//

#ifndef GFX_PROTOTYPE_EOMODULEMODIFIER_H
#define GFX_PROTOTYPE_EOMODULEMODIFIER_H

#include <variant>
#include "modifiers/eoModifier.h"
#include "eoMap.h"
#include "eoMessageBox.h"

template <class T>
class eoModule {
private:
    T* modifier;
    std::map<std::string, std::variant<float, eoVector2>> values;

public:
    explicit eoModule(T* modifier);

    void onRender(eoGLProgram *program);
    void setArgument(std::string name, float value);
    void setArgument(std::string name, eoVector2 value);
    eoVector2 getVector2(std::string name);
    eoVector2 getVector2(std::string name, eoVector2 def);
    float getFloat(std::string name);
    float getFloat(std::string name, float def);

    T *getModifier() const;
};

template <class T>
eoModule<T>::eoModule(T* modifier)
        : modifier(modifier)
{
    for(int i = 0; i < modifier->getArgumentCount(); i++) {
        eoModifier::Argument arg = modifier->getArgument(i);

        switch(arg.getType()) {
            case VOID:break;
            case FLOAT:
                values[arg.getName()] = 0;
                break;
            case VEC2:
                values[arg.getName()] = eoVector2(0,0);
                break;
            case VEC3:break;
            case VEC4:break;
        }
    }
}

template <class T>
void eoModule<T>::onRender(eoGLProgram *program) {
    for(const auto& [key, value] : values) {
        if(std::holds_alternative<float>(value)) {
            program->setFloat(key, std::get<float>(value));
        } else if(std::holds_alternative<eoVector2>(value)) {
            program->setVec2(key, std::get<eoVector2>(value));
        }
    }
}

template <class T>
void eoModule<T>::setArgument(std::string name, float value) {
    values[name] = value;
}

template <class T>
void eoModule<T>::setArgument(std::string name, eoVector2 value) {
    values[name] = value;
}

template <class T>
T *eoModule<T>::getModifier() const {
    return modifier;
}

template <class T>
eoVector2 eoModule<T>::getVector2(std::string name) {
    return std::get<eoVector2>(values[name]);
}

template <class T>
float eoModule<T>::getFloat(std::string name) {
    return std::get<float>(values[name]);
}

template<class T>
eoVector2 eoModule<T>::getVector2(std::string name, eoVector2 def) {
    if(values.find(name) == values.end()) {
        return def;
    }
    return std::get<eoVector2>(values[name]);
}

template<class T>
float eoModule<T>::getFloat(std::string name, float def) {
    if(values.find(name) == values.end()) {
        return def;
    }
    return std::get<float>(values[name]);
}

#endif //GFX_PROTOTYPE_EOMODULEMODIFIER_H
