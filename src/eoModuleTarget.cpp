//
// Created by ego on 8/12/18.
//

#include "eoModuleTarget.h"
#include "eoModule.h"

void eoModuleTarget::updateFill() {
}

eoShaderModifiers *eoModuleTarget::generateShader(eoShaderModifiers::BlendingMode blending) {

    auto shader = new eoShaderModifiers(blending);

    for(auto &module :fillModules) {
        shader->appendEffect(module->getModifier());
    }

    shader->finish();
    return shader;
}

void eoModuleTarget::renderModules(eoGLProgram* program) {
    
    for(auto &modifier :fillModules) {
        modifier->onRender(program);
    }
}

eoModule<eoModifierFill> *eoModuleTarget::getFillModule(eoModifierFill *modifier) {

    for(auto &module :fillModules) {
        if(module->getModifier() == modifier) {
            return module;
        }
    }

    auto module = new eoModule<eoModifierFill>(modifier);
    fillModules.emplace_back(module);

    return module;
}

eoMatrix eoModuleTarget::generateMatrix() {
    eoMatrix matrix;

    if(translate) {
        matrix.translate(translate->getVector2("position"));
    }

    if(scale) {
        matrix.scale(scale->getVector2("scale"));
    }

    if(rotate) {
        matrix.rotate(rotate->getFloat("rotation"));
    }

    return matrix;
}

void eoModuleTarget::updateTransform() {
    
}

eoModule<eoModifier>* eoModuleTarget::getTranslateModule() {

    if(!translate)
        translate = new eoModule<eoModifier>(eoModifier::TRANSLATE);

    return translate;
}

eoModule<eoModifier>* eoModuleTarget::getRotateModule() {

    if(!rotate)
        rotate = new eoModule<eoModifier>(eoModifier::ROTATE);

    return rotate;
}

eoModule<eoModifier>* eoModuleTarget::getScaleModule() {
    
    if(!scale) {
        scale = new eoModule<eoModifier>(eoModifier::SCALE);
        scale->setArgument("scale", eoVector2(1.0f, 1.0f));
    }

    return scale;
}

eoVector2 eoModuleTarget::traceBack(eoVector2 point) {
    if(translate) {
        point -= translate->getVector2("position");
    }

    if(scale) {
        point /= scale->getVector2("scale");
    }

    if(rotate) {
        auto pointLen = point.length();
        auto pointAngle = point.toAngle() - rotate->getFloat("rotation");
        point = eoVector2::fromAngle(pointAngle) * pointLen;
    }

    return point;
}

