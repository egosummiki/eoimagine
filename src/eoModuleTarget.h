//
// Created by ego on 8/12/18.
//

#ifndef GFX_PROTOTYPE_EOMODIFIERTARGET_H
#define GFX_PROTOTYPE_EOMODIFIERTARGET_H

#include <vector>
#include "eoModule.h"
#include "shaders/eoShaderModifiers.h"
#include "eoMatrix.h"

/*
 * Module Roles:
 *
 * Fill modules form a fragment shader.
 * Transform modules form the fransform matrix.
 *
 * */

class eoModuleTarget {

private:
    std::vector<eoModule<eoModifierFill>*> fillModules;
    eoModule<eoModifier>* translate = nullptr;
    eoModule<eoModifier>* rotate = nullptr;
    eoModule<eoModifier>* scale = nullptr;

public:
    virtual void updateFill();
    virtual void updateTransform();
    eoModule<eoModifierFill>* getFillModule(eoModifierFill* modifier);
    eoModule<eoModifier>* getTranslateModule();
    eoModule<eoModifier>* getRotateModule();
    eoModule<eoModifier>* getScaleModule();

    eoShaderModifiers* generateShader(eoShaderModifiers::BlendingMode blending);
    eoMatrix generateMatrix();
    void renderModules(eoGLProgram* program);
    eoVector2 traceBack(eoVector2 point);
};


#endif //GFX_PROTOTYPE_EOMODIFIERTARGET_H
