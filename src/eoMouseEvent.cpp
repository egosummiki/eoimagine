//
// Created by ego on 7/26/18.
//

#include "eoMouseEvent.h"
#include "eoCanvas.h"

eoMouseEvent::eoMouseEvent(QMouseEvent *event, eoCanvas *canvas, float middleWidth, float middleHeight)
        : QMouseEvent(*event),
        
        screenPosition (
            (x() - middleWidth),
            (middleHeight - y())
        ),

        canvasPosition(
            (x() - middleWidth - canvas->getOffsetX())  / canvas->getScaleFactor(),
            (middleHeight - y() - canvas->getOffsetY()) / canvas->getScaleFactor()
        ),

        canvasUVPosition(
            canvasPosition.getX() + canvas->getMiddleWidth(),
            canvasPosition.getY() + canvas->getMiddleHeight()
        )
{
}

float eoMouseEvent::getCanvasX() const {
    return canvasPosition.getX();
}

float eoMouseEvent::getCanvasY() const {
    return canvasPosition.getY();
}

const eoVector2 &eoMouseEvent::getCanvasPosition() const {
    return canvasPosition;
}

eoVector2 eoMouseEvent::getPosition() {
    return eoVector2(x(), y());
}

const eoVector2 &eoMouseEvent::getScreenPosition() const {
    
    return screenPosition;
}

float eoMouseEvent::getCanvasUVX() const {
    
    return canvasUVPosition.getX();
}

float eoMouseEvent::getCanvasUVY() const {
    
    return canvasUVPosition.getY();
}
