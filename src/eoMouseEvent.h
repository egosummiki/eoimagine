//
// Created by ego on 7/26/18.
//

#ifndef GFX_PROTOTYPE_EOMOUSEEVENT_H
#define GFX_PROTOTYPE_EOMOUSEEVENT_H

#include <QMouseEvent>
#include "eoVector2.h"

class eoCanvas;

class eoMouseEvent : public QMouseEvent {

    eoVector2 canvasPosition;
    eoVector2 canvasUVPosition;
    eoVector2 screenPosition;

public:

    eoMouseEvent(QMouseEvent *event, eoCanvas *canvas, float middleWidth, float middleHeight);

    float getCanvasX() const;
    float getCanvasY() const;

    float getCanvasUVX() const;
    float getCanvasUVY() const;

    const eoVector2 &getCanvasPosition() const;
    const eoVector2 &getScreenPosition() const;
    eoVector2 getPosition();
};


#endif //GFX_PROTOTYPE_EOMOUSEEVENT_H
