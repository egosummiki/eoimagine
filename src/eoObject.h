//
// Created by ego on 11/25/17.
//

#ifndef GFX_PROTOTYPE_EOOBJECT_H
#define GFX_PROTOTYPE_EOOBJECT_H

#include "eoGLProgram.h"
#include "eoCanvas.h"

class eoObject {
protected:
    eoCanvas* canvas;
public:
    explicit eoObject(eoCanvas *canvas);
    virtual void onRender();
};


#endif //GFX_PROTOTYPE_EOOBJECT_H
