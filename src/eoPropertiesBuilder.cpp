//
// Created by ego on 10/10/18.
//

#include "eoPropertiesBuilder.h"
#include <QtGui>

eoPropertiesBuilder::eoPropertiesBuilder() 
    : layout(new QVBoxLayout)
{
    layout->setAlignment(Qt::AlignTop);
    setLayout(layout);
}

eoPropertiesBuilder::~eoPropertiesBuilder() {

    
    for(auto& slider :floatSliders) {

        delete slider.edit;
        delete slider.validator;
        delete slider.label;
        delete slider.layout;
        delete slider.slider;
    }

    floatSliders.clear();

    delete colorView.colorLabel;

    for(auto& splitter :splitters) {

        delete splitter;
    }

    splitters.clear();

    for(auto& colorSelect :colorSelects) {

        delete colorSelect.colorButton;
        delete colorSelect.colorDiag;
    }

    colorSelects.clear();

    for(auto& button :buttons) {

        delete button.button;
    }

    buttons.clear();

    delete layout;
}

int eoPropertiesBuilder::addFloatSlider(QString label, int min, int max, float *property, std::function<void(float)> cback) {

    TypeFloatSlider propertyInfo;    

    propertyInfo.property = property;
    propertyInfo.callback = std::move(cback);

    propertyInfo.layout = new QHBoxLayout();

    propertyInfo.label = new QLabel();
    propertyInfo.label->setText(label);

    propertyInfo.validator = new QIntValidator(min, max);

    propertyInfo.edit = new QLineEdit();
    propertyInfo.edit->setText(QString(std::to_string(static_cast<int>(*property)).c_str()));
    propertyInfo.edit->setAlignment(Qt::AlignCenter);
    propertyInfo.edit->setStyleSheet("QLineEdit { border: none; font-weight: bold; background-color: rgba(0,0,0,0); }");
    propertyInfo.edit->setMaximumWidth(64);
    propertyInfo.edit->setValidator(propertyInfo.validator);
    
    propertyInfo.slider = new QSlider(Qt::Horizontal);
    propertyInfo.slider->setValue(static_cast<int>(*property));
    propertyInfo.slider->setRange(min, max);
    
    propertyInfo.layout->addWidget(propertyInfo.label);
    propertyInfo.layout->addWidget(propertyInfo.edit);

    layout->addLayout(propertyInfo.layout);
    layout->addWidget(propertyInfo.slider);

    connect(propertyInfo.slider, QOverload<int>::of(&QSlider::sliderMoved), [propertyInfo] (int value) {

        *(propertyInfo.property) = static_cast<float>(value);
        propertyInfo.edit->setText(QString(std::to_string(value).c_str()));

        if(propertyInfo.callback)
            propertyInfo.callback(static_cast<float>(value));
    });

    connect(propertyInfo.edit, static_cast<void (QLineEdit::*)(const QString&)>(&QLineEdit::textEdited), [propertyInfo] (const QString& text) {

        if(text.isEmpty())
            return;

        float value = std::stof(text.toStdString());

        *(propertyInfo.property) = value;
        propertyInfo.slider->setValue(static_cast<int>(value));

        if(propertyInfo.callback)
            propertyInfo.callback(value);
    });

   floatSliders.emplace_back(propertyInfo);

   return floatSliders.size() - 1; 
}

void eoPropertiesBuilder::onFloatSliderChange(int index) {

    TypeFloatSlider propertyInfo = floatSliders[index];

    int value = static_cast<int>(*(propertyInfo.property));
    
    propertyInfo.edit->setText(QString(std::to_string(value).c_str()));
    propertyInfo.slider->setValue(value);
}

int eoPropertiesBuilder::addColorView(float *colors) {
    
    colorView.property = colors;

    colorView.colorLabel = new QLabel();
    colorView.colorLabel->setMinimumHeight(32);
    colorView.colorLabel->setAutoFillBackground(true);
    colorView.colorLabel->setStyleSheet(QString("QLabel { border-radius: 8px; background: rgb(%1, %2, %3); }").arg(colors[0]).arg(colors[1]).arg(colors[2]));

    layout->addWidget(colorView.colorLabel);

    return 0;
}

void eoPropertiesBuilder::updateColorView() {
    
    colorView.colorLabel->setStyleSheet(QString("QLabel { border-radius: 8px; background: rgb(%1, %2, %3); }")
            .arg(colorView.property[0]).arg(colorView.property[1]).arg(colorView.property[2]));
}

void eoPropertiesBuilder::addSplitter() {
    
    auto splitter = new QSplitter;

    splitter->setMinimumHeight(12);
    layout->addWidget(splitter);

    splitters.emplace_back(splitter);
}

void eoPropertiesBuilder::addColorSelect(QString label, eoColor *color, std::function<void(eoColor)> callback) {
    
    TypeColorSelect colorSelect;

    colorSelect.property = color;

    colorSelect.layout = new QHBoxLayout();

    colorSelect.label = new QLabel();
    colorSelect.label->setText(label);

    colorSelect.layout->addWidget(colorSelect.label);

    colorSelect.colorButton = new QPushButton("&");
    colorSelect.colorButton->setMinimumHeight(26);
    colorSelect.colorButton->setMaximumWidth(42);
    colorSelect.colorButton->setStyleSheet(QString("QPushButton { border-radius: 8px; background: rgb(%1, %2, %3); }")
            .arg(colorSelect.property->getR()*255.0f).arg(colorSelect.property->getG()*255.0f).arg(colorSelect.property->getB()*255.0f));
    colorSelect.colorButton->setAutoFillBackground(true);

    colorSelect.colorDiag = new eoColorDialog(nullptr, color, [colorSelect, callback] () {
            
        colorSelect.colorButton->setStyleSheet(QString("QPushButton { border-radius: 8px; background: rgb(%1, %2, %3); }")
                .arg(colorSelect.property->getR()*255.0f).arg(colorSelect.property->getG()*255.0f).arg(colorSelect.property->getB()*255.0f));

        if(callback)
            callback(*(colorSelect.property));
    });

    colorSelect.layout->addWidget(colorSelect.colorButton);

    layout->addLayout(colorSelect.layout);

    connect(colorSelect.colorButton, &QPushButton::pressed, [colorSelect] () {
        
        colorSelect.colorDiag->show();
    });
    
    colorSelects.emplace_back(colorSelect);
}

void eoPropertiesBuilder::addButton(QString label, std::function<void(void)> callback) {
    
    TypeButton button;

    button.callback = std::move(callback);
    button.button = new QPushButton();
    button.button->setText(label);

    layout->QLayout::addWidget(button.button);

    connect(button.button, &QPushButton::released, button.callback);

    buttons.emplace_back(button);
}

QComboBox *eoPropertiesBuilder::addCombo(QString label, const QString items[], size_t itemCount, unsigned *property) {
    
    TypeCombo combo;

    combo.property = property;

    combo.label = new QLabel();
    combo.label->setText(label);

    layout->addWidget(combo.label);

    combo.combo = new QComboBox();

    for(int i = 0; i < itemCount; ++i) {

        combo.combo->addItem(items[i]);
    }

    combo.combo->setCurrentIndex(*property);

    layout->addWidget(combo.combo);

    connect(combo.combo, QOverload<int>::of(&QComboBox::currentIndexChanged), [combo] (int value) {

            *(combo.property) = value;
    });

    return combo.combo;
}

void eoPropertiesBuilder::addWidget(QWidget *widget) {
    
    layout->addWidget(widget);
}

QComboBox *eoPropertiesBuilder::addCombo(QString label, std::function<void(int)> callback) {
    
    TypeCombo combo;

    combo.property = nullptr;

    combo.label = new QLabel();
    combo.label->setText(label);

    layout->addWidget(combo.label);

    combo.combo = new QComboBox();

    layout->addWidget(combo.combo);

    connect(combo.combo, QOverload<int>::of(&QComboBox::activated), callback);

    return combo.combo;
}
