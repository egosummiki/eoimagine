#pragma once
//
// Created by ego on 10/10/18.
//
#include <QWidget>
#include <QHBoxLayout>
#include <vector>
#include <QLabel>
#include <QLineEdit>
#include <QSlider>
#include <QIntValidator>
#include <functional>
#include <QSplitter>
#include <QPushButton>
#include <QComboBox>

#include "ui/eoColorDialog.h"

class eoPropertiesBuilder : public QWidget {

Q_OBJECT

private:
    
    struct TypeFloatSlider {

        QHBoxLayout *layout;
        QLabel *label;
        QLineEdit *edit;
        QSlider *slider;
        QIntValidator *validator;
        std::function<void(float)> callback;

        float *property;
    };

    struct TypeColorView {

        QLabel *colorLabel;
        float *property;
    };

    struct TypeColorSelect {

        QHBoxLayout *layout;
        QLabel *label;
        eoColorDialog* colorDiag;
        QPushButton *colorButton;
        eoColor *property;
    
    };

    struct TypeButton {

        QPushButton *button;
        std::function<void(void)> callback;
    };

    struct TypeCombo {
        QComboBox *combo;
        QLabel *label;

        unsigned *property;
    };

    QVBoxLayout *layout;

    std::vector<TypeFloatSlider> floatSliders;
    std::vector<QSplitter*> splitters;
    std::vector<TypeColorSelect> colorSelects;
    std::vector<TypeButton> buttons;
    TypeColorView colorView;
    
public:

    eoPropertiesBuilder();
    ~eoPropertiesBuilder();

    int addFloatSlider(QString label, int min, int max, float *property, std::function<void(float)> cback = std::function<void(float)>());
    void onFloatSliderChange(int index);

    int addColorView(float *colors);
    void updateColorView();

    void addColorSelect(QString label, eoColor *color, std::function<void(eoColor)> callback = std::function<void(eoColor)>());

    void addSplitter();

    void addButton(QString label, std::function<void(void)> callback);

    QComboBox *addCombo(QString label, const QString items[], size_t itemCount, unsigned *property);
    QComboBox *addCombo(QString label, std::function<void(int)> callback);

    void addWidget(QWidget *widget);

};
