//
// Created by ego on 11/23/17.
//

#include "eoRender.h"
#include "eoGLProgram.h"

#include "shaders/eoShaderFile.h"
#include "shaders/eoShaderVertexTrivial.h"

#include "eoMatrix.h"
#include "eoFrameBuffer.h"
#include "eoTexture.h"
#include "shaders/eoShaderFragmentTrivial.h"
#include "eoLayerRaster.h"
#include "ui/eoWidgetLayers.h"
#include "ui/eoWidgetHistory.h"
#include "ui/eoColorDialog.h"
#include "tools/eoToolNormal.h"
#include "eoMessageBox.h"

#include "modifiers/eoModifier.h"

#include <cmath>

#include <chrono>

#include "actions/eoActionFillColor.h"

#include "eoBlending.h"
#include "eoFontManager.h"

eoRender::eoRender(int screenWidth, int screenHeight, eoWindowMain *mainWindow)
        :   screenDimension(mainWindow->getGlArea()->width(), mainWindow->getGlArea()->height()),
            mainWindow(mainWindow),
            keyBind(new eoKeyBind(mainWindow))
{
    /*
     *  Blending is enabled only within layers.
     *  Interlayer blending is done manually, to perform more complex blending modes.
     *  All the layers are premultipled, so we're able to draw on a transparent layer.
     *  Orgb = Srgb + (1-Sa)*Drgb
     *
     *  Set the blending to premultipled, then disable it.
     * */
    eoBlending::apply(eoBlending::PREMUL);
    eoBlending::apply(eoBlending::NONE);

    glEnable(GL_POINT_SMOOTH);
    glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
    glEnable(GL_MULTISAMPLE);

    /* Font manager */
    fontManager = new eoFontManager(this);

    /* Generation static objects. */
    eoShader::loadShaders(this);
    eoVertexBuffer::generateStaticBuffers();
    eoGLProgram::loadStaticPrograms();
    eoModifier::generateModifiers();
    eoTool::generateTools(this);

    glClearColor(0.15f, 0.15f, 0.15f, 1.0f);
}

eoRender::~eoRender() {
    eoShader::releaseShaders();

    for(auto &canvas :canvases) {
        delete canvas;
    }

    delete keyBind;
}

void eoRender::draw() {
    glDrawBuffer(GL_COLOR_ATTACHMENT0);
    eoBlending::disable();

    // Perform render callback. Multiple utilizations.
    while(!callbackQueue.empty()) {
        auto callback = callbackQueue.front();
        callbackQueue.pop();
        callback(currentCanvas);
    }

    glClear (GL_COLOR_BUFFER_BIT);

    if(currentCanvas)
        currentCanvas->onRender();

    if(currentTool)
        currentTool->onTick();

    glFlush();
}

void eoRender::resize(int width, int height) {
    screenDimension = eoVector2(width, height);

    for(auto& canvas :canvases)
        canvas->onResize(width, height);
}

eoCanvas *eoRender::getCanvas() const {
    return currentCanvas;
}

eoCanvas *eoRender::createDocument(std::string name, int width, int height) {
    auto canvas = new eoCanvas(this, eoVector2(width, height), mainWindow, keyBind, canvases.size());
    canvases.emplace_back(canvas);
    canvas->setDocumentTitle(name);
    canvas->addLayer(new eoLayerRaster(canvas, eoColor::WHITE, "Blank Layer"));
    mainWindow->getTabLayout()->addTab(canvas);
    setCurrentCanvas(canvas);

    return canvas;
}

eoCanvas *eoRender::createDocumentFromTexture(std::string name, eoTexture *texture) {
    auto canvas = new eoCanvas(this, texture->getDimension(), mainWindow, keyBind, canvases.size());
    canvases.emplace_back(canvas);
    canvas->setDocumentTitle(name);
    canvas->addLayer(new eoLayerRaster(canvas, QString(name.c_str()), texture));
    mainWindow->getTabLayout()->addTab(canvas);
    setCurrentCanvas(canvas);

    return canvas;
}

void eoRender::setCurrentCanvas(eoCanvas* canvas) {
    eoTabButton* tabButton;

    tabButton = mainWindow->getTabLayout()->getTab(currentCanvas);

    if(tabButton)
        tabButton->setInactiveStyle();

    if(currentCanvas)
        currentCanvas->onLoseCurrent();

    if(currentTool)
        currentTool->onLose();

    performOnRender([this] (eoCanvas *canvas) {
        for(auto tool = reinterpret_cast<eoTool**>(&eoTool::t); *tool; ++tool) {
            (*tool)->onCanvasChange(canvas);
        }

        fontManager->onCanvasChange(canvas);

        /* This needs to be performed after tools are informed about the canvas change. */
        if(currentTool)
            currentTool->cast();
    
    });

    currentCanvas = canvas;

    canvas->onSetCurrent();

    mainWindow->setWindowTitle(canvas->getDocumentTitle().c_str());

    tabButton = mainWindow->getTabLayout()->getTab(canvas);

    if(tabButton)
        tabButton->setActiveStyle();

    requestRedraw();
}

void eoRender::performOnRender(std::function<void(eoCanvas *)> callback) {
    callbackQueue.emplace(std::move(callback));
}

void eoRender::requestRedraw() const {
    mainWindow->getGlArea()->update();
}

void eoRender::killCanvas(eoCanvas* canvas) {
    EO_ASSERT(canvas->getId() >= 0 && canvas->getId() < canvases.size(), "Trying to kill a canvas, that isn't listed.");

    mainWindow->getTabLayout()->removeTab(canvas);

    if(currentCanvas == canvas) {
        
        if(canvas->getId() > 0) {

            setCurrentCanvas(canvases[canvas->getId()-1]);
        } else if(canvases.size() == 1) {

            clearWorkSpace();
        } else {

            setCurrentCanvas(canvases[1]);
        }
    }
    
    canvases.erase(canvases.begin() + canvas->getId());
    delete canvas;
}

void eoRender::clearWorkSpace() {
    currentCanvas = nullptr;
    mainWindow->getLayerWidget()->getLayerTree()->setModel(nullptr);
    mainWindow->getHistoryWidget()->getListView()->setModel(nullptr);
}

eoWindowMain *eoRender::getMainWindow() {
    return mainWindow;
}

void eoRender::useTool(eoTool *tool) {
    if(currentTool && currentTool->getLastResult() == eoTool::ResultSuccess)
        currentTool->onLose();

    currentTool = tool;
    currentTool->cast();

    auto label = currentTool->getLabel();
    mainWindow->getStatusBar()->setModeLabel(label.label, label.background, label.foreground);

    mainWindow->getPropertiesDock()->setWidget(currentTool->getPropertiesWidget());
}

eoTool *eoRender::getCurrentTool() {
    return currentTool;
}

eoVector2 &eoRender::getScreenDimension() {
    return screenDimension;
}

void eoRender::setFillColor() {
    glClearColor(0.1f, 0.1f, 0.11f, 1.0f);
}

FT_Library &eoRender::getFontLibrary() {
    return fontManager->getFontLibrary();
}

eoFontManager *eoRender::getFontManager() {
    return fontManager;
}
