//
// Created by ego on 11/23/17.
//

#ifndef GFX_PROTOTYPE_EORENDER_H
#define GFX_PROTOTYPE_EORENDER_H

#include "eoCanvas.h"
#include "eoInput.h"
#include "eoKeyBind.h"
#include <ft2build.h>
#include FT_FREETYPE_H  
#include <vector>
#include <queue>
#include <string>

class eoWindowMain;
class eoTexture;
class eoTool;
class eoFont;
class eoFontManager;

class eoRender {

private:

    std::vector<eoCanvas*> canvases;
    eoCanvas* currentCanvas = nullptr;
    std::queue<std::function<void(eoCanvas*)>> callbackQueue;

    eoWindowMain* mainWindow;
    eoKeyBind* keyBind;
    eoTool* currentTool = nullptr;
    eoFontManager *fontManager = nullptr;

    eoVector2 screenDimension;

public:
    explicit eoRender(int screenWidth, int screenHeight, eoWindowMain *mainWindow);
    ~eoRender();
    void draw();
    void resize(int width, int height);
    eoWindowMain *getMainWindow();
    eoCanvas *getCanvas() const;
    eoCanvas *createDocument(std::string name, int width, int height);
    eoCanvas *createDocumentFromTexture(std::string name, eoTexture* texture);
    eoVector2 &getScreenDimension();
    void killCanvas(eoCanvas* canvas);
    void setCurrentCanvas(eoCanvas* canvas);
    void requestRedraw() const;
    void performOnRender(std::function<void(eoCanvas*)> callback);
    void clearWorkSpace();
    void useTool(eoTool* tool);
    eoTool *getCurrentTool();
    FT_Library &getFontLibrary();
    eoFontManager *getFontManager();

    static void setFillColor();
};


#endif //GFX_PROTOTYPE_EORENDER_H
