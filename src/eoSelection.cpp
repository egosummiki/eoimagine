//
// Created by ego on 15/10/18.
//

#include "eoSelection.h"
#include "eoVertexBuffer.h"
#include "shaders/eoShaderFile.h"

eoSelection::eoSelection(eoVector2 dimension) 
    :   selectionBuffer(eoFrameBuffer::createPersevering(dimension, GL_COLOR_ATTACHMENT2, GL_RED, GL_RED)),
        aidBuffer(eoFrameBuffer::createPersevering(dimension, GL_COLOR_ATTACHMENT3, GL_RED, GL_RED)),
        dimension(dimension)
{
    
    selectionProgram = new eoGLProgram();
    selectionProgram->attachShader(eoShader::VERTEX_SCREENDIM);
    selectionProgram->attachShader(eoShader::FRAGMENT_SELECTION);
    selectionProgram->link();

    selectAll();

    auto fragment = new eoShaderFile(GL_FRAGMENT_SHADER, "data/glsl/fragment_negative.glsl");

    invertProgram = new eoGLProgram();
    invertProgram->attachShader(eoShader::VERTEX_TRIVIAL);
    invertProgram->attachShader(fragment);
    invertProgram->link();

    delete fragment;

    fragment = new eoShaderFile(GL_FRAGMENT_SHADER, "data/glsl/selection_grow.glsl");

    growProgram = new eoGLProgram();
    growProgram->attachShader(eoShader::VERTEX_TRIVIAL);
    growProgram->attachShader(fragment);
    growProgram->link();

    delete fragment;

    fragment = new eoShaderFile(GL_FRAGMENT_SHADER, "data/glsl/selection_harden.glsl");

    hardenProgram = new eoGLProgram();
    hardenProgram->attachShader(eoShader::VERTEX_TRIVIAL);
    hardenProgram->attachShader(fragment);
    hardenProgram->link();

    delete fragment;
}

eoFrameBuffer *eoSelection::getSelectionBuffer() const {
    
    return selectionBuffer;
}

void eoSelection::renderSelection(eoVertexBuffer *vertexBuffer, eoZoom *zoom) {

    static float blink = 0.0f;
    blink += .1f;
    
    selectionProgram->use();
    selectionProgram->setTransform(zoom->getZoomMatrix());
    selectionProgram->setFloat("blink", blink);
    selectionProgram->setVec2("canvasMiddle", dimension);
    selectionProgram->setFloat("zoom", zoom->getScaleFactor());
    selectionBuffer->bind();
    vertexBuffer->render();
}

void eoSelection::selectAll() {
    
    selectionBuffer->begin(); {

        glClearColor(1.0f, 0.0f, 0.0f, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT);
    
    } selectionBuffer->end();

    glClearColor(0.1f, 0.1f, 0.11f, 1.0f);
}

void eoSelection::selectNone() {
    
    selectionBuffer->begin(); {

        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT);
    
    } selectionBuffer->end();

    glClearColor(0.1f, 0.1f, 0.11f, 1.0f);
}

void eoSelection::applyAidBuffer() {
    
    selectionBuffer->begin(); {

        eoGLProgram::TRIVIAL->use();
        eoGLProgram::TRIVIAL->setTransform(eoMatrix());
        aidBuffer->bind();
        eoVertexBuffer::FULL_DIMENSION->render();
    
    } selectionBuffer->end();
}

void eoSelection::invertSelection() {
    
    aidBuffer->begin(); {

        invertProgram->use();
        invertProgram->setTransform(eoMatrix());
        selectionBuffer->bind();
        eoVertexBuffer::FULL_DIMENSION->render();
    
    } aidBuffer->end();

    applyAidBuffer();
}

void eoSelection::growSelection(float radius) {
    
    aidBuffer->begin(); {

        growProgram->use();
        growProgram->setTransform(eoMatrix());
        growProgram->setVec2("canvasMiddle", dimension);
        growProgram->setFloat("radius", radius);
        selectionBuffer->bind();
        eoVertexBuffer::FULL_DIMENSION->render();
    
    } aidBuffer->end();

    applyAidBuffer();
}

void eoSelection::hardenSelection() {
    
    aidBuffer->begin(); {

        hardenProgram->use();
        hardenProgram->setTransform(eoMatrix());
        selectionBuffer->bind();
        eoVertexBuffer::FULL_DIMENSION->render();
    
    } aidBuffer->end();

    applyAidBuffer();
}
