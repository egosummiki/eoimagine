#pragma once
//
// Created by ego on 15/10/18.
//
#include "eoFrameBuffer.h"
#include "eoGLProgram.h"
#include "eoZoom.h"

class eoVertexBuffer;

class eoSelection {

private:
    eoFrameBuffer* selectionBuffer;
    eoFrameBuffer* aidBuffer;
    eoGLProgram* selectionProgram;
    eoGLProgram* invertProgram;
    eoGLProgram* growProgram;
    eoGLProgram* hardenProgram;
    eoVector2 dimension;

    void applyAidBuffer();

public:
    eoSelection(eoVector2 dimension);

    eoFrameBuffer *getSelectionBuffer() const;
    void renderSelection(eoVertexBuffer *vertexBuffer, eoZoom *zoom);
    void selectAll();
    void selectNone();
    void invertSelection();
    void growSelection(float radius);
    void hardenSelection();
};
