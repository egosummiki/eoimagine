//
// Created by ego on 11/25/17.
//

#include "eoTexture.h"
#include "eoException.h"
#include <fstream>


eoTexture::eoTexture(QImage image, int bytesPerLine) 
    : QOpenGLTexture(image), bytesPerLine(bytesPerLine)
{
    
}


eoTexture::eoTexture(int width, int height, uchar *rawData, QOpenGLTexture::PixelFormat format)
    : QOpenGLTexture(QOpenGLTexture::Target2D)
{
    
    setMinMagFilters(QOpenGLTexture::Linear, QOpenGLTexture::Linear);
    create();

    setSize(width, height, 1);
    setFormat(QOpenGLTexture::RGBA8_UNorm);
    allocateStorage();
    setData(format, QOpenGLTexture::UInt8, rawData);
}

int eoTexture::getWidth() {
    return width();
}

int eoTexture::getHeight() {
    return height();
}

eoVector2 eoTexture::getDimension() {
    
    return eoVector2(width(), height());
}

eoTexture *eoTexture::load(QString path) {
    
    QImage image = QImage(path).mirrored();
    return new eoTexture(image, image.bytesPerLine());
}

int eoTexture::getBytesPerLine() {
    
    return bytesPerLine;
}
