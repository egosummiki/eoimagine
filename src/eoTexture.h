//
// Created by ego on 11/25/17.
//

#ifndef GFX_PROTOTYPE_EOTEXTURE_H
#define GFX_PROTOTYPE_EOTEXTURE_H

#include <GL/gl.h>
#include <QOpenGLTexture>
#include <QtCore/QString>
#include "eoFrameBuffer.h"
#include "eoVector2.h"

class eoTexture : public QOpenGLTexture {
    GLuint id;
    int bytesPerLine;

public:
    explicit eoTexture(QImage image, int bytesPerLine);
    explicit eoTexture(int width, int height, uchar *rawData, QOpenGLTexture::PixelFormat format);

    int getWidth();
    int getHeight();
    int getBytesPerLine();
    eoVector2 getDimension();
    static eoTexture *load(QString path);

};


#endif //GFX_PROTOTYPE_EOTEXTURE_H
