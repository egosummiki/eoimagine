//
// Created by ego on 8/12/18.
//

#ifndef GFX_PROTOTYPE_EOTYPE_H
#define GFX_PROTOTYPE_EOTYPE_H

enum eoType :unsigned int {

    VOID,
    FLOAT,
    VEC2,
    VEC3,
    VEC4

};


#endif //GFX_PROTOTYPE_EOTYPE_H
