//
// Created by ego on 7/13/18.
//

#include "eoVector2.h"
#include <cmath>

eoVector2::eoVector2(float x, float y) : x(x), y(y) {

}

eoVector2::eoVector2() : x(0.0f), y(0.0f) {
    
}

float eoVector2::getX() const {
    return x;
}

float eoVector2::getY() const {
    return y;
}

void eoVector2::set(float x, float y) {

    this->x = x;
    this->y = y;

}

bool eoVector2::operator!=(const eoVector2& other) {
    return x != other.x || y != other.y;
}

eoVector2 eoVector2::toUV(float width, float height) {
    return eoVector2(x / width + 0.5f, y / height + 0.5f);
}

eoVector2 eoVector2::operator/(const eoVector2 &other) {
    return eoVector2(x / other.getX(), y / other.getY());
}

eoVector2 &eoVector2::operator-=(eoVector2 other) {
    x -= other.x;
    y -= other.y;

    return *(this);
}

eoVector2 &eoVector2::operator+=(eoVector2 other) {
    x += other.x;
    y += other.y;

    return *(this);
}

eoVector2 &eoVector2::operator*=(eoVector2 other) {
    x *= other.x;
    y *= other.y;

    return *(this);
}

eoVector2 &eoVector2::operator*=(float other) {

    x *= other;
    y *= other;

    return *(this);
}

eoVector2 &eoVector2::operator/=(eoVector2 other) {
    
    x /= other.x;
    y /= other.y;

    return *(this);
}

eoVector2 eoVector2::operator/(const float other) const {
    return eoVector2(x/other, y/other);
}

eoVector2 &eoVector2::flipY() {
    y = -y;
    return *(this);
}

eoVector2 eoVector2::operator+(const eoVector2 other) const {
    return eoVector2(x + other.x, y + other.y);
}

eoVector2 eoVector2::operator-(eoVector2 other) const {
    return eoVector2(x - other.x, y - other.y);
}

eoVector2 eoVector2::operator*(eoVector2 other) {
    return eoVector2(x * other.x, y * other.y);
}

eoVector2 eoVector2::operator*(float other) const {
    return eoVector2(x*other, y*other);
}

float eoVector2::toAngle() const {
    
    if(x == 0.0f) {
        return y > 0.0f ? 0.0f : 3.141592f;
    } else if(x > 0.0f) {
        return atan(y / x);
    }

    return 3.141592f + atan(y / x);
}

float distance(eoVector2 one, eoVector2 other) {
    float xDiff = one.getX() - other.getX();
    float yDiff = one.getY() - other.getY();

    return sqrt( xDiff*xDiff + yDiff*yDiff );
}

eoVector2 mix(eoVector2 one, eoVector2 other, float amount) {
    
    return eoVector2(   one.getX() * (1.0f - amount) + other.getX() * amount,
                        one.getY() * (1.0f - amount) + other.getY() * amount    );
}

bool eoVector2::operator==(const eoVector2& other) const {
    
    return x == other.x && y == other.y;
}

void eoVector2::setX(float x) {
    
    this->x = x;
}

void eoVector2::setY(float y) {
    
    this->y = y;
}

eoVector2 eoVector2::normalise() const {

    float len = length();
    
    return eoVector2(x/len, y/len);
}

float eoVector2::length() const {
    
    return sqrt(x*x + y*y);
}

float min(float a, float b) {
    
    return a > b ? b : a;
}
float max(float a, float b) {
    
    return a > b ? a : b;
}

eoVector2 min(eoVector2 self, eoVector2 other) {
    
    return eoVector2(min(self.getX(), other.getX()), min(self.getY(), other.getY()));
}

eoVector2 max(eoVector2 self, eoVector2 other) {
    
    return eoVector2(max(self.getX(), other.getX()), max(self.getY(), other.getY()));
}

eoVector2 eoVector2::fromAngle(float angle) {
    
    return eoVector2(cos(angle), sin(angle));
}
