//
// Created by ego on 7/13/18.
//

#ifndef GFX_PROTOTYPE_EOVECTOR2_H
#define GFX_PROTOTYPE_EOVECTOR2_H


class eoVector2 {

    float x,y;
public:
    eoVector2(float x, float y);
    eoVector2();

    float getX() const;
    float getY() const;
    void set(float x, float y);
    void setX(float x);
    void setY(float y);

    eoVector2& flipY();
    eoVector2 normalise() const;
    float length() const;
    float toAngle() const;

    bool operator!=(const eoVector2& other);
    bool operator==(const eoVector2& other) const;
    eoVector2 operator/(const eoVector2& other);
    eoVector2 &operator-=(eoVector2 other);
    eoVector2 &operator+=(eoVector2 other);
    eoVector2 &operator*=(eoVector2 other);
    eoVector2 &operator/=(eoVector2 other);
    eoVector2 &operator*=(float other);
    eoVector2 operator+(eoVector2 other) const;
    eoVector2 operator-(eoVector2 other) const;
    eoVector2 operator*(eoVector2 other);
    eoVector2 operator*(float other) const;
    eoVector2 operator/(float other) const;

    eoVector2 toUV(float width, float height);

    static eoVector2 fromAngle(float angle);
};

float distance(eoVector2 one, eoVector2 other);
eoVector2 mix(eoVector2 one, eoVector2 other, float amount);
float min(float a, float b);
float max(float a, float b);

eoVector2 min(eoVector2 self, eoVector2 other);
eoVector2 max(eoVector2 self, eoVector2 other);


#endif //GFX_PROTOTYPE_EOVECTOR2_H
