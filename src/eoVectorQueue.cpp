//
// Created by ego on 8/5/18.
//

#include "eoVectorQueue.h"

eoVectorQueue::eoVectorQueue()
= default;

void eoVectorQueue::addVector(float x, float y) {
    renderQueue.emplace_back(eoVector2(x, y));
}

void eoVectorQueue::clearQueue() {
    renderQueue.clear();
}

eoVector2 eoVectorQueue::popQueue() {
    eoVector2 last = getLast();
    renderQueue.pop_back();
    return last;
}

void eoVectorQueue::updateDynamicBuffer(eoVertexBufferDynamic *buffer) {

    buffer->update(&(renderQueue[0]), renderQueue.size());
}

eoVector2 eoVectorQueue::getLast() {
    return renderQueue[renderQueue.size()-1];
}

void eoVectorQueue::addVector(eoVector2 vector) {
    renderQueue.emplace_back(vector);
}
