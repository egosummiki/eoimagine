//
// Created by ego on 8/5/18.
//

#ifndef GFX_PROTOTYPE_EOVECTORQUEUE_H
#define GFX_PROTOTYPE_EOVECTORQUEUE_H

#include <vector>
#include "eoVector2.h"
#include "eoCanvas.h"
#include "eoVertexBufferDynamic.h"

class eoVectorQueue {

    std::vector<eoVector2> renderQueue;

public:

    eoVectorQueue();

    void addVector(float x, float y);
    void addVector(eoVector2 vector);
    void clearQueue();
    eoVector2 popQueue();
    eoVector2 getLast();
    void updateDynamicBuffer(eoVertexBufferDynamic* buffer);

};


#endif //GFX_PROTOTYPE_EOVECTORQUEUE_H
