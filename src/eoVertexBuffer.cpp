//
// Created by ego on 11/29/17.
//

#include "eoVertexBuffer.h"

eoVertexBuffer* eoVertexBuffer::FULL_DIMENSION;
eoVertexBuffer* eoVertexBuffer::UNIT_CIRCLE;

eoVertexBuffer::eoVertexBuffer(GLenum mode, int numVertex) : mode(mode), numVertex(numVertex) {
    glGenVertexArrays(1, &arrayId);
    glBindVertexArray(arrayId);
    glGenBuffers(1, &buffer);


    data = new GLfloat[4*numVertex];
    data_index = data;
}

eoVertexBuffer::~eoVertexBuffer() {

    delete data;
    glDeleteBuffers(1, &buffer);
    glDeleteVertexArrays(1, &arrayId);
}

void eoVertexBuffer::pushVertex(GLfloat x, GLfloat y, GLfloat u, GLfloat v) {

    *data_index = x;
    data_index++;
    *data_index = y;
    data_index++;
    *data_index = u;
    data_index++;
    *data_index = v;
    data_index++;
}

void eoVertexBuffer::finish() {
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*4*numVertex, data, GL_STATIC_DRAW);
}

void eoVertexBuffer::render() {
    glBindBuffer(GL_ARRAY_BUFFER, buffer);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4*sizeof(float), (void*)0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4*sizeof(float), (void*)(2*sizeof(float)));

    glDrawArrays(mode, 0, numVertex);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);

}

void eoVertexBuffer::generateStaticBuffers() {

    FULL_DIMENSION = new eoVertexBuffer(GL_TRIANGLES, 6);
    FULL_DIMENSION->pushVertex(-1,  1, 0.0f, 1.0f);
    FULL_DIMENSION->pushVertex( 1,  1, 1.0f, 1.0f);
    FULL_DIMENSION->pushVertex(-1, -1, 0.0f, 0.0f);
    FULL_DIMENSION->pushVertex( 1,  1, 1.0f, 1.0f);
    FULL_DIMENSION->pushVertex( 1, -1, 1.0f, 0.0f);
    FULL_DIMENSION->pushVertex(-1, -1, 0.0f, 0.0f);
    FULL_DIMENSION->finish();

    UNIT_CIRCLE = circle(1.0f, 64);
}

eoVertexBuffer *eoVertexBuffer::square(float width, float height) {

    auto result = new eoVertexBuffer(GL_TRIANGLES, 6);
    result->pushVertex(-width,  height, 0.0f, 1.0f);
    result->pushVertex( width,  height, 1.0f, 1.0f);
    result->pushVertex(-width, -height, 0.0f, 0.0f);
    result->pushVertex( width,  height, 1.0f, 1.0f);
    result->pushVertex( width, -height, 1.0f, 0.0f);
    result->pushVertex(-width, -height, 0.0f, 0.0f);
    result->finish();

    return result;
}

eoVertexBuffer *eoVertexBuffer::square(eoVector2 vector) {
    return square(vector.getX() / 2.0f, vector.getY() / 2.0f);
}

eoVertexBuffer *eoVertexBuffer::canvas(eoCanvas *info) {
    return square(info->getMiddleWidth(), info->getMiddleHeight());
}

#define eoTAU 6.283185f
#include <cmath>

eoVertexBuffer *eoVertexBuffer::circle(float radius, float cuts) {
    
    auto result = new eoVertexBuffer(GL_TRIANGLES, 3*cuts);

    for(float i = 0.0f; i < cuts;) {

        result->pushVertex(
                radius*cos(i/cuts*eoTAU),
                radius*sin(i/cuts*eoTAU),
                (cos(i/cuts*eoTAU) + 1.0f)/2.0f,
                (sin(i/cuts*eoTAU) + 1.0f)/2.0f);

        result->pushVertex(0.0f, 0.0f, 0.5f, 0.5f);

        ++i;

        result->pushVertex(
                radius*cos(i/cuts*eoTAU),
                radius*sin(i/cuts*eoTAU),
                (cos(i/cuts*eoTAU) + 1.0f)/2.0f,
                (sin(i/cuts*eoTAU) + 1.0f)/2.0f);
    }

    result->finish();

    return result;
}

eoVertexBuffer *eoVertexBuffer::squareCorner(float width, float height) {
    
    auto result = new eoVertexBuffer(GL_TRIANGLES, 6);

    result->pushVertex(0.0f,  height, 0.0f, 0.0f);
    result->pushVertex(width, height, 1.0f, 0.0f);
    result->pushVertex(0.0f,  0.0f,   0.0f, 1.0f);
    result->pushVertex(width, height, 1.0f, 0.0f);
    result->pushVertex(width, 0.0f,   1.0f, 1.0f);
    result->pushVertex(0.0f,  0.0f,   0.0f, 1.0f);
    result->finish();

    return result;
}
