//
// Created by ego on 11/29/17.
//

#ifndef GFX_PROTOTYPE_EOVERTEXBUFFER_H
#define GFX_PROTOTYPE_EOVERTEXBUFFER_H

/*
 *  Vertex buffer
 *
 *  Standard is
 *  (x, y, u ,v)
 *
 * */

#include <GL/gl.h>
#include <GLES3/gl32.h>
#include "eoCanvas.h"


class eoVertexBuffer {
    int numVertex;
    GLfloat* data;
    GLfloat* data_index;
    GLuint buffer;
    GLuint arrayId;
    GLenum mode;
public:
    eoVertexBuffer(GLenum mode, int numVertex);
    ~eoVertexBuffer();

    void pushVertex(GLfloat x, GLfloat y, GLfloat u, GLfloat v);
    void finish();
    void render();

    static void generateStaticBuffers();
    static eoVertexBuffer* square(float width, float height);
    static eoVertexBuffer* square(eoVector2 vector);
    static eoVertexBuffer* squareCorner(float width, float height);
    static eoVertexBuffer* circle(float radius, float cuts);
    static eoVertexBuffer* canvas(eoCanvas* info);

    static eoVertexBuffer* FULL_DIMENSION;
    static eoVertexBuffer* UNIT_CIRCLE;
};


#endif //GFX_PROTOTYPE_EOVERTEXBUFFER_H
