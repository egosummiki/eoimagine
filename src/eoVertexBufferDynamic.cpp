//
// Created by ego on 12/15/17.
//

#include "eoVertexBufferDynamic.h"

eoVertexBufferDynamic::eoVertexBufferDynamic(GLenum mode) : mode(mode) {
    glGenVertexArrays(1, &arrayID);
    glBindVertexArray(arrayID);
    glGenBuffers(1, &buffer);
}

void eoVertexBufferDynamic::update(eoVector2 *data, size_t size) {
    bufferSize = size;

    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(eoVector2)*bufferSize, data, GL_STATIC_DRAW);
}

void eoVertexBufferDynamic::render() {
    glBindBuffer(GL_ARRAY_BUFFER, buffer);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(eoVector2), nullptr);

    glDrawArrays(mode, 0, bufferSize);

    glDisableVertexAttribArray(0);
}
