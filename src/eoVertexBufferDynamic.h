//
// Created by ego on 12/15/17.
//

#ifndef GFX_PROTOTYPE_EOVERTEXBUFFERDYNAMIC_H
#define GFX_PROTOTYPE_EOVERTEXBUFFERDYNAMIC_H

#include <cstdio>
#include <GL/gl.h>
#include <GLES3/gl32.h>

#include "eoVector2.h"

class eoVertexBufferDynamic {
    GLenum mode;
    GLuint buffer;
    GLuint arrayID;

    size_t bufferSize;
public:
    explicit eoVertexBufferDynamic(GLenum mode);
    void update(eoVector2* data, size_t size);
    void render();
};


#endif //GFX_PROTOTYPE_EOVERTEXBUFFERDYNAMIC_H
