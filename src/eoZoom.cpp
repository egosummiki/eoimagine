//
// Created by ego on 15/10/18.
//

#include "eoZoom.h"
#include "eoInput.h"

eoZoom::eoZoom() {

}

void eoZoom::bindZoomTransformations(eoInput *input, eoWindowMain *mainWindow) {
    
    input->onWheel([this, mainWindow] (QWheelEvent* event) {

        float magnification = pow(1.05f, event->delta()/100.0f);

        scaleFactor *= magnification;
        offset      *= magnification;

        getZoomMatrix().setIdentity();
        getZoomMatrix().translate(offset);
        getZoomMatrix().scale(scaleFactor);

        mainWindow->getStatusBar()->updateZoom(scaleFactor);

    });

    input->onMotion([this, mainWindow] (eoMouseEvent* event) {

        mainWindow->getStatusBar()->updateMousePos(event->getCanvasX(), event->getCanvasY());

        if(!moveMode)
            return;

        offset += (event->getPosition() - lastMousePos).flipY();

        getZoomMatrix().setIdentity();
        getZoomMatrix().translate(offset);
        getZoomMatrix().scale(scaleFactor);

        lastMousePos = event->getPosition();
    });

    input->onButtonPress([this] (eoMouseEvent* event) {

        if(event->button() == Qt::MidButton) {
            moveMode = true;
            lastMousePos = event->getPosition();
        }
    });

    input->onButtonRelease([this] (eoMouseEvent* event) {

        if(event->button() == Qt::MidButton) {
            moveMode = false;
        }
    });

}

eoMatrix& eoZoom::getZoomMatrix() {
    return zoomMatrix;
}

float eoZoom::getScaleFactor() const {
    
    return scaleFactor;
}

float eoZoom::getOffsetX() const {
    return offset.getX();
}

float eoZoom::getOffsetY() const {
    return offset.getY();
}
