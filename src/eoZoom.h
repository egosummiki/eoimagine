#pragma once
//
// Created by ego on 15/10/18.
//
#include "eoMatrix.h"

class eoInput;
class eoWindowMain;

class eoZoom {

private:

    eoMatrix zoomMatrix = eoMatrix();
    eoVector2 offset = eoVector2(0.0f, 0.0f);
    eoVector2 lastMousePos = eoVector2(0.0f, 0.0f);
    float scaleFactor = 1.0f;
    bool moveMode = false;

public:
    eoZoom();

    void bindZoomTransformations(eoInput *input, eoWindowMain *mainWindow);
    eoMatrix& getZoomMatrix();
    float getScaleFactor() const;
    float getOffsetX() const;
    float getOffsetY() const;
};
