#include "ui/eoWindowMain.h"
#include "eoApp.h"
#include "eoException.h"

int main(int argc, char** argv)
{
    eoApp app(argc, argv);

    auto window = new eoWindowMain("No document", 800, 600);
    window->show();

    return app.run();
}
