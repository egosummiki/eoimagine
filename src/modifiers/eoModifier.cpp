#include <utility>

//
// Created by ego on 12/3/17.
//

#include "eoModifier.h"
#include "eoModifierFill.h"

/*
 *  Effect variables np.
 *
 *  Contrast
 *
 *  color = clamp( contrast*(color.rgb - 0.5) + 0.5, 0, 1 );
 *
 *  the variable is contrast so we should input
 * */

eoModifier::eoModifier(Role role, std::string name)
    : role(role), name(std::move(name))
{
}

void eoModifier::attachArgument(Argument argument) {
    arguments.emplace_back(argument);
}

int eoModifier::getArgumentCount() {
    return (int)arguments.size();
}

eoModifier::Argument eoModifier::getArgument(int id) {
    return arguments[id];
}

eoModifier::Argument::Argument(eoType type, const std::string &name) : type(type), name(name) {}

eoType eoModifier::Argument::getType() const {
    return type;
}

const std::string &eoModifier::Argument::getName() const {
    return name;
}

eoModifierFill* eoModifier::CONTRAST;
eoModifierFill* eoModifier::BRIGHTNESS;
eoModifier* eoModifier::TRANSLATE;
eoModifier* eoModifier::ROTATE;
eoModifier* eoModifier::SCALE;

void eoModifier::generateModifiers() {
    CONTRAST = new eoModifierFill(
            "contrast",
            "color = vec4( clamp( contrast*(color.rgb - 0.5) + 0.5, 0, 1 ), color.a);\n");
    CONTRAST->attachArgument(eoModifier::Argument(eoType::FLOAT, "contrast"));

	BRIGHTNESS = new eoModifierFill(
	        "brightness",
	        "color = vec4( clamp( color.rgb + brightness/255.0, 0, 1 ), color.a);\n");
	BRIGHTNESS->attachArgument(eoModifier::Argument(eoType::FLOAT, "brightness"));

    TRANSLATE = new eoModifier(TRANSFORM, "translate");
    TRANSLATE->attachArgument(eoModifier::Argument(eoType::VEC2, "position"));

    ROTATE = new eoModifier(TRANSFORM, "rotate");
    ROTATE->attachArgument(eoModifier::Argument(eoType::FLOAT, "rotation"));

    SCALE = new eoModifier(TRANSFORM, "scale");
    SCALE->attachArgument(eoModifier::Argument(eoType::VEC2, "scale"));
}

eoModifier::Role eoModifier::getRole() const {
    return role;
}

const std::string &eoModifier::getName() const {
    return name;
}
