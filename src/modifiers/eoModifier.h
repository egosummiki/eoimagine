//
// Created by ego on 12/3/17.
//

#ifndef GFX_PROTOTYPE_EOEFFECT_H
#define GFX_PROTOTYPE_EOEFFECT_H

#include <string>
#include <vector>
#include "../eoType.h"

class eoModifierFill;

class eoModifier {

public:
    class Argument {
        eoType type;
        std::string name;

    public:
        Argument(eoType type, const std::string &name);
        eoType getType() const;
        const std::string &getName() const;
    };

    enum Role {
        NONE,
        TRANSFORM,
        FILL
    };

private:

    std::vector<Argument> arguments;
    Role role;
    std::string name;
public:
    eoModifier(Role role, std::string name);
    void attachArgument(Argument argument);
    int getArgumentCount();
    Argument getArgument(int id);

    static eoModifierFill* BRIGHTNESS;
    static eoModifierFill* CONTRAST;
    static eoModifier* TRANSLATE;
    static eoModifier* ROTATE;
    static eoModifier* SCALE;
    static void generateModifiers();

    const std::string &getName() const;
    Role getRole() const;
};


#endif //GFX_PROTOTYPE_EOEFFECT_H

