#include <utility>

#include <utility>

//
// Created by ego on 8/22/18.
//

#include "eoModifierFill.h"

eoModifierFill::eoModifierFill(std::string name, std::string code)
        : eoModifier(eoModifier::FILL, std::move(name)), code(std::move(code))
{

}

const std::string &eoModifierFill::getCode() const {
    return code;
}
