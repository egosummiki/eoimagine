//
// Created by ego on 8/22/18.
//

#ifndef EOIMAGINE_EOMODIFIERFILL_H
#define EOIMAGINE_EOMODIFIERFILL_H

#include "eoModifier.h"
#include <string>

class eoModifierFill : public eoModifier {

private:
    std::string code;

public:
    eoModifierFill(std::string name, std::string code);
    const std::string &getCode() const;
};


#endif //EOIMAGINE_EOMODIFIERFILL_H
