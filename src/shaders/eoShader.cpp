//
// Created by ego on 11/24/17.
//

#include "eoShader.h"
#include "../eoException.h"

#include "../eoMessageBox.h"
#include "eoShaderFragmentTrivial.h"
#include "eoShaderVertexTrivial.h"
#include "eoShaderFile.h"
#include "eoShaderScreenDimension.h"
#include "../eoRender.h"

eoShader::eoShader(GLenum type) : type(type) {
    id = glCreateShader(type);
    onUseCallback = nullptr;
}

eoShader::~eoShader() {
    glDeleteShader(id);
}

GLuint eoShader::getId() {
    return id;
}

GLenum eoShader::getType() {
    return type;
}

void eoShader::compileShader(const char* code, int code_size) {
    try {
        glShaderSource(id, 1, &code , &code_size);
        glCompileShader(id);

        GLint result = GL_FALSE;
        int infoLogLength;

        glGetShaderiv(id, GL_COMPILE_STATUS, &result);
        glGetShaderiv(id, GL_INFO_LOG_LENGTH, &infoLogLength);

        if(infoLogLength > 0)
        {
            auto infoLog = new char[infoLogLength + 1];
            glGetShaderInfoLog(id, infoLogLength, nullptr, infoLog);
            throw eoExceptionShaderCompile(infoLog);
        }
    } catch(eoException& exception)
    {
        exception.showMessageBox();
    }



}

void eoShader::onUse(std::function<void(void*)> callback) {
    onUseCallback = std::move(callback);
    onUseCallbackReady = true;
}

bool eoShader::onUseReady() {
    return onUseCallbackReady;
}

eoShader* eoShader::VERTEX_TRIVIAL;
eoShader* eoShader::VERTEX_SCREENDIM;

eoShader* eoShader::FRAGMENT_TRIVIAL;
eoShader* eoShader::FRAGMENT_PREMUL;
eoShader* eoShader::FRAGMENT_NORMAL;
eoShader* eoShader::FRAGMENT_SELECTION;
eoShader* eoShader::FRAGMENT_CHECKERBOARD;

void eoShader::loadShaders(eoRender *render) {

    VERTEX_TRIVIAL          = new eoShaderVertexTrivial();
    VERTEX_SCREENDIM        = new eoShaderScreenDimension(render);

    FRAGMENT_TRIVIAL        = new eoShaderFragmentTrivial();
    FRAGMENT_NORMAL         = new eoShaderFile(GL_FRAGMENT_SHADER, "data/glsl/blend/normal.glsl");
    FRAGMENT_PREMUL         = new eoShaderFile(GL_FRAGMENT_SHADER, "data/glsl/fragment_premul.glsl");
    FRAGMENT_SELECTION      = new eoShaderFile(GL_FRAGMENT_SHADER, "data/glsl/fragment_selection.glsl");
    FRAGMENT_CHECKERBOARD   = new eoShaderFile(GL_FRAGMENT_SHADER, "data/glsl/fragment_checkerboard.glsl");
}

void eoShader::releaseShaders() {

    delete FRAGMENT_TRIVIAL;
    delete VERTEX_TRIVIAL;

}
