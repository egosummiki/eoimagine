//
// Created by ego on 11/24/17.
//

#ifndef GFX_PROTOTYPE_EOSHADER_H
#define GFX_PROTOTYPE_EOSHADER_H

#include <GL/gl.h>
#include <GLES3/gl32.h>
#include <vector>
#include <functional>

class eoRender;

class eoShader {
protected:
    bool onUseCallbackReady = false;
    GLuint id;
    GLenum type;
    void compileShader(const char* code, int code_size);
public:
    explicit eoShader(GLenum type);
    ~eoShader();

    GLuint getId();
    GLenum getType();
    virtual void onUse(std::function<void(void*)> callback);
    bool onUseReady();

    std::function<void(void*)> onUseCallback;

    static void loadShaders(eoRender *render);
    static void releaseShaders();

    static eoShader* VERTEX_TRIVIAL;
    static eoShader* VERTEX_SCREENDIM;
    static eoShader* FRAGMENT_TRIVIAL;
    static eoShader* FRAGMENT_PREMUL;
    static eoShader* FRAGMENT_NORMAL;
    static eoShader* FRAGMENT_SELECTION;
    static eoShader* FRAGMENT_CHECKERBOARD;
};


#endif //GFX_PROTOTYPE_EOSHADER_H
