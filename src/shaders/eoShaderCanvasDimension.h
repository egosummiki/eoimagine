//
// Created by ego on 12/7/17.
//

#ifndef GFX_PROTOTYPE_EOSHADERPIXELDIMENSION_H
#define GFX_PROTOTYPE_EOSHADERPIXELDIMENSION_H

#include "eoShader.h"
#include "../eoCanvas.h"

class eoShaderCanvasDimension : public eoShader {
public:
    explicit eoShaderCanvasDimension(eoCanvas *canvas);
};


#endif //GFX_PROTOTYPE_EOSHADERPIXELDIMENSION_H
