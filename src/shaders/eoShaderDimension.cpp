//
// Created by ego on 8/6/18.
//

#include <cstring>
#include "eoShaderDimension.h"
#include "../eoGLProgram.h"

eoShaderDimension::eoShaderDimension(float middleWidth, float middleHeight)
    : eoShader(GL_VERTEX_SHADER)
{

    const char* shader_code =
            "#version 330 core\n"
            "layout(location = 0) in vec2 pos;\n"
            "layout(location = 1) in vec2 uv;\n"
            "out vec2 Texcoord;\n"
            "uniform mat3 transform;\n"
            "uniform vec2 screenHalfSize;"
            "void main()\n"
            "{\n"
            "\tTexcoord = uv;\n"
            "\tgl_Position.xyzw = vec4(transform * vec3(pos, 1) / vec3(screenHalfSize, 1), 1);\n"
            "}\n";

    compileShader(shader_code, static_cast<int>(strlen(shader_code)));

    onUse([middleWidth, middleHeight] (void* _program) {
        auto program = (eoGLProgram*) _program;
        program->setVec2("screenHalfSize", middleWidth, middleHeight);
    });

}
