//
// Created by ego on 8/6/18.
//

#ifndef GFX_PROTOTYPE_EOSHADERDIMENSION_H
#define GFX_PROTOTYPE_EOSHADERDIMENSION_H

#include "eoShader.h"

class eoShaderDimension : public eoShader {
public:
    eoShaderDimension(float middleWidth, float middleHeight);

};


#endif //GFX_PROTOTYPE_EOSHADERDIMENSION_H
