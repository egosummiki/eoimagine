//
// Created by ego on 11/25/17.
//

#include "eoShaderFile.h"

#include "../eoException.h"
#include <fstream>

eoShaderFile::eoShaderFile(GLenum type, const char *path) : eoShader(type)
{
    try{
        char* shader_code;
        std::streampos shader_code_size;

        std::ifstream shader_file(path, std::ios::in|std::ios::binary|std::ios::ate);
        if(!shader_file.is_open())
        {
            throw eoExceptionShaderNoFile(path);
        }

        shader_code_size = shader_file.tellg();
        shader_code = new char[shader_code_size];

        shader_file.seekg(0, std::ios::beg);
        shader_file.read(shader_code, shader_code_size);
        shader_file.close();

        compileShader(shader_code, shader_code_size);

        delete[] shader_code;
    } catch (eoException& exception)
    {
        exception.showMessageBox();
    }

}
