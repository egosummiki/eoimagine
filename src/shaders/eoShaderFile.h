//
// Created by ego on 11/25/17.
//

#ifndef GFX_PROTOTYPE_EOSHADERFILE_H
#define GFX_PROTOTYPE_EOSHADERFILE_H

#include "eoShader.h"

class eoShaderFile : public eoShader {
public:
    eoShaderFile(GLenum type, const char* path);
};


#endif //GFX_PROTOTYPE_EOSHADERFILE_H
