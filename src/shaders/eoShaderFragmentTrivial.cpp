//
// Created by ego on 11/28/17.
//

#include <cstring>
#include "eoShaderFragmentTrivial.h"

eoShaderFragmentTrivial::eoShaderFragmentTrivial() : eoShader(GL_FRAGMENT_SHADER)
{
    const char* shader_code =
        "#version 330 core\n"
        "in vec2 Texcoord;\n"
        "out vec4 color;\n"
        "uniform sampler2D tex;\n"
        "void main()\n"
        "{\n"
        "\tcolor = texture(tex, Texcoord);\n"
        "}\n"
    ;

    compileShader(shader_code, strlen(shader_code));
}
