//
// Created by ego on 12/3/17.
//

#include "eoShaderModifiers.h"
#include "../modifiers/eoModifierFill.h"
#include "../eoType.h"


eoShaderModifiers::eoShaderModifiers(BlendingMode mode)
    : eoShader(GL_FRAGMENT_SHADER), mode(mode)
{

    glsl_vars << "#version 330 core\n"
            "in vec2 Texcoord;\n"
            "out vec4 color;\n"
            "uniform sampler2D source_texture;\n"
            "uniform sampler2D dest_texture;\n"
            "uniform float opacity;\n"
            "uniform vec2 canvasMid;\n"
            "uniform vec2 position;\n"
            "uniform vec2 sizeMid;\n"
            "uniform vec2 scale;\n"
            "uniform float rotation;\n";

    glsl_code << "void main()\n"
            "{\n"
            "vec4 source = texture(source_texture, Texcoord);\n"
            "vec2 destAt = (Texcoord*2-1)*sizeMid;\n"
            "float destAngle = acos(destAt.y / length(destAt));\n"
            "if(destAt.x < 0) destAngle *= -1;\n"
            "vec2 destTrans = length(destAt)*vec2(sin(destAngle - rotation), cos(destAngle - rotation))*scale + position;\n"
            "vec4 dest = texture(dest_texture, destTrans/canvasMid/2+0.5);\n"
            "vec3 target = vec3(0,0,0);\n"
            "if(source.a > 0) source.rgb = source.rgb / source.a;\n"
            "float output_color_blend = opacity * source.a;\n"
            "float output_alpha = clamp(source.a + dest.a, 0, 1);\n";

    switch(mode) {
        case NORMAL:
            glsl_code << "target = source.rgb;";
            break;
        case DARKEN:
            glsl_code << "target = vec3(min(source.r, dest.r), min(source.g, dest.g), min(source.b, dest.b));";
            break;
        case MULTIPLY:
            glsl_code << "target = vec3(source.rgb * dest.rgb);";
            break;
        case COLOR_BURN:
            glsl_code <<
                "target = vec3("
                "source.r > 0 ? 1 - (1 - dest.r)/source.r : 0,"
                "source.g > 0 ? 1 - (1 - dest.g)/source.g : 0,"
                "source.b > 0 ? 1 - (1 - dest.b)/source.b : 0);";
            break;
        case LINEAR_BURN:
            glsl_code <<
                "target = vec3("
                "source.rgb + dest.rgb - vec3(1,1,1));";
            break;
        case LIGHTEN:
            glsl_code <<
                "target = vec3("
                "max(source.r, dest.r), "
                "max(source.g, dest.g), "
                "max(source.b, dest.b));";
            break;
        case SCREEN:
            glsl_code <<
                "target = vec3("
                "1 - (1 - source.r)*(1 - dest.r), "
                "1 - (1 - source.g)*(1 - dest.g), "
                "1 - (1 - source.b)*(1 - dest.b));";
            break;
        case COLOR_DODGE:
            glsl_code <<
                "target = vec3("
                "dest.r/(1 - source.r), "
                "dest.g/(1 - source.g), "
                "dest.b/(1 - source.b));";
            break;
        case LINEAR_DODGE:
            glsl_code <<
                "target = vec3("
                "clamp(source.r + dest.r, 0, 1), "
                "clamp(source.g + dest.g, 0, 1), "
                "clamp(source.b + dest.b, 0, 1));";
            break;
        case OVERLAY:
            glsl_code <<
                "target = vec3("
                "clamp(dest.r < 0.5 ? 2 * dest.r * source.r : 1 - 2*(1 - dest.r)*(1 - source.r), 0, 1), "
                "clamp(dest.g < 0.5 ? 2 * dest.g * source.g : 1 - 2*(1 - dest.g)*(1 - source.g), 0, 1), "
                "clamp(dest.b < 0.5 ? 2 * dest.b * source.b : 1 - 2*(1 - dest.b)*(1 - source.b), 0, 1)); ";
            break;
        case SOFT_LIGHT:
            glsl_code <<
                "target = vec3("
                "source.r < 0.5 ? 2 * dest.r * source.r + (dest.r)*(dest.r) * (1 - 2 * source.r) : 2*dest.r*(1-source.r) + sqrt(dest.r)*(2*source.r - 1), "
                "source.g < 0.5 ? 2 * dest.g * source.g + (dest.g)*(dest.g) * (1 - 2 * source.g) : 2*dest.g*(1-source.g) + sqrt(dest.g)*(2*source.g - 1), "
                "source.b < 0.5 ? 2 * dest.b * source.b + (dest.b)*(dest.b) * (1 - 2 * source.b) : 2*dest.b*(1-source.b) + sqrt(dest.b)*(2*source.b - 1)); " ;

            break;
        case HARD_LIGHT:
            glsl_code <<
                "target = vec3("
                "dest.r > 0.5 ? (1 - (1 - source.r) * (1 - 2*(dest.r - 0.5))) : (source.r*2*dest.r), "
                "dest.g > 0.5 ? (1 - (1 - source.g) * (1 - 2*(dest.g - 0.5))) : (source.g*2*dest.g), "
                "dest.b > 0.5 ? (1 - (1 - source.b) * (1 - 2*(dest.b - 0.5))) : (source.b*2*dest.b)); "
                ;
            break;
        case VIVID_LIGHT:
            glsl_code <<
                "target = vec3("
                "source.r > 0.5 ? (1 - (1 - dest.r) * (2*(source.r - 0.5))) : (dest.r*(1 - 2*source.r)), "
                "source.g > 0.5 ? (1 - (1 - dest.g) * (2*(source.g - 0.5))) : (dest.g*(1 - 2*source.g)), "
                "source.b > 0.5 ? (1 - (1 - dest.b) * (2*(source.b - 0.5))) : (dest.b*(1 - 2*source.b))); "
                ;
            break;
        case LINEAR_LIGHT:
            glsl_code <<
                "target = vec3("
                "dest.r > 0.5 ? (source.r + 2*(dest.r - 0.5)) : (source.r + 2*dest.r - 1), "
                "dest.g > 0.5 ? (source.g + 2*(dest.g - 0.5)) : (source.g + 2*dest.g - 1), "
                "dest.b > 0.5 ? (source.b + 2*(dest.b - 0.5)) : (source.b + 2*dest.b - 1)); "
                ;
            break;
        case PIN_LIGHT:
            glsl_code <<
                "target = vec3("
                "dest.r > 0.5 ? (max(source.r, 2*(dest.r - 0.5))) : (min(source.r, 2*dest.r)), "
                "dest.g > 0.5 ? (max(source.g, 2*(dest.g - 0.5))) : (min(source.g, 2*dest.g)), "
                "dest.b > 0.5 ? (max(source.b, 2*(dest.b - 0.5))) : (min(source.b, 2*dest.b))); "
                ;
            break;
        case DIFFERENCE:
            glsl_code <<
                "target = vec3(abs(source.rgb - dest.rgb)); ";
            break;
        case EXCLUSION:
            glsl_code <<
                "target = vec3(vec3(0.5, 0.5, 0.5) + 2*(source.r - vec3(0.5, 0.5, 0.5)*(dest.r - vec3(0.5, 0.5, 0.5)))); ";
            break;

        default: break;
    }

    glsl_code << "color = vec4(target*output_color_blend + dest.rgb*(1-output_color_blend), output_alpha);";
}

void eoShaderModifiers::appendEffect(eoModifierFill *modifier) {

    for(int i = 0; i < modifier->getArgumentCount(); i++)
    {
        eoModifier::Argument argument = modifier->getArgument(i);

        glsl_vars << "uniform ";
        switch (argument.getType())
        {
            case eoType::FLOAT:
                glsl_vars << "float ";
                break;
            case eoType::VEC2:
                glsl_vars << "vec2 ";
                break;
            case eoType::VEC3:
                glsl_vars << "vec3 ";
                break;
            case eoType::VEC4:
                glsl_vars << "vec4 ";
                break;
            default:
                break;
        }

        glsl_vars << argument.getName() << ";\n";
    }

    glsl_code << modifier->getCode();
}

void eoShaderModifiers::finish() {
    glsl_vars << glsl_code.str() << "}";
    std::string result_code = glsl_vars.str();
    compileShader(result_code.c_str(), (int)result_code.size());
}
