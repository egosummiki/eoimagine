//
// Created by ego on 12/3/17.
//

#ifndef GFX_PROTOTYPE_EOSHADEREFFECTS_H
#define GFX_PROTOTYPE_EOSHADEREFFECTS_H

#include "eoShader.h"
#include "../modifiers/eoModifier.h"
#include "../modifiers/eoModifierFill.h"
#include <string>
#include <sstream>


class eoShaderModifiers : public eoShader {

public:

    enum BlendingMode :int {
        NORMAL,
        DARKEN,
        MULTIPLY,
        COLOR_BURN,
        LINEAR_BURN,
        LIGHTEN,
        SCREEN,
        COLOR_DODGE,
        LINEAR_DODGE,
        OVERLAY,
        SOFT_LIGHT,
        HARD_LIGHT,
        VIVID_LIGHT,
        LINEAR_LIGHT,
        PIN_LIGHT,
        DIFFERENCE,
        EXCLUSION
    };

private:

    std::stringstream glsl_vars;
    std::stringstream glsl_code;
    BlendingMode mode;

public:
    eoShaderModifiers(BlendingMode mode);
    void appendEffect(eoModifierFill *modifier);
    void finish();
};


#endif //GFX_PROTOTYPE_EOSHADEREFFECTS_H
