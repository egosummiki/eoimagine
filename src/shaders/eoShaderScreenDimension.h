//
// Created by ego on 7/2/18.
//

#ifndef GFX_PROTOTYPE_EOSHADERSCREENDIMENSION_H
#define GFX_PROTOTYPE_EOSHADERSCREENDIMENSION_H


#include "eoShader.h"
#include "../eoRender.h"

class eoShaderScreenDimension : public eoShader {
public:
    eoShaderScreenDimension(eoRender *screenDimension);

};


#endif //GFX_PROTOTYPE_EOSHADERSCREENDIMENSION_H
