//
// Created by ego on 11/25/17.
//

#include <cstring>
#include "eoShaderVertexTrivial.h"

eoShaderVertexTrivial::eoShaderVertexTrivial() : eoShader(GL_VERTEX_SHADER)
{
    const char* shader_code =
        "#version 330 core\n"
        "layout(location = 0) in vec2 pos;\n"
        "layout(location = 1) in vec2 uv;\n"
        "out vec2 Texcoord;\n"
        "uniform mat3 transform;\n"
        "void main()\n"
        "{\n"
        "\tTexcoord = uv;\n"
        "\tgl_Position.xyzw = vec4(transform * vec3(pos, 1), 1);\n"
        "}\n";

    compileShader(shader_code, strlen(shader_code));
}
