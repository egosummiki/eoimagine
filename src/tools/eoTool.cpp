//
// Created by ego on 12/9/17.
//

#include "eoTool.h"

eoTool::eoTool(eoRender* render) : render(render) {}

eoTool::~eoTool() = default;

void eoTool::onTick() {

}

eoTool::CastingResult eoTool::onCast() {

    return ResultSuccess;
}

void eoTool::onLose() {

}

void eoTool::onLayerChange() {

    if(result == ResultSuccess)
        onLose();

    cast();
}

void eoTool::onCanvasChange(eoCanvas *freshCanvas) {

    canvas = freshCanvas;    
}

eoTool::SelectionLabel eoTool::getLabel() {
    
    return (eoTool::SelectionLabel) {"NORMAL", "#24cd6a", "#31363b"};
}

#include "eoToolBrush.h"
#include "eoToolTranslate.h"
#include "eoToolRotate.h"
#include "eoToolNormal.h"
#include "eoToolScale.h"
#include "eoToolRectSelect.h"
#include "eoToolCircleSelect.h"
#include "eoToolWand.h"
#include "eoToolText.h"
#include "eoToolCrop.h"

eoTool::ToolStruct eoTool::t;

void eoTool::generateTools(eoRender *render) {
    
    t.NORMAL        = new eoToolNormal(render);
    t.BRUSH         = new eoToolBrush(render);
    t.TRANSLATE     = new eoToolTranslate(render);
    t.ROTATE        = new eoToolRotate(render);
    t.SCALE         = new eoToolScale(render);
    t.RECTSELECT    = new eoToolRectSelect(render);
    t.CIRCLESELECT  = new eoToolCircleSelect(render);
    t.WAND          = new eoToolWand(render);
    t.TEXT          = new eoToolText(render);
    t.CROP          = new eoToolCrop(render);

}

QWidget *eoTool::getPropertiesWidget() {

    static QWidget *noWidget = new QWidget;
    
    return noWidget;
}

void eoTool::cast() {
   
    result = onCast();
}

eoTool::CastingResult eoTool::getLastResult() {
    
    return result;
}
