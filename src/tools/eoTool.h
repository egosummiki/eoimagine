//
// Created by ego on 12/9/17.
//

#ifndef GFX_PROTOTYPE_EOTOOL_H
#define GFX_PROTOTYPE_EOTOOL_H

#include "../eoCanvas.h"

class eoRender;
class QWidget;
class eoToolNormal;
class eoToolBrush;
class eoToolTranslate;
class eoToolRotate;
class eoToolScale;
class eoToolRectSelect;
class eoToolCircleSelect;
class eoToolWand;
class eoToolText;
class eoToolCrop;

class eoTool {

public:

    enum LockAxis {
        LockNone,
        LockX,
        LockY,
        LockKeepRatio
    };

    enum SelectionMode {

        SelectNormal,
        SelectAdd,
        SelectSubtract,
        SelectCommon
    };

    enum CastingResult {
        ResultNone,
        ResultSuccess,
        ResultFail
    };

    struct SelectionLabel {

        QString label, background, foreground;
    };

protected:
    eoRender *render = nullptr;
    eoCanvas *canvas = nullptr;
    CastingResult result = ResultNone;

public:
    explicit eoTool(eoRender* render);
    virtual ~eoTool();
    virtual void onTick();
    virtual CastingResult onCast();
    virtual void onLose();
    virtual void onLayerChange();
    virtual void onCanvasChange(eoCanvas *freshCanvas);
    virtual QWidget *getPropertiesWidget();
    virtual SelectionLabel getLabel();
    void cast();
    CastingResult getLastResult();


    static struct ToolStruct {

        eoToolNormal* NORMAL;
        eoToolBrush* BRUSH;
        eoToolTranslate* TRANSLATE;
        eoToolRotate* ROTATE;
        eoToolScale* SCALE;
        eoToolRectSelect* RECTSELECT;
        eoToolCircleSelect* CIRCLESELECT;
        eoToolWand* WAND;
        eoToolText* TEXT;
        eoToolCrop* CROP;

        eoTool *NOTOOL = nullptr; // null terminated tools.

    } t;


    static void generateTools(eoRender* render);
};


#endif //GFX_PROTOTYPE_EOTOOL_H
