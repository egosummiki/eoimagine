//
// Created by ego on 7/3/18.
//

#include <QtWidgets/QToolButton>
#include <QFileDialog>
#include <QSplitter>
#include "eoToolBar.h"
#include "../tools/eoToolBrush.h"
#include "../tools/eoToolScale.h" 
#include "../tools/eoToolRotate.h" 
#include "../tools/eoToolTranslate.h" 
#include "../tools/eoToolRectSelect.h" 
#include "../tools/eoToolCircleSelect.h" 
#include "../tools/eoToolWand.h" 
#include "../tools/eoToolText.h" 

eoToolBar::eoToolBar(eoWindowMain *window) : QToolBar(window), window(window) {

    brush = new QToolButton();
    brush->setIcon(QIcon("data/brush.png"));
    brush->setToolTip(tr("Brush Tool"));
    addWidget(brush);
    connect(brush, &QToolButton::released, this, &eoToolBar::useBrush);

    //stamp = new QToolButton();
    //stamp->setIcon(QIcon("data/stamp.png"));
    //stamp->setToolTip(tr("Stamp Tool"));
    //addWidget(stamp);
    //connect(stamp, &QToolButton::released, this, &eoToolBar::useStamp);

    grab = new QToolButton();
    grab->setIcon(QIcon("data/grab.png"));
    grab->setToolTip(tr("Grab Tool"));
    addWidget(grab);
    connect(grab, &QToolButton::released, this, &eoToolBar::useGrab);

    rotate = new QToolButton();
    rotate->setIcon(QIcon("data/rotate.png"));
    rotate->setToolTip(tr("Rotate Tool"));
    addWidget(rotate);
    connect(rotate, &QToolButton::released, this, &eoToolBar::useRotate);

    scale = new QToolButton();
    scale->setIcon(QIcon("data/scale.png"));
    scale->setToolTip(tr("Scale Tool"));
    addWidget(scale);
    connect(scale, &QToolButton::released, this, &eoToolBar::useScale);

    rectSelect = new QToolButton();
    rectSelect->setIcon(QIcon("data/rect-select.png"));
    rectSelect->setToolTip(tr("Rect Select Tool"));
    addWidget(rectSelect);
    connect(rectSelect, &QToolButton::released, this, &eoToolBar::useRectSelect);

    circleSelect = new QToolButton();
    circleSelect->setIcon(QIcon("data/circle-select.png"));
    circleSelect->setToolTip(tr("Circle Select Tool"));
    addWidget(circleSelect);
    connect(circleSelect, &QToolButton::released, this, &eoToolBar::useCircleSelect);

    wand = new QToolButton();
    wand->setIcon(QIcon("data/wand.png"));
    wand->setToolTip(tr("Wand Tool"));
    addWidget(wand);
    connect(wand, &QToolButton::released, this, &eoToolBar::useWand);

    text = new QToolButton();
    text->setIcon(QIcon("data/text.png"));
    text->setToolTip(tr("Text Tool"));
    addWidget(text);
    connect(text, &QToolButton::released, this, &eoToolBar::useText);
}

void eoToolBar::useBrush(void) {

    window->getRender()->useTool(eoTool::t.BRUSH);
}

void eoToolBar::useStamp(void) {

    //window->getRender()->useTool(eoTool::STAMP);
}

void eoToolBar::useGrab(void) {
    
    window->getRender()->useTool(eoTool::t.TRANSLATE);
}

void eoToolBar::useRotate(void) {
    
    window->getRender()->useTool(eoTool::t.ROTATE);
}

void eoToolBar::useScale(void) {
    
    window->getRender()->useTool(eoTool::t.SCALE);
}

void eoToolBar::useRectSelect(void) {
    
    window->getRender()->useTool(eoTool::t.RECTSELECT);
}

void eoToolBar::useCircleSelect(void) {
    
    window->getRender()->useTool(eoTool::t.CIRCLESELECT);
}

void eoToolBar::useWand(void) {
    
    window->getRender()->useTool(eoTool::t.WAND);
}

void eoToolBar::useText(void) {
    
    window->getRender()->useTool(eoTool::t.TEXT);
}
