//
// Created by ego on 7/3/18.
//

#ifndef GFX_PROTOTYPE_EOTOOLBAR_H
#define GFX_PROTOTYPE_EOTOOLBAR_H

#include <QToolBar>
#include "../ui/eoWindowMain.h"

class eoToolBar : public QToolBar {

    Q_OBJECT

    eoWindowMain* window;

    QToolButton* brush;
    QToolButton* stamp;

    QToolButton* grab;
    QToolButton* rotate;
    QToolButton* scale;

    QToolButton* rectSelect;
    QToolButton* circleSelect;

    QToolButton* wand;
    QToolButton* text;

public:
    explicit eoToolBar(eoWindowMain *window = nullptr);

private slots:

    void useBrush(void);
    void useStamp(void);
    void useGrab(void);
    void useRotate(void);
    void useScale(void);
    void useRectSelect(void);
    void useCircleSelect(void);
    void useWand(void);
    void useText(void);

};


#endif //GFX_PROTOTYPE_EOTOOLBAR_H
