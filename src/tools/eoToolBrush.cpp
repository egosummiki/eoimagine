//
// Created by ego on 12/9/17.
//

#include <cmath>
#include "eoToolBrush.h"
#include "../eoLayerRaster.h"
#include "../shaders/eoShaderFile.h"
#include "../shaders/eoShaderScreenDimension.h"
#include "../shaders/eoShaderVertexTrivial.h"
#include "../ui/eoWindowMain.h"
#include "../eoBlending.h"

#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QSlider>
#include <QLineEdit>

eoToolBrush::eoToolBrush(eoRender *render)
    :   eoTool(render),
        properties(new eoPropertiesBuilder())
{
    brushProgram = new eoGLProgram();

    auto brushFragment = new eoShaderFile(GL_FRAGMENT_SHADER, "data/glsl/tools/brush_fragment_new.glsl");
    auto brushGeometry = new eoShaderFile(GL_GEOMETRY_SHADER, "data/glsl/tools/brush_geometry_new.glsl");

    brushProgram->attachShader(brushFragment);
    brushProgram->attachShader(brushGeometry);
    brushProgram->attachShader(eoShader::VERTEX_TRIVIAL);
    brushProgram->link();

    delete brushFragment;
    delete brushGeometry;

    brushBuffer = new eoVertexBufferDynamic(GL_POINTS);

    properties->addColorSelect("Brush color", &brushColor);
    properties->addSplitter();
    properties->addFloatSlider("Brush size", 1, 512, &brushSize);
    properties->addFloatSlider("Brush hardness", 1, 64, &brushHardness);
    properties->addFloatSlider("Point separation", 1, 64, &brushOffset);
}

eoToolBrush::~eoToolBrush() {
    delete brushProgram;
    delete brushBuffer;
}


eoTool::CastingResult eoToolBrush::onCast() {

    if(!canvas || !canvas->getCurrentLayer()->hasAttribute(eoLayer::RASTER))
        return ResultFail;

    auto rasterLayer = dynamic_cast<eoLayerRaster*>(canvas->getCurrentLayer());
    aidBuffer = eoFrameBuffer::createPersevering(rasterLayer->getDimension());

    buttonPressID = canvas->getInput()->onButtonPress([this, rasterLayer] (eoMouseEvent* event) {

        if(event->button() != Qt::LeftButton)
            return;

        if(canvas->isInside(event)) {

            clearQueue();
            addVector(rasterLayer->traceBack(event->getCanvasPosition()));
            updateDynamicBuffer(brushBuffer);
            rasterLayer->begin();
        }

    });

    buttonReleaseID = canvas->getInput()->onButtonRelease([this, rasterLayer] (eoMouseEvent* event) {

        if(event->button() != Qt::LeftButton)
            return;

        rasterLayer->end();
    });

    buttonMotionID = canvas->getInput()->onMotion([this, rasterLayer] (eoMouseEvent* event) {

        switch(resizeMode) {

            case ResPREP:
            
                resizeMode = ResONGOING;
                begPosition = event->getScreenPosition();
                endPosition = event->getScreenPosition();
                return;

            case ResONGOING:
                endPosition = event->getScreenPosition();
                return;

            case ResFINISH:

                resizeMode = ResNONE;
                return;

            case ResNONE:

                begPosition = event->getScreenPosition();
        }

        if(canvas->getInput()->isLeftButtonHeld()) {

            if(canvas->isInside(event)) {

                auto last = getLast();
                auto target = rasterLayer->traceBack(event->getCanvasPosition());

                switch(lockAxis) {

                    case LockX:
                        target.setY(last.getY());
                        break;
                    case LockY:
                        target.setX(last.getX());
                        break;
                    default: break;

                }

                if(last == target)
                    return;

                float interpolation;
                auto step = brushOffset / distance(last, target);


                for(interpolation = step; interpolation <= 1.0f; interpolation += step) {

                    //eoVector2 displace = eoVector2(rand()%50 - 25, rand()%50 - 25);
                    addVector(mix(last, target, interpolation) /*+ displace*/);
                }


                updateDynamicBuffer(brushBuffer);

            }

        }

    });

    rasterLayer->onAlterLayer([this, rasterLayer] (GLuint previousTexture) {

        // Draw the brush into the aid buffer, for better blending with itself.
        aidBuffer->begin();

        // Clear the framebuffer.
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // The blend equation is max, so it doesn't accumulate the alpha channel.
        eoBlending::apply(eoBlending::MAX);

        brushProgram->use();
        brushProgram->setTransform(eoMatrix());
        brushProgram->setVec2("canvasMiddle", rasterLayer->getDimension() / 2.0f);
        brushProgram->setFloat("brushSize", brushSize);
        brushProgram->setFloat("hardness", brushHardness);
        brushProgram->setTexture("backTexture", 0);
        brushProgram->setTexture("selection", 1);
        
        // Pass though back texture for advanced blending and effects.
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, previousTexture);

        canvas->getSelectionBuffer()->bind(GL_TEXTURE1);

        // We need black for proper erasing.
        if(eraseMode)
            brushProgram->setColor("targetColor", eoColor::BLACK);
        else
            brushProgram->setColor("targetColor", brushColor);

        brushBuffer->render();

        aidBuffer->end();

        // Reverse subtract erases the alpha from the texture.
        if(eraseMode)
            eoBlending::apply(eoBlending::ERASE);
        else
            eoBlending::apply(eoBlending::PREMUL);

        eoGLProgram::TRIVIAL->use();
        eoGLProgram::TRIVIAL->setTransform(eoMatrix());
        aidBuffer->bind();
        eoVertexBuffer::FULL_DIMENSION->render();

        // Set everything back to normal.
        eoRender::setFillColor();
        eoBlending::apply(eoBlending::PREMUL);
        //glBlendEquation(GL_FUNC_ADD);
        //glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

    });
    
    keyPressID = canvas->getInput()->onKeyPress([this] (int key) {

        switch(key) {
            case Qt::Key_X:
                lockAxis = LockX;
                break;
            case Qt::Key_Y:
                lockAxis = LockY;
                break;
            case Qt::Key_Shift:
                lockAxis = LockNone;
                break;
            case Qt::Key_E:
                eraseMode = !eraseMode;

                if(eraseMode)
                    canvas->getMainWindow()->getStatusBar()->setModeLabel("BRUSH (ERASE)", "#f27672", "#31363b");
                else
                    canvas->getMainWindow()->getStatusBar()->setModeLabel("BRUSH", "#f27672", "#31363b");

                break;
            case Qt::Key_Space:
                resizeMode = ResPREP;
                canvas->getInput()->disableAutoRepeat();
                break;

            default: break;
        }
    });

    keyReleaseID = canvas->getInput()->onKeyRelease([this] (int key) {

        switch(key) {
            case Qt::Key_Space:
                resizeMode = ResFINISH;
                canvas->getInput()->enableAutoRepeat();
                break;
        }

    });

    return ResultSuccess;
}

void eoToolBrush::onLose() {

    if(!canvas)
        return;

    clearQueue();

    canvas->getInput()->removeEvent(eoButtonPress, buttonPressID);
    canvas->getInput()->removeEvent(eoButtonRelease, buttonReleaseID);
    canvas->getInput()->removeEvent(eoMotion, buttonMotionID);
    canvas->getInput()->removeEvent(eoKeyPress, keyPressID);
    canvas->getInput()->removeEvent(eoKeyRelease, keyReleaseID);

    delete aidBuffer;
    aidBuffer = nullptr;
}

void eoToolBrush::onTick() {

    if(resizeMode == ResONGOING && brushResizeProgram) {

        glEnable(GL_BLEND);

        brushSize = distance(begPosition, endPosition);

        brushResizeProgram->use();
        brushResizeProgram->setTransform(eoMatrix().translate(begPosition).scale(brushSize));
        brushResizeProgram->setFloat("radius", brushSize);
        brushResizeProgram->setInt("variant", 1);
        eoVertexBuffer::UNIT_CIRCLE->render();

        glDisable(GL_BLEND);

        brushSize /= canvas->getScaleFactor();
        properties->onFloatSliderChange(0);
    } else {
    
        glEnable(GL_BLEND);

        brushResizeProgram->use();
        brushResizeProgram->setTransform(eoMatrix().translate(begPosition).scale(brushSize * canvas->getScaleFactor()));
        brushResizeProgram->setFloat("radius", brushSize);
        brushResizeProgram->setInt("variant", 0);
        eoVertexBuffer::UNIT_CIRCLE->render();

        glDisable(GL_BLEND);
    }
}

void eoToolBrush::setBrushSize(float size) {
    
    brushSize = size;
}

void eoToolBrush::setBrushColor(eoColor color) {
    
    brushColor = color;
}

void eoToolBrush::setBrushOffset(float offset) {
    
    brushOffset = offset;
}

void eoToolBrush::setBrushHardness(float hardness) {
    
    brushHardness = hardness;
}

void eoToolBrush::onCanvasChange(eoCanvas *freshCanvas) {

    eoTool::onCanvasChange(freshCanvas);
    
    delete brushResizeProgram;

    auto fragment = new eoShaderFile(GL_FRAGMENT_SHADER, "data/glsl/tools/fragment_brush_size.glsl");

    brushResizeProgram = new eoGLProgram();
    brushResizeProgram->attachShader(fragment);
    brushResizeProgram->attachShader(eoShader::VERTEX_SCREENDIM);
    brushResizeProgram->link();

    delete fragment;
}

QWidget *eoToolBrush::getPropertiesWidget() {
    
    return properties;
}

eoTool::SelectionLabel eoToolBrush::getLabel() {
    
    return (SelectionLabel){"BRUSH", "#f27672", "#31363b"};
}
