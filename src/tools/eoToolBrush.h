//
// Created by ego on 12/9/17.
//

#ifndef GFX_PROTOTYPE_EOTOOLBRUSH_H
#define GFX_PROTOTYPE_EOTOOLBRUSH_H

#include "eoTool.h"
#include "../eoGLProgram.h"
#include "../eoVertexBuffer.h"
#include "../eoVertexBufferDynamic.h"
#include "../eoVectorQueue.h"
#include "../eoColor.h"
#include "../eoPropertiesBuilder.h"
#include "../eoFrameBuffer.h"

class eoToolBrush : public eoTool, eoVectorQueue {

    eoPropertiesBuilder *properties;

    int buttonPressID;
    int buttonReleaseID;
    int buttonMotionID;
    int keyPressID;
    int keyReleaseID;

    eoGLProgram *brushProgram;
    eoGLProgram *brushResizeProgram = nullptr;
    eoVertexBufferDynamic *brushBuffer;
    eoFrameBuffer *aidBuffer = nullptr;

    float brushSize = 20.0f;
    eoColor brushColor = eoColor(0.0, 0.0, 0.0, 1.0);
    float brushOffset = 5.0f;
    float brushHardness = 10.0f;

    LockAxis lockAxis = LockNone;
    bool eraseMode = false;
    
    enum Resizing {
        ResNONE,
        ResPREP,
        ResONGOING,
        ResFINISH
    };

    Resizing resizeMode = ResNONE;
    eoVector2 begPosition, endPosition;

public:
    explicit eoToolBrush(eoRender* render);
    virtual ~eoToolBrush();

    void onTick() override;
    CastingResult onCast() override;
    void onLose() override;
    void onCanvasChange(eoCanvas *freshCanvas) override;
    QWidget *getPropertiesWidget() override;
    SelectionLabel getLabel() override;

    void setBrushSize(float size);
    void setBrushColor(eoColor color);
    void setBrushOffset(float offset);
    void setBrushHardness(float hardness);
};


#endif //GFX_PROTOTYPE_EOTOOLBRUSH_H
