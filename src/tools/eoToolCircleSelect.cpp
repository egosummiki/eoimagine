//
// Created by ego on 08/10/18.
//
#include "eoToolCircleSelect.h"
#include "../eoFrameBuffer.h"
#include "../eoGLProgram.h"
#include "../eoVertexBuffer.h"
#include "../shaders/eoShaderCanvasDimension.h"
#include "../shaders/eoShaderFile.h"
#include "../eoInput.h"

eoToolCircleSelect::eoToolCircleSelect(eoRender* render)
    : eoToolRectSelect(render)
{
}

void eoToolCircleSelect::drawSelection() {
    
    canvas->getSelectionBuffer()->begin(); {


        auto bottomLeft = min(begPosition, endPosition);
        auto topRight = max(begPosition, endPosition);

        auto scaleFactor = eoVector2(
                (topRight.getX() - bottomLeft.getX()) / 2.0f, 
                (topRight.getY() - bottomLeft.getY()) / 2.0f);


        auto position = eoVector2(
                bottomLeft.getX() + scaleFactor.getX(),
                bottomLeft.getY() + scaleFactor.getY());

        if(selection == SelectNormal) {

            glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
            glClear(GL_COLOR_BUFFER_BIT);
        }           

        program->use();
        program->setTransform(eoMatrix().translate(position).scale(scaleFactor));

        if(selection == SelectSubtract)
            program->setFloat("strength", 0.0f);
        else
            program->setFloat("strength", 1.0f);
        
        eoVertexBuffer::UNIT_CIRCLE->render();
    
    } canvas->getSelectionBuffer()->end();
}

eoTool::SelectionLabel eoToolCircleSelect::getLabel() {
    
    return (SelectionLabel){"CIRCLE SELECT", "#400040", "#ffffff"};
}
