#pragma once
//
// Created by ego on 08/10/18.
//

#include "eoToolRectSelect.h"
#include "../eoRender.h"
#include "../eoGLProgram.h"
#include "../eoVector2.h"

class eoToolCircleSelect : public eoToolRectSelect {


public:

    eoToolCircleSelect(eoRender* render);
    void drawSelection() override;
    SelectionLabel getLabel() override;
};
