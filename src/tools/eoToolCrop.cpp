//
// Created by ego on 25/11/18.
//
#include "eoToolCrop.h"

eoToolCrop::eoToolCrop(eoRender* render)
    : eoTool(render)
{
}

eoTool::CastingResult eoToolCrop::onCast() {
    
    return ResultSuccess;
}

void eoToolCrop::onLose() {

}

eoTool::SelectionLabel eoToolCrop::getLabel() {
    
    return (eoTool::SelectionLabel) {"NORMAL", "#24cd6a", "#31363b"};
}
