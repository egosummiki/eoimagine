#pragma once
//
// Created by ego on 25/11/18.
//

#include "eoTool.h"
#include "../eoRender.h"

class eoToolCrop : public eoTool {

public:

    eoToolCrop(eoRender* render);
    CastingResult onCast() override;
    void onLose() override;
    SelectionLabel getLabel() override;
};
