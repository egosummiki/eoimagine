//
// Created by ego on 29/08/18.
//

#include "eoToolNormal.h"
#include "../ui/eoWindowMain.h"

eoToolNormal::eoToolNormal(eoRender* render)
    : eoTool(render)
{
}

eoTool::CastingResult eoToolNormal::onCast() {
    
    render->getMainWindow()->getStatusBar()->setModeLabel("NORMAL", "#24cd6a", "#31363b");

    return ResultSuccess;
}
