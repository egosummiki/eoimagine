#pragma once
//
// Created by ego on 29/08/18.
//
#include "eoTool.h"

class eoToolNormal : public eoTool {

public:
    eoToolNormal(eoRender* render);
    
    CastingResult onCast();
};
