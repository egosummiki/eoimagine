//
// Created by ego on 07/10/18.
//
#include "eoToolRectSelect.h"
#include "../eoFrameBuffer.h"
#include "../eoGLProgram.h"
#include "../eoVertexBuffer.h"
#include "../shaders/eoShaderCanvasDimension.h"
#include "../shaders/eoShaderFile.h"
#include "../eoInput.h"

eoToolRectSelect::eoToolRectSelect(eoRender* render)
    :   eoTool(render)
{
}

eoTool::CastingResult eoToolRectSelect::onCast() {
    
    render->getMainWindow()->getStatusBar()->setModeLabel("RECT SELECT", "#000000", "#ffffff"); 

    if(!canvas)
        return ResultFail;

    buttonPressID = canvas->getInput()->onButtonPress([this] (eoMouseEvent *event) {

        if(event->button() != Qt::LeftButton)
            return;

        begPosition = event->getCanvasPosition();
    });

    motionID = canvas->getInput()->onMotion([this] (eoMouseEvent *event) {

        if(!canvas->getInput()->isLeftButtonHeld())
            return;

        endPosition = event->getCanvasPosition();
        this->drawSelection();

    });

    buttonReleaseID = canvas->getInput()->onButtonRelease([this] (eoMouseEvent *event) {

        if(event->button() != Qt::LeftButton)
            return;

        endPosition = event->getCanvasPosition();
        this->drawSelection();
    });

    keyPressID = canvas->getInput()->onKeyPress([this] (int key) {

        switch(key) {
            
            case Qt::Key_Control:
                selection = SelectSubtract;
            break;

            case Qt::Key_Shift:
                selection = SelectAdd;
            break;
        }
    });

    keyReleaseID = canvas->getInput()->onKeyRelease([this] (int key) {

        switch(key) {
            
            case Qt::Key_Control:
            case Qt::Key_Shift:
                selection = SelectNormal;
            break;
        }
    });

    return ResultSuccess;
}

void eoToolRectSelect::onLose() {

    auto canvas = render->getCanvas();

    if(!canvas)
        return;

    canvas->getInput()->removeEvent(eoButtonPress, buttonPressID);
    canvas->getInput()->removeEvent(eoMotion, motionID);
    canvas->getInput()->removeEvent(eoButtonRelease, buttonReleaseID);
    canvas->getInput()->removeEvent(eoKeyPress, keyPressID);
    canvas->getInput()->removeEvent(eoKeyRelease, keyReleaseID);

}

void eoToolRectSelect::onTick() {
    
}

void eoToolRectSelect::onCanvasChange(eoCanvas *freshCanvas) {
    
    eoTool::onCanvasChange(freshCanvas);

    delete program;

    if(!canvas) {

        program = nullptr;
        return;
    }

    auto vertex = new eoShaderCanvasDimension(canvas);
    auto fragment = new eoShaderFile(GL_FRAGMENT_SHADER, "data/glsl/fragment_white.glsl");

    program = new eoGLProgram();
    program->attachShader(fragment);
    program->attachShader(vertex);
    program->link();

    delete vertex;
    delete fragment;
}

void eoToolRectSelect::drawSelection() {

    canvas->getSelectionBuffer()->begin(); {


        auto bottomLeft = min(begPosition, endPosition);
        auto topRight = max(begPosition, endPosition);

        auto scaleFactor = eoVector2(
                (topRight.getX() - bottomLeft.getX()) / 2.0f, 
                (topRight.getY() - bottomLeft.getY()) / 2.0f);


        auto position = eoVector2(
                bottomLeft.getX() + scaleFactor.getX(),
                bottomLeft.getY() + scaleFactor.getY());

        if(selection == SelectNormal) {

            glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
            glClear(GL_COLOR_BUFFER_BIT);
        }           

        program->use();
        program->setTransform(eoMatrix().translate(position).scale(scaleFactor));

        if(selection == SelectSubtract)
            program->setFloat("strength", 0.0f);
        else
            program->setFloat("strength", 1.0f);
        
        eoVertexBuffer::FULL_DIMENSION->render();
    
    } canvas->getSelectionBuffer()->end();
}

eoTool::SelectionLabel eoToolRectSelect::getLabel() {
    
    return (SelectionLabel){"RECT SELECT", "#000000", "#ffffff"};
}
