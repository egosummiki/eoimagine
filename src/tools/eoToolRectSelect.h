#pragma once
//
// Created by ego on 07/10/18.
//

#include "eoTool.h"
#include "../eoRender.h"
#include "../eoGLProgram.h"
#include "../eoVector2.h"

class eoToolRectSelect : public eoTool {



    int buttonPressID;
    int buttonReleaseID;
    int motionID;

    int keyPressID;
    int keyReleaseID;

protected:

    eoGLProgram *program = nullptr;

    eoVector2 begPosition, endPosition;

    SelectionMode selection;

public:

    eoToolRectSelect(eoRender* render);
    CastingResult onCast() override;
    void onLose() override;
    void onTick() override;
    void onCanvasChange(eoCanvas *freshCanvas) override;
    SelectionLabel getLabel() override;

    virtual void drawSelection();

};
