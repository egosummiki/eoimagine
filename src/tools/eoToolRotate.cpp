//
// Created by ego on 27/08/18.
//

#include <cmath>
#include "eoToolRotate.h"
#include "../ui/eoWindowMain.h"
#include "../eoLayer.h"
#include "../actions/eoActionRotate.h"

eoToolRotate::eoToolRotate(eoRender* render)
    : eoTool(render)
{
}

eoTool::CastingResult eoToolRotate::onCast() {
    if(!canvas)
        return ResultFail;

    buttonPressedID = canvas->getInput()->onButtonPress([this] (eoMouseEvent* event) {
        auto layer = canvas->getCurrentLayer();
        auto position = layer->getTranslateModule()->getVector2("position");

        baseRotation = layer->getRotateModule()->getFloat("rotation");
        baseAngle = (event->getCanvasPosition() - position).toAngle() - baseRotation;
    });

    buttonMotionID = canvas->getInput()->onMotion([this] (eoMouseEvent* event) {
        if(canvas->getInput()->isLeftButtonHeld()) {
            auto layer = canvas->getCurrentLayer();
            auto module = layer->getRotateModule();
            auto position = layer->getTranslateModule()->getVector2("position");

            module->setArgument("rotation", (event->getCanvasPosition() - position).toAngle() - baseAngle);

            canvas->getCurrentLayer()->updateTransform();
        }
    });

    buttonReleasedID = canvas->getInput()->onButtonRelease([this] (eoMouseEvent* event) {
        auto layer = canvas->getCurrentLayer();
        auto position = layer->getTranslateModule()->getVector2("position");
        auto rotation = (event->getCanvasPosition() - position).toAngle() - baseAngle;

        canvas->getActionManager()->perform(new eoActionRotate(canvas, rotation, baseRotation));
    });

    return ResultSuccess;
}

void eoToolRotate::onLose() {
    if(!canvas)
        return;

    canvas->getInput()->removeEvent(eoMotion, buttonMotionID);
    canvas->getInput()->removeEvent(eoButtonPress, buttonPressedID);
    canvas->getInput()->removeEvent(eoButtonRelease, buttonReleasedID);
}

eoTool::SelectionLabel eoToolRotate::getLabel() {
    return (SelectionLabel){"ROTATE", "#60FF60", "#31363b"};
}
