#pragma once
//
// Created by ego on 27/08/18.
//

#include "eoTool.h"

class eoToolRotate : public eoTool {

    int buttonMotionID;
    int buttonReleasedID;
    int buttonPressedID;

    float baseAngle, baseRotation;

public:

    eoToolRotate(eoRender* render);
    CastingResult onCast() override;
    void onLose() override;
    SelectionLabel getLabel() override;

};
