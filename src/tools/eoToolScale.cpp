//
// Created by ego on 29/09/18.
//
#include "eoToolScale.h"
#include "../ui/eoWindowMain.h"
#include "../eoLayer.h"
#include "../actions/eoActionScale.h"

eoToolScale::eoToolScale(eoRender* render)
    : eoTool(render)
{
}

eoTool::CastingResult eoToolScale::onCast() {

    if(!canvas)
        return ResultFail;
    

    buttonPressedID = canvas->getInput()->onButtonPress([this] (eoMouseEvent* event) {

            auto layer = canvas->getCurrentLayer();
            auto position = layer->getTranslateModule()->getVector2("position");
            baseDistance = distance(position, event->getCanvasPosition());
            baseScale = layer->getScaleModule()->getVector2("scale", eoVector2(1.0f, 1.0f));

            printf("Base scale is %f x %f.\n", baseScale.getX(), baseScale.getY());
    });

    buttonMotionID = canvas->getInput()->onMotion([this] (eoMouseEvent* event) {

        if(canvas->getInput()->isLeftButtonHeld()) {

            auto layer = canvas->getCurrentLayer();
            auto module = layer->getScaleModule();
            auto position = layer->getTranslateModule()->getVector2("position");
            float scaleValue = distance(position, event->getCanvasPosition()) / baseDistance;
            eoVector2 scaleAmount(1.0f, 1.0f);

            switch(lockAxis) {
                case LockNone:
                   scaleAmount = eoVector2(
                                    abs(position.getX() - event->getCanvasPosition().getX())/baseDistance,
                                    abs(position.getY() - event->getCanvasPosition().getY())/baseDistance
                                   ); 
                break;
                case LockKeepRatio:
                    scaleAmount = eoVector2(scaleValue, scaleValue);
                break;
            }

            module->setArgument("scale", baseScale * scaleAmount);
            canvas->getCurrentLayer()->updateTransform();
        }
    });

    buttonReleasedID = canvas->getInput()->onButtonRelease([this] (eoMouseEvent* event) {

            auto layer = canvas->getCurrentLayer();
            auto module = layer->getScaleModule();
            auto position = layer->getTranslateModule()->getVector2("position");
            float scaleValue = distance(position, event->getCanvasPosition()) / baseDistance;

        canvas->getActionManager()->perform(
                    new eoActionScale(canvas, eoVector2(baseScale * eoVector2(scaleValue, scaleValue)), baseScale)); 
    });

    return ResultSuccess;
}

void eoToolScale::onLose() {

    auto canvas = render->getCanvas();

    if(!canvas)
        return;

    canvas->getInput()->removeEvent(eoMotion, buttonMotionID);
    canvas->getInput()->removeEvent(eoButtonPress, buttonPressedID);
    canvas->getInput()->removeEvent(eoButtonRelease, buttonReleasedID);
}

eoTool::SelectionLabel eoToolScale::getLabel() {
    
    return (SelectionLabel){"SCALE", "#ffffff", "#000000"};
}
