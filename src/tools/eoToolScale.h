#pragma once
//
// Created by ego on 29/09/18.
//

#include "eoTool.h"

class eoToolScale : public eoTool {

    int buttonMotionID;
    int buttonReleasedID;
    int buttonPressedID;

    LockAxis lockAxis = LockKeepRatio;

    float baseDistance;
    eoVector2 baseScale = eoVector2(1.0f, 1.0f);
public:

    eoToolScale(eoRender* render);
    CastingResult onCast() override;
    void onLose() override;
    SelectionLabel getLabel() override;

};
