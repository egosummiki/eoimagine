//
// Created by ego on 12/10/17.
//

#include "eoToolStamp.h"
#include "../shaders/eoShaderFile.h"
#include "../shaders/eoShaderCanvasDimension.h"
#include "../eoLayerRaster.h"
#include "../ui/eoWindowMain.h"


eoToolStamp::eoToolStamp(eoRender* render) : eoTool(render), offset(0.0f, 0.0f)
{

    /*
    auto fragment   = new eoShaderFile(GL_FRAGMENT_SHADER, "data/glsl/tools/stamp_fragment.glsl");
    auto vertex     = new eoShaderCanvasDimension(canvas);

    program = new eoGLProgram();
    program->attachShader(fragment);
    program->attachShader(vertex);
    program->link();

    delete fragment;
    delete vertex;

    vertexBuffer = eoVertexBuffer::canvas(canvas);
*/
}

void eoToolStamp::onTick() {
}

eoTool::CastingResult eoToolStamp::onCast() {

    return ResultFail;

    //canvas->getMainWindow()->getStatusBar()->setModeLabel("STAMP", "#d2d2d2", "#31363b");
//
    //if(canvas->getCurrentLayer()->hasAttribute(eoLayer::RASTER))
    //{
        //auto rasterLayer = dynamic_cast<eoLayerRaster*>(canvas->getCurrentLayer());
//
        //buttonPressID = canvas->getInput()->onButtonPress([this, rasterLayer] (eoMouseEvent* event) {
//
            //if(canvas->isInside(event))
            //{
                //if(canvas->getInput()->isAltPressed())
                //{
                    //initOffset = true;
                    //offset = event->getCanvasPosition();
                //} else
                //{
                    //if(initOffset)
                    //{
                        //initOffset = false;
                        //offset -= event->getCanvasPosition();
                    //}
//
                    //addVector(event->getCanvasPosition());
                    //rasterLayer->update();
                //}
//
            //}
//
        //});
//
        //buttonMotionID = canvas->getInput()->onMotion([this, rasterLayer] (eoMouseEvent* event) {
//
            //if(canvas->isInside(event))
            //{
                //if(canvas->getInput()->isLeftButtonHeld()) {
                    //addVector(event->getCanvasPosition());
                    //rasterLayer->update();
                //}
            //}
//
        //});
//
        //rasterLayer->onAlterLayer([this, rasterLayer] (GLuint previousTexture) {
            //program->use();
            //glBindTexture(GL_TEXTURE_2D, previousTexture);
//
            //program->setTransform(eoMatrix());
//
            //program->setVec2  ("pointer_pos",    rasterLayer->toLayerSpace(popQueue()).toUV(rasterLayer->getWidth(), rasterLayer->getHeight()));
            //program->setVec2  ("stamp_offset",   offset / eoVector2(rasterLayer->getWidth(), rasterLayer->getHeight()));
            //program->setVec2  ("canvas_size",    canvas->getWidth(), canvas->getHeight());
            //program->setFloat ("brush_size",     canvas->getCurrentBrushSize() / canvas->getWidth());
            //program->setFloat ("brush_hardness", canvas->getCurrentBrushHardness());
//
            //vertexBuffer->render();
        //});
    //}
}

void eoToolStamp::onLose() {
    //eoLayer* currentLayer = canvas->getCurrentLayer();
//
    //if(currentLayer->hasAttribute(eoLayer::RASTER))
    //{
        //clearQueue();
//
        //canvas->getInput()->removeEvent(eoButtonPress, buttonPressID);
        //canvas->getInput()->removeEvent(eoMotion, buttonMotionID);
//
        //auto rasterLayer = (eoLayerRaster*) currentLayer;
        //rasterLayer->clearAlterLayer();
    //}
}
