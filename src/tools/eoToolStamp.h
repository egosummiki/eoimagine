//
// Created by ego on 12/10/17.
//

#ifndef GFX_PROTOTYPE_EOTOOLSTAMP_H
#define GFX_PROTOTYPE_EOTOOLSTAMP_H

#include "eoTool.h"
#include "../eoGLProgram.h"
#include "../eoVertexBuffer.h"
#include "../eoVertexBufferDynamic.h"
#include "../eoVectorQueue.h"



class eoToolStamp : public eoTool, eoVectorQueue {

    int buttonPressID;
    int buttonMotionID;

    eoGLProgram* program;
    eoVertexBuffer* vertexBuffer;

//    float offsetX = 0.0f;
//    float offsetY = 0.0f;
    eoVector2 offset;
    bool initOffset = false;
public:
    explicit eoToolStamp(eoRender* render);

    void onTick() override;
    CastingResult onCast() override;
    void onLose() override;
};


#endif //GFX_PROTOTYPE_EOTOOLSTAMP_H
