//
// Created by ego on 27/10/18.
//
#include "eoToolText.h"
#include "../eoLayerText.h"
#include <string>
#include <QComboBox>

eoToolText::eoToolText(eoRender* render)
    : eoTool(render), properties(new eoPropertiesBuilder())
{
    fontManager = render->getFontManager();

    fontCombo = properties->addCombo("Font Family", [this] (int value) {

        fontFamilyID = value;

        while(styleCombo->count() > 0)
            styleCombo->removeItem(0);

        auto familyStyles = fontManager->getFamilyStyles(value);

        for(auto &style :familyStyles) {

            styleCombo->addItem(style);
        }

        if(!canvas)
            return;

        auto layer = canvas->getCurrentLayer();

        if(!layer->hasAttribute(eoLayer::TEXT))
            return;

        auto layerText = dynamic_cast<eoLayerText*>(layer);

        canvas->performOnRender([this, layerText] (eoCanvas *canvas) {
            layerText->setFont(fontManager->getFont(fontFamilyID, 0, (int)fontSize));
        });

        canvas->requestRedraw();

    });

    styleCombo = properties->addCombo("Style", [this] (int value) {

        fontStyleID = value;
            
        if(!canvas)
            return;

        auto layer = canvas->getCurrentLayer();

        if(!layer->hasAttribute(eoLayer::TEXT))
            return;

        auto layerText = dynamic_cast<eoLayerText*>(layer);

        canvas->performOnRender([this, layerText, value] (eoCanvas *canvas) {
            layerText->setFont(fontManager->getFont(fontFamilyID, value, (int)fontSize));
        });

        canvas->requestRedraw();

    });


    for(int i = 0; i < fontManager->getFontAmount(); ++i) {

        eoFontManager::FontInfo info = fontManager->getFontInfo(i);

        if(info.family != std::nullopt) {

            fontCombo->addItem(info.family.value());
        }
    }

    properties->addColorSelect("Color", &color, [this] (eoColor color) {


        if(!canvas)
            return;

        auto layer = canvas->getCurrentLayer();

        if(!layer->hasAttribute(eoLayer::TEXT))
            return;

        auto layerText = dynamic_cast<eoLayerText*>(layer);

        layerText->setColor(color);

        canvas->requestRedraw();
    });

    properties->addFloatSlider("Size", 5, 360, &fontSize, [this] (float value) {

        if(!canvas)
            return;

        auto layer = canvas->getCurrentLayer();

        if(!layer->hasAttribute(eoLayer::TEXT))
            return;

        auto layerText = dynamic_cast<eoLayerText*>(layer);

        canvas->performOnRender([this, layerText, value] (eoCanvas *canvas) {

            layerText->setFont(fontManager->getFont(fontFamilyID, fontStyleID, value));
        });

        canvas->requestRedraw();
    });

    auto updateSep = [this] (float value) {
        
        if(!canvas)
            return;

        auto layer = canvas->getCurrentLayer();

        if(!layer->hasAttribute(eoLayer::TEXT))
            return;

        auto layerText = dynamic_cast<eoLayerText*>(layer);
        layerText->setSeparation(sepChar, sepSpace, sepLine);

        canvas->requestRedraw();
    };

    properties->addFloatSlider("Character seperation", 0, 50, &sepChar, updateSep);
    properties->addFloatSlider("Space seperation", 0, 50, &sepSpace, updateSep);
    properties->addFloatSlider("Line seperation", 0, 50, &sepLine, updateSep);

}

eoTool::CastingResult eoToolText::onCast() {
    
    if(!canvas)
        return ResultFail;

    auto layer = canvas->getCurrentLayer();

    if(!layer->hasAttribute(eoLayer::TEXT)) {
        
        canvas->performOnRender([this] (eoCanvas *canvas) {
            auto layerText = new eoLayerText(canvas, "Text Layer", fontManager->getFont("Arial", "Regular", 35));
            canvas->addLayer(layerText);
        });

        return ResultFail;

    }

    auto layerText = dynamic_cast<eoLayerText*>(layer);

    textInputID = canvas->getInput()->onText([this, layerText] (QString text) {

        if(text.isEmpty())
            return;

        switch(text.at(0).unicode()) {
            case 8:
                layerText->popChar();
                break;
            case '\r':
                layerText->appendText("\n");
                break;
            default: 
                layerText->appendText(text.toStdString());
        }
    });

    return ResultSuccess;
}

void eoToolText::onLose() {

    if(!canvas)
        return;

    canvas->getInput()->removeEvent(eoText, textInputID);
}

eoTool::SelectionLabel eoToolText::getLabel() {
    
    return (eoTool::SelectionLabel) {"TEXT", "#24cd6a", "#31363b"};
}

QWidget *eoToolText::getPropertiesWidget() {
    
    return properties;
}
