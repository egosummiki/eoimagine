#pragma once
//
// Created by ego on 27/10/18.
//

#include "eoTool.h"
#include "../eoRender.h"
#include "../eoPropertiesBuilder.h"
#include "../eoFontManager.h"

class eoToolText : public eoTool {

    eoPropertiesBuilder *properties;
    int textInputID;
    unsigned fontFamilyID, fontStyleID = 0;
    float sepChar = 0, sepSpace = 0, sepLine = 0;
    float fontSize = 40;
    QComboBox *fontCombo, *styleCombo;
    eoFontManager *fontManager;
    eoColor color = eoColor::BLACK;

public:

    eoToolText(eoRender* render);
    CastingResult onCast() override;
    void onLose() override;
    SelectionLabel getLabel() override;
    QWidget *getPropertiesWidget() override;

};
