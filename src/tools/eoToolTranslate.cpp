//
// Created by ego on 8/23/18.
//

#include "eoToolTranslate.h"
#include "../ui/eoWindowMain.h"
#include "../eoLayer.h"
#include "../actions/eoActionTranslate.h"

eoToolTranslate::eoToolTranslate(eoRender* render)
    : eoTool(render)
{
}

eoTool::CastingResult eoToolTranslate::onCast() {
    if(!canvas)
        return ResultFail;

    auto input = canvas->getInput();

    buttonPressedID = input->onButtonPress([this] (eoMouseEvent* event) {
        savedTranslation = canvas->getCurrentLayer()->getTranslateModule()->getVector2("position");
    });

    buttonMotionID = input->onMotion([this, input] (eoMouseEvent* event) {
        auto layer = canvas->getCurrentLayer();

        if(input->isLeftButtonHeld()) {
            eoVector2 position = layer->getTranslateModule()->getVector2("position");
            eoVector2 diff = event->getCanvasPosition() - input->getPreviousMousePosition();

            switch(lockAxis) {
                case LockX:
                    diff.setY(0.0f);
                    break;
                case LockY:
                    diff.setX(0.0f);
                    break;
                default: break;

            }

            layer->getTranslateModule()->setArgument("position", position + diff);
            layer->updateTransform();
        }

    });

    buttonReleasedID = input->onButtonRelease([this, input] (eoMouseEvent* event) {
        eoVector2 position =
            canvas->getCurrentLayer()->getTranslateModule()->getVector2("position")
            + event->getCanvasPosition()
            - input->getPreviousMousePosition();

        canvas->getActionManager()->perform(new eoActionTranslate(canvas, position, savedTranslation));
    });

    keyPressID = canvas->getInput()->onKeyPress([this] (int key) {
        switch(key) {
            case Qt::Key_X:
                lockAxis = LockX;
                break;
            case Qt::Key_Y:
                lockAxis = LockY;
                break;
            case Qt::Key_Shift:
                lockAxis = LockNone;
                break;

            default: break;
        }
    });
    

    return ResultSuccess;
}


void eoToolTranslate::onLose() {
    if(!canvas)
        return;

    auto input = canvas->getInput();

    input->removeEvent(eoMotion, buttonMotionID);
    input->removeEvent(eoButtonRelease, buttonReleasedID);
    input->removeEvent(eoButtonPress, buttonPressedID);
    input->removeEvent(eoKeyPress, keyPressID);
}


eoTool::SelectionLabel eoToolTranslate::getLabel() {
    return (SelectionLabel){"GRAB", "#A05050", "#31363b"};
}
