//
// Created by ego on 8/23/18.
//

#ifndef EOIMAGINE_EOTOOLTRANSLATE_H
#define EOIMAGINE_EOTOOLTRANSLATE_H

#include "eoTool.h"

class eoToolTranslate : public eoTool {

    int buttonMotionID;
    int buttonReleasedID;
    int buttonPressedID;
    int keyPressID;

    eoVector2 savedTranslation = eoVector2(.0f, .0f);

    LockAxis lockAxis = LockNone;

public:

    eoToolTranslate(eoRender* render);
    CastingResult onCast() override;
    void onLose() override;
    SelectionLabel getLabel() override;

};


#endif //EOIMAGINE_EOTOOLTRANSLATE_H
