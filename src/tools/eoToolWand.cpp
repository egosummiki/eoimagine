//
// Created by ego on 12/10/18.
//
#include "eoToolWand.h"
#include "../eoLayerRaster.h"
#include "../eoColor.h"
#include "../eoTexture.h"
#include "../eoCanvas.h"
#include "../eoInput.h"
#include "../shaders/eoShaderFile.h"
#include <queue>
#include <cstring>
#include <cmath>

eoToolWand::eoToolWand(eoRender* render)
    : eoTool(render), properties(new eoPropertiesBuilder())
{
    const static QString propagationTypes[] = { QString("Absolute"), QString("Relative") };
    const static QString selectionTypes[] = { QString("Soft"), QString("Hard") };
    const static QString similatiryTypes[] = { 
        QString("RGB distance"),
        QString("HSV distance"),
        QString("HSL distance"),
        QString("Red channel"),
        QString("Green channel"),
        QString("Blue channel"),
        QString("Hue"),
        QString("Saturation"),
        QString("Value"),
        QString("Lightness")
    };


    properties->addFloatSlider("Tolerance", 0, 100, &tolerance);
    properties->addCombo("Propagation", propagationTypes, 2, &propagation);
    properties->addCombo("Selection", selectionTypes, 2, &hardSelection);
    properties->addCombo("Similarity function", similatiryTypes, 10, &simFunc);

    auto fragment = new eoShaderFile(GL_FRAGMENT_SHADER, "data/glsl/fragment_negative.glsl");

    program = new eoGLProgram();
    program->attachShader(fragment);
    program->attachShader(eoShader::VERTEX_TRIVIAL);
    program->link();
}

eoTool::CastingResult eoToolWand::onCast() {
    
    if(!canvas)
        return ResultFail;
    

    buttonPressID = canvas->getInput()->onButtonPress([this] (eoMouseEvent *event) {
        
        /* 
            Perform selection on left key pressed. 
        */


        int x, y;

        if(event->button() != Qt::LeftButton)
            return;

        if(canvas->isInside(event)) {

            x = event->getCanvasUVX();
            y = event->getCanvasUVY();

            canvas->performOnRender([this, x, y] (eoCanvas *canvas) {

                performSelection(x, y);
            });
        }

    });

    keyPressID = canvas->getInput()->onKeyPress([this] (int key) {

        /* 
            Handling add/subtract selection.
        */

        switch(key) {
            
            case Qt::Key_Control:
                selection = SelectSubtract;
            break;

            case Qt::Key_Shift:
                selection = SelectAdd;
            break;
        }
    });

    keyReleaseID = canvas->getInput()->onKeyRelease([this] (int key) {

        switch(key) {
            
            case Qt::Key_Control:
            case Qt::Key_Shift:
                selection = SelectNormal;
            break;
        }
    });

    return ResultSuccess;
}

void eoToolWand::onLose() {

    if(!canvas)
        return;

    canvas->getInput()->removeEvent(eoButtonPress, buttonPressID);
    canvas->getInput()->removeEvent(eoKeyPress, keyPressID);
    canvas->getInput()->removeEvent(eoKeyRelease, keyReleaseID);
}

struct queuePoint {
    int x, y;
};

float simRGB(eoColor self, eoColor other) {

    float rDiff = self.getR() - other.getR();
    float gDiff = self.getG() - other.getG();
    float bDiff = self.getB() - other.getB();

    return 1.0f - sqrt( rDiff*rDiff + gDiff*gDiff + bDiff*bDiff ) / sqrt(3);
}

float simHSV(eoColor self, eoColor other) {

    auto selfHSV = self.toHSV();
    auto otherHSV = other.toHSV();

    float hDiff = abs(selfHSV.h - otherHSV.h);
    if(hDiff > 6.0f)
        hDiff = 6.0f - hDiff;
    hDiff /= 3.0f;

    float sDiff = selfHSV.s - otherHSV.s;
    float vDiff = selfHSV.v - otherHSV.v;

    return 1.0f - sqrt( hDiff*hDiff + sDiff*sDiff + vDiff*vDiff ) / sqrt(3);
}

float simHSL(eoColor self, eoColor other) {

    auto selfHSL = self.toHSL();
    auto otherHSL = other.toHSL();

    float hDiff = abs(selfHSL.h - otherHSL.h);
    if(hDiff > 6.0f)
        hDiff = 6.0f - hDiff;
    hDiff /= 3.0f;

    float sDiff = selfHSL.s - otherHSL.s;
    float lDiff = selfHSL.l - otherHSL.l;

    return 1.0f - sqrt( hDiff*hDiff + sDiff*sDiff + lDiff*lDiff ) / sqrt(3);
}

float simLight(eoColor self, eoColor other) {

    return 1.0f - abs(self.getL() - other.getL());
}

float simValue(eoColor self, eoColor other) {

    return 1.0f - abs(self.getV() - other.getV());
}

float simHue(eoColor self, eoColor other) {

    float dist = abs(self.getH() - other.getH());

    if(dist > 3.0f)
        dist = 6.0f - dist;

    return 1.0f - dist/3.0f;
}

float simSat(eoColor self, eoColor other) {

    return 1.0f - abs(self.getS() - other.getS());
}

float simRed(eoColor self, eoColor other) {

    return 1.0f - abs(self.getR() - other.getR());
}

float simGreen(eoColor self, eoColor other) {

    return 1.0f - abs(self.getG() - other.getG());
}

float simBlue(eoColor self, eoColor other) {

    return 1.0f - abs(self.getB() - other.getB());
}


void eoToolWand::performSelection(int x, int y) {

    eoLayer *layer;
    eoLayerRaster *rasterLayer;
    uint *layerData;
    uchar *selectionBuffer, *visitedNodes;
    int width, height, bytesPerLine;
    std::queue<queuePoint> nodeQueue;
    eoColor comparingColor(0);
    float acceptance;
    float (*similarity)(eoColor, eoColor);

    // Which similarity function.
    switch(simFunc) {

        case 1:
            similarity = simHSV;
        break;
        case 2:
            similarity = simHSL;
        break;
        case 3:
            similarity = simRed;
        break;
        case 4:
            similarity = simGreen;
        break;
        case 5:
            similarity = simBlue;
        break;
        case 6:
            similarity = simHue;
        break;
        case 7:
            similarity = simSat;
        break;
        case 8:
            similarity = simValue;
        break;
        case 9:
            similarity = simLight;
        break;
        default:
            similarity = simRGB;
        break;
    }
    
    // Relative propagation need to have much higher acceptance.
    if(propagation) {
        acceptance = 1.0f - tolerance/2000.0f;
    } else {
        acceptance = 1.0f - tolerance/100.0f;
    }

    if(!canvas || !(layer = canvas->getCurrentLayer()) || !(layer->hasAttribute(eoLayer::RASTER)))
        return;

    rasterLayer = static_cast<eoLayerRaster*>(layer);
    
    // Get the raw data from the front layer framebuffer.
    auto frameBuffer = canvas->getFrameBuffer();
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    layerData = frameBuffer->getRawData();
    width = frameBuffer->getWidth();
    height = frameBuffer->getHeight();
    bytesPerLine = frameBuffer->getWidth();
    
    // Allocate the memory for the selection buffer.
    selectionBuffer = new uchar[width * height * sizeof(uchar)];
    memset(selectionBuffer, 0x00, width * height * sizeof(uchar));
    
    // Allocate the memory for the "visited nodes" buffer.
    visitedNodes = new uchar[width * height * sizeof(uchar)];
    memset(visitedNodes, 0x00, width * height * sizeof(uchar));
    
    // Add first element to the queue.
    nodeQueue.emplace((queuePoint){x, y});
    visitedNodes[x + y*width] = 1;
    comparingColor = eoColor(layerData[x + y*width]);
    
    // Handle the queue
    while(!nodeQueue.empty()) {

        queuePoint node = nodeQueue.front();
        queuePoint freshNode;
        nodeQueue.pop();

        // Relative propagation
        if(propagation) {
            comparingColor = eoColor(layerData[node.x + node.y*width]);
        }

        //For each surrounding node
        
        for(int value = -1; value <= 1; value += 2) {

            for(int axis = 0; axis < 2; ++axis) {

                freshNode = axis ?
                    (queuePoint) {node.x + value, node.y} :
                    (queuePoint) {node.x, node.y + value};

                int freshPos = freshNode.x + width * freshNode.y;

                if(
                       freshNode.x >= 0 && freshNode.x < width
                    && freshNode.y >= 0 && freshNode.y < height 
                    && !visitedNodes[freshPos]) {

                    // Mark node as visited
                    propagation || ++visitedNodes[freshPos];

                    // Calculate similarity
                    float sim = similarity(comparingColor, eoColor(layerData[freshPos]));

                    if(sim >= acceptance) {
                        
                        // Mark node as visited
                        propagation && ++visitedNodes[freshPos];

                        nodeQueue.emplace(freshNode);

                        if(hardSelection) {
                            selectionBuffer[freshPos] = 0xFF;
                        } else {
                            selectionBuffer[freshPos] = static_cast<uchar>((sim - acceptance)/(1 - acceptance)*255.0f);
                        }
                    }
                }
            }
        }
    }

    // Empty queue - We can use our selection buffer.
    // But first clear up some space.
    
    delete[] layerData;
    delete[] visitedNodes;

    auto selectionTexture = eoTexture(width, height, selectionBuffer, QOpenGLTexture::Red);

    drawSelection(selectionTexture);
    
    delete[] selectionBuffer;
}

void eoToolWand::drawSelection(eoTexture &texture) {

    canvas->getSelectionBuffer()->begin(); {

        if(selection != SelectNormal) {
            glEnable(GL_BLEND);
            
            switch(selection) {
                case SelectAdd:
                    glBlendEquation(GL_MAX);
                    break;
                case SelectSubtract:
                    glBlendEquation(GL_MIN);
                    break;
            }
        }

        if(selection == SelectSubtract) {
            program->use(); 
            program->setTransform(eoMatrix()); 
        } else {
            eoGLProgram::TRIVIAL->use(); 
            eoGLProgram::TRIVIAL->setTransform(eoMatrix()); 
        }

        texture.bind();
        
        eoVertexBuffer::FULL_DIMENSION->render();

        if(selection != SelectNormal) {
            glBlendEquation(GL_FUNC_ADD);
            glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
            glDisable(GL_BLEND);
        }
    
    } canvas->getSelectionBuffer()->end();
}

QWidget *eoToolWand::getPropertiesWidget() {
    
    return properties;
}

eoTool::SelectionLabel eoToolWand::getLabel() {
    
    return (SelectionLabel){"WAND", "#ffffff", "#a010a0"};
}
