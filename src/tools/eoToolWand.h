#pragma once
//
// Created by ego on 12/10/18.
//

#include "eoTool.h"
#include "../eoRender.h"
#include "../eoPropertiesBuilder.h"
#include "../eoGLProgram.h"

class eoToolWand : public eoTool {

    eoPropertiesBuilder *properties;

    int buttonPressID;
    int keyPressID;
    int keyReleaseID;

    float tolerance = 20.0f;
    unsigned propagation = 0;
    unsigned hardSelection = 0;
    unsigned simFunc = 0;

    SelectionMode selection;

    eoGLProgram *program;

public:

    eoToolWand(eoRender* render);
    CastingResult onCast() override;
    void onLose() override;
    SelectionLabel getLabel() override;

    void performSelection(int x, int y);
    void drawSelection(eoTexture &texture);

    QWidget *getPropertiesWidget() override;

};
