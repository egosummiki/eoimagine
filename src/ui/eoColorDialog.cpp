//
// Created by ego on 11/10/18.
//

#include "eoColorDialog.h"
#include "../eoPropertiesBuilder.h"

eoColorDialog::eoColorDialog(eoWindowMain *mainWindow, eoColor* color, std::function<void(void)> closeCallback) 
    :   QWidget(mainWindow, Qt::Window),
        color(color),
        layout(new QHBoxLayout),
        colorWidget(new eoOGLColorWidget(this, false, &hue)),
        hueWidget(new eoOGLColorWidget(this, true, &hue)),
        properties(new eoPropertiesBuilder()),
        closeCallback(std::move(closeCallback))
{
    int width, height;

    auto screenGeometry = QApplication::desktop()->screenGeometry();
    width   = 600;
    height  = 450;

    setWindowTitle("Color Dialog");
    setGeometry(
        screenGeometry.width() / 2 - width / 2,
        screenGeometry.height() / 2 - height / 2,
        width,
        height);

    setFixedSize(width, height);

    setFocus();

    properties->addColorView(&r);

    properties->addSplitter();

    properties->addFloatSlider("Hue", 0, 360, &hue, [this] (float value) {
            
            colorWidget->update();
            calculateRGB();
    });

    properties->addFloatSlider("Saturation", 0, 100, &saturation);
    properties->addFloatSlider("Value", 0, 100, &value);

    properties->addFloatSlider("Red", 0, 255, &r);
    properties->addFloatSlider("Green", 0, 255, &g);
    properties->addFloatSlider("Blue", 0, 255, &b);

    properties->addSplitter();

    properties->addButton("Ok", [this] () {

            *(this->color) = eoColor(r/255.0f, g/255.0f, b/255.f, 1.0f);
            if(this->closeCallback)
                this->closeCallback();
            close();
    });

    properties->setMaximumWidth(200);

    hueWidget->setMaximumWidth(24);

    layout->addWidget(colorWidget);
    layout->addWidget(hueWidget);
    layout->addWidget(properties);

    setLayout(layout);
}

eoOGLColorWidget *eoColorDialog::getColorWidget() {
    
    return colorWidget;
}

eoOGLColorWidget *eoColorDialog::getHueWidget() {
    
    return hueWidget;
}

eoPropertiesBuilder *eoColorDialog::getProperties() {
    
    return properties;
}

void eoColorDialog::setSV(float sat, float val) {
    
    saturation = sat;
    value = val;

    properties->onFloatSliderChange(1);
    properties->onFloatSliderChange(2);
}

void eoColorDialog::calculateRGB() {

    float c, x, m, h, s, v;

    h = hue / 60.0f;
    s = saturation / 100.0f;
    v = value / 100.0f;

    c = v * s;
    x = c * (1.0f - abs(fmod(h, 2.0f) - 1.0f));
    m = v - c;

    if(h < 1.0f) {
        r = 255.0f*(c+m); g = 255.0f*(x+m); b = 255.0f*m;
    } else if(h < 2.0f) {
        r = 255.0f*(x+m); g = 255.0f*(c+m); b = 255.0f*m;
    } else if(h < 3.0f) {
        r = 255.0f*m; g = 255.0f*(c+m); b = 255.0f*(x+m);
    } else if(h < 4.0f) {
        r = 255.0f*m; g = 255.0f*(x+m); b = 255.0f*(c+m);
    } else if(h < 5.0f) {
        r = 255.0f*(x+m); g = 255.0f*m; b = 255.0f*(c+m);
    } else {
        r = 255.0f*(c+m); g = 255.0f*m; b = 255.0f*(x+m);
    }

    properties->onFloatSliderChange(3);
    properties->onFloatSliderChange(4);
    properties->onFloatSliderChange(5);
    properties->updateColorView();
}
