#pragma once
//
// Created by ego on 11/10/18.
//
#include <QWidget>
#include <QHBoxLayout>
#include "eoWindowMain.h"
#include "../eoColor.h"
#include "eoOGLColorWidget.h"

#include <functional>

class eoColorDialog : public QWidget {

Q_OBJECT

private:

    eoColor* color;
    QHBoxLayout *layout;
    eoOGLColorWidget *colorWidget;
    eoOGLColorWidget *hueWidget;
    eoPropertiesBuilder *properties;

    std::function<void(void)> closeCallback;

    float hue = 0.0f;
    float saturation = 100.0f;
    float value = 100.0f;

    float r = 255.0f;
    float g = 0.0f;
    float b = 0.0f;

public:

    eoColorDialog(eoWindowMain *mainWindow, eoColor* color, std::function<void(void)> closeCallback = std::function<void(void)>());

    eoOGLColorWidget *getColorWidget();
    eoOGLColorWidget *getHueWidget();
    eoPropertiesBuilder *getProperties();

    void setSV(float sat, float val);

    void calculateRGB();
};
