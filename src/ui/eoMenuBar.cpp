//
// Created by ego on 8/4/18.
//

#include <QtWidgets/QStyle>
#include <QFileDialog>
#include <iostream>
#include "eoMenuBar.h"
#include "../eoTexture.h"
#include "../eoLayerRaster.h"
#include "../actions/eoActionLoadImage.h"
#include "eoWindowMain.h"
#include "../eoMessageBox.h"
#include "../actions/eoActionFillColor.h"
#include "../actions/eoActionMoveLayerUp.h"
#include "../actions/eoActionMoveLayerDown.h"
#include "../eoPropertiesBuilder.h"

eoMenuBar::eoMenuBar(eoWindowMain *window) : QMenuBar(window), mainWindow(window) {

    fileMenu = addMenu(tr("&File"));

    addSubmenu(fileMenu, "New Image", "document-new", QKeySequence::New, &eoMenuBar::newDocument);
    addSubmenu(fileMenu, "Open", "document-open", QKeySequence::Open, &eoMenuBar::openDocument);
    addSubmenu(fileMenu, "Open recent", "document-open-recent", QKeySequence(), nullptr);
    addSubmenu(fileMenu, "Import", "", QKeySequence("Ctrl+Shift+O"), &eoMenuBar::importImage);

    fileMenu->addSeparator();

    addSubmenu(fileMenu, "Save", "document-save", QKeySequence::Save, &eoMenuBar::saveDocument);
    addSubmenu(fileMenu, "Save As", "document-save-as", QKeySequence::SaveAs, nullptr);
    addSubmenu(fileMenu, "Export", "", QKeySequence("Ctrl+E"), nullptr);
    addSubmenu(fileMenu, "Export As", "", QKeySequence("Ctrl+Shift+E"), nullptr);

    fileMenu->addSeparator();

    addSubmenu(fileMenu, "Print", "document-print", QKeySequence::Print, nullptr);

    fileMenu->addSeparator();

    addSubmenu(fileMenu, "Quit", "application-exit", QKeySequence::Quit, &eoMenuBar::quitApplication);

    editMenu = addMenu(tr("&Edit"));

    addSubmenu(editMenu, "Undo", "edit-undo", QKeySequence::Undo, &eoMenuBar::undo);
    addSubmenu(editMenu, "Redo", "edit-redo", QKeySequence("Ctrl+Y"), &eoMenuBar::redo);

    editMenu->addSeparator();

    addSubmenu(editMenu, "Cut", "edit-cut", QKeySequence::Cut, nullptr);
    addSubmenu(editMenu, "Copy", "edit-copy", QKeySequence::Copy, nullptr);
    addSubmenu(editMenu, "Paste", "edit-paste", QKeySequence::Paste, nullptr);
    addSubmenu(editMenu, "Delete", "edit-delete", QKeySequence("Delete"), &eoMenuBar::deleteActive);

    editMenu->addSeparator();
    
    addSubmenu(editMenu, "Scale Image", "", QKeySequence(), nullptr);
    addSubmenu(editMenu, "Resize Canvas", "", QKeySequence(), nullptr);

    editMenu->addSeparator();

    addSubmenu(editMenu, "Fill", "", QKeySequence(), nullptr);

    editMenu->addSeparator();

    addSubmenu(editMenu, "Preferences", "preferences-other", QKeySequence(), nullptr);

    selectMenu = addMenu(tr("&Select"));

    addSubmenu(selectMenu, "Select All", "edit-select-all", QKeySequence("Ctrl+A"), &eoMenuBar::selectAll);
    addSubmenu(selectMenu, "Select None", "edit-select-none", QKeySequence("Ctrl+Shift+A"), &eoMenuBar::selectNone);
    addSubmenu(selectMenu, "Invert Selection", "", QKeySequence("Ctrl+I"), &eoMenuBar::selectInvert);

    selectMenu->addSeparator();

    addSubmenu(selectMenu, "Grow", "", QKeySequence(""), &eoMenuBar::selectionGrow);
    addSubmenu(selectMenu, "Shrink", "", QKeySequence(""), &eoMenuBar::selectionShrink);
    addSubmenu(selectMenu, "Blur Selection", "", QKeySequence(""), nullptr);
    addSubmenu(selectMenu, "Harden Selection", "", QKeySequence(""), &eoMenuBar::selectionHarden);

    viewMenu = addMenu(tr("&View"));

    QMenu *canvasBack = viewMenu->addMenu(tr("&Canvas Background"));
    canvasBack->setIcon(QIcon::fromTheme("viewimage", QIcon()));
    addSubmenu(canvasBack, "Checkerboard", "", QKeySequence(), &eoMenuBar::canvasBackCheck);
    addSubmenu(canvasBack, "White", "", QKeySequence(), &eoMenuBar::canvasWhite);
    addSubmenu(canvasBack, "Black", "", QKeySequence(), &eoMenuBar::canvasBlack);
    addSubmenu(canvasBack, "Pink", "", QKeySequence(), &eoMenuBar::canvasPink);
    addSubmenu(canvasBack, "Green", "", QKeySequence(), &eoMenuBar::canvasGreen);

    layersMenu = addMenu(tr("&Layers"));

    addSubmenu(layersMenu, "Toggle Visibility", "visibility", QKeySequence(), &eoMenuBar::layerToggle);
    addSubmenu(layersMenu, "Move Layer Up", "up", QKeySequence(), &eoMenuBar::layerMoveUp);
    addSubmenu(layersMenu, "Move Layer Down", "down", QKeySequence(), &eoMenuBar::layerMoveDown);
    addSubmenu(layersMenu, "Remove Current Layer", "edit-delete", QKeySequence(), &eoMenuBar::layerRemove);

    layersMenu->addSeparator();

    addSubmenu(layersMenu, "Crop to content", "image-crop", QKeySequence(), nullptr);

    modifyMenu = addMenu(tr("&Modify"));

    QMenu *colorMenu = modifyMenu->addMenu(tr("&Color"));
    colorMenu->setIcon(QIcon::fromTheme("colorfx", QIcon()));
    QMenu *effectMenu = modifyMenu->addMenu(tr("&Effect"));
    effectMenu->setIcon(QIcon::fromTheme("draw-star", QIcon()));
    QMenu *distortMenu = modifyMenu->addMenu(tr("&Distort"));
    distortMenu->setIcon(QIcon::fromTheme("lensdistortion", QIcon()));
    QMenu *blurMenu = modifyMenu->addMenu(tr("&Blur"));
    blurMenu->setIcon(QIcon::fromTheme("blurimage", QIcon()));

    addSubmenu(colorMenu, "Brightness/Contrast", "xfpm-brightness-lcd", QKeySequence(""), nullptr);
    addSubmenu(colorMenu, "Hue/Saturation", "color-management", QKeySequence(""), nullptr);
    addSubmenu(colorMenu, "Levels", "adjustlevels", QKeySequence(""), nullptr);
    addSubmenu(colorMenu, "Curves", "adjustcurves", QKeySequence(""), nullptr);

    addSubmenu(effectMenu, "Shadow", "", QKeySequence(""), nullptr);
    addSubmenu(effectMenu, "Inner Shadow", "", QKeySequence(""), nullptr);
    addSubmenu(effectMenu, "Outer Glow", "", QKeySequence(""), nullptr);
    addSubmenu(effectMenu, "Inner Glow", "", QKeySequence(""), nullptr);
    addSubmenu(effectMenu, "Border", "", QKeySequence(""), nullptr);

    addSubmenu(distortMenu, "Nosify", "", QKeySequence(""), nullptr);
    addSubmenu(distortMenu, "Pixelete", "", QKeySequence(""), nullptr);

    addSubmenu(blurMenu, "Simple Blur", "", QKeySequence(""), nullptr);
    addSubmenu(blurMenu, "Gaussian Blur", "", QKeySequence(""), &eoMenuBar::modifyGBlur);

    generateMenu = addMenu(tr("&Generate"));
    windowMenu = addMenu(tr("&Window"));
    helpMenu = addMenu(tr("&Help"));

    addSubmenu(helpMenu, "About", "", QKeySequence(""), &eoMenuBar::about);
}

void eoMenuBar::quitApplication() {

    exit(0);

}

eoMenuBar::~eoMenuBar() {

    delete fileMenu;
    delete editMenu;
    delete viewMenu;
    delete selectMenu;
    delete layersMenu;
    delete modifyMenu;
    delete generateMenu;
    delete windowMenu;
    delete helpMenu;

    for(auto &action :actions) {

        delete action;
    }
    actions.clear();

    if(windowNewDocument)
        delete windowNewDocument;

}

void eoMenuBar::importImage() {

    QString fileName = QFileDialog::getOpenFileName(window(), 
            tr("Open Image Document"), "",
            tr(
                "Image files (*.png *.jpg *.jpeg *.tga *.bmp *.gif);;"
                "PNG document (*.png);;"
                "JPEG document (*.jpg *.jpeg);;"
                "TGA file (*.tga);;"
                "All Files (*)"));

    if(fileName.isEmpty())
        return;

    mainWindow->getCurrentCanvas()->getActionManager()
        ->perform(new eoActionLoadImage(mainWindow->getCurrentCanvas(), fileName.toStdString().c_str()));
}

void eoMenuBar::openDocument() {

    QString fileName = QFileDialog::getOpenFileName(window(), 
            tr("Open Image Document"), "",
            tr(
                "Image files (*.png *.jpg *.jpeg *.tga *.bmp *.gif);;"
                "PNG document (*.png);;"
                "JPEG document (*.jpg *.jpeg);;"
                "TGA file (*.tga);;"
                "All Files (*)"));

    if(fileName.isEmpty())
        return;

    auto fileNameStd  = fileName.toStdString();
    auto render = mainWindow->getGlArea()->getRender();

    render->performOnRender([render, fileNameStd](eoCanvas* canvas) {

        auto texture = eoTexture::load(fileNameStd.c_str());

        render->createDocumentFromTexture(
            basename(fileNameStd.c_str()),
            texture
        );

        delete texture;
    });

    mainWindow->getGlArea()->update();
}

void eoMenuBar::saveDocument() {

    QString fileName = QFileDialog::getSaveFileName(window(),
            tr("Save Image Document"), "",
            tr(
                "PNG document (*.png);;"
                "JPEG document (*.jpg *.jpeg);;"
                "TGA file (*.tga);;"
                ));

    if(fileName.isEmpty())
        return;

    mainWindow->getCurrentCanvas()->performOnRender([fileName] (eoCanvas* canvas) {
            eoFrameBuffer* buffer = canvas->getFrameBuffer();
            QImage image = buffer->toQImage().mirrored();
            image.save(fileName);
    });

}

void eoMenuBar::newDocument() {

    if(windowNewDocument)
        delete windowNewDocument;
    
    windowNewDocument = new eoWindowNewDocument(mainWindow);
    windowNewDocument->show();
}

QAction *eoMenuBar::addSubmenu(QMenu *menu, QString name, QString icon, QKeySequence shortcut, void (eoMenuBar::*callback)(void)) {
    
    const QIcon actionIcon = QIcon::fromTheme(icon, QIcon());
    QAction *action;

    action = new QAction();
    action->setText(name);
    action->setShortcut(shortcut);
    action->setIcon(actionIcon);
    menu->addAction(action);

    if(callback)
        connect(action, &QAction::triggered, this, callback);

    actions.emplace_back(action);

    return action;
}

void eoMenuBar::undo() {
    
    auto canvas = mainWindow->getCurrentCanvas();

    if(canvas) {
        
        canvas->getActionManager()->neglectLastAction();
    }
}

void eoMenuBar::redo() {
    
    auto canvas = mainWindow->getCurrentCanvas();

    if(canvas) {
        
        canvas->getActionManager()->retrieveLastAction();
    }
}

void eoMenuBar::selectAll() {
    
    auto canvas = mainWindow->getCurrentCanvas();

    if(canvas) {
        
        canvas->selectAll();
    }
}

void eoMenuBar::selectNone() {
    
    auto canvas = mainWindow->getCurrentCanvas();

    if(canvas) {
        
        canvas->selectNone();
    }
}

void eoMenuBar::selectInvert() {
    

    auto canvas = mainWindow->getCurrentCanvas();

    if(canvas) {
        
        canvas->invertSelection();
    }
}

void eoMenuBar::selectionGrow() {
    
    auto canvas = mainWindow->getCurrentCanvas();

    if(canvas) {
        
        canvas->growSelection(10.0f);
    }
}

void eoMenuBar::selectionShrink() {
    
    auto canvas = mainWindow->getCurrentCanvas();

    if(canvas) {
        
        canvas->growSelection(-10.0f);
    }
}

void eoMenuBar::selectionHarden() {
    
    auto canvas = mainWindow->getCurrentCanvas();

    if(canvas) {
        
        canvas->hardenSelection();
    }
}

void eoMenuBar::about() {
    
    eoMessageBox::show("eoImagné - Free and Open Source Graphic Program. GNU GENERAL PUBLIC LICENSE v3. Created and maintained by Mikołaj Bednarek.");
}

void eoMenuBar::deleteActive() {

    static char *fillTransparent = "Fill with transparency";
    auto canvas = mainWindow->getCurrentCanvas();

    if(canvas) {

        canvas->performOnRender([] (eoCanvas *canvas) {
            canvas->getActionManager()->perform(new eoActionFillColor(fillTransparent, canvas, eoColor::TRANSPARENT));
        });
    }
}

void eoMenuBar::layerMoveUp() {
    
    auto canvas = mainWindow->getCurrentCanvas();

    if(canvas) {

        canvas->getActionManager()->perform(new eoActionMoveLayerUp(canvas));
    }
}

void eoMenuBar::layerMoveDown() {
    
    auto canvas = mainWindow->getCurrentCanvas();

    if(canvas) {

        canvas->getActionManager()->perform(new eoActionMoveLayerDown(canvas));
    }
}

void eoMenuBar::layerToggle() {

    auto canvas = mainWindow->getCurrentCanvas();

    if(!canvas)
        return;

    canvas->toggleLayerVisibility(canvas->getCurrentLayer());
}

void eoMenuBar::layerRemove() {
    
}

void eoMenuBar::canvasBackCheck() {

    auto canvas = mainWindow->getCurrentCanvas();

    if(canvas) {
    
        canvas->setBackground(eoCanvas::CHECKERBOARD);
        mainWindow->getGlArea()->update();
    }
    
}

void eoMenuBar::canvasWhite() {

    auto canvas = mainWindow->getCurrentCanvas();

    if(canvas) {
    
        canvas->setBackground(eoCanvas::WHITE);
        mainWindow->getGlArea()->update();
    }
    
}

void eoMenuBar::canvasBlack() {

    auto canvas = mainWindow->getCurrentCanvas();

    if(canvas) {
    
        canvas->setBackground(eoCanvas::BLACK);
        mainWindow->getGlArea()->update();
    }
    
}

void eoMenuBar::canvasPink() {

    auto canvas = mainWindow->getCurrentCanvas();

    if(canvas) {
    
        canvas->setBackground(eoCanvas::PINK);
        mainWindow->getGlArea()->update();
    }
    
}

void eoMenuBar::canvasGreen() {

    auto canvas = mainWindow->getCurrentCanvas();

    if(canvas) {
    
        canvas->setBackground(eoCanvas::GREEN);
        mainWindow->getGlArea()->update();
    }
    
}

void eoMenuBar::modifyGBlur() {
    
    static float f;
    eoPropertiesBuilder *properties = new eoPropertiesBuilder();

    properties->setFixedSize(QSize(240, 160));
    properties->setWindowTitle("Gaussian Blur");
    properties->addFloatSlider("Deviation", 1, 20, &f);
    properties->addSplitter();
    properties->addSplitter();
    properties->addButton("Apply", [] () {});
    properties->show();
    properties->setFocus();
}
