//
// Created by ego on 8/4/18.
//

#ifndef GFX_PROTOTYPE_EOMENUBAR_H
#define GFX_PROTOTYPE_EOMENUBAR_H

#include <QMenuBar>
#include "eoWindowNewDocument.h"
#include <vector>
#include <functional>


class eoWindowMain;

class eoMenuBar : public QMenuBar {

    Q_OBJECT

    std::vector<QAction*> actions;

    QMenu* fileMenu;
    QMenu* editMenu;
    QMenu* viewMenu;
    QMenu* selectMenu;
    QMenu* layersMenu;
    QMenu* modifyMenu;
    QMenu* generateMenu;
    QMenu* windowMenu;
    QMenu* helpMenu;

    eoWindowMain* mainWindow;
    eoWindowNewDocument* windowNewDocument = nullptr;

public:

    eoMenuBar(eoWindowMain *window = nullptr);
    ~eoMenuBar();

    QAction *addSubmenu(QMenu *menu, QString name, QString icon, QKeySequence shortcut, void (eoMenuBar::*callback)(void));

private slots:

    void quitApplication();
    void openDocument();
    void importImage();
    void saveDocument();
    void newDocument();

    void undo();
    void redo();
    void deleteActive();
    
    void selectAll();
    void selectNone();
    void selectInvert();
    void selectionGrow();
    void selectionShrink();
    void selectionHarden();

    void canvasBackCheck();
    void canvasWhite();
    void canvasBlack();
    void canvasPink();
    void canvasGreen();

    void layerMoveUp();
    void layerMoveDown();
    void layerToggle();
    void layerRemove();

    void modifyGBlur();

    void about();
};


#endif //GFX_PROTOTYPE_EOMENUBAR_H
