//
// Created by ego on 11/10/18.
//

#include "eoOGLColorWidget.h"
#include "../shaders/eoShaderFile.h"
#include "../shaders/eoShaderVertexTrivial.h"
#include "eoColorDialog.h"
#include "../eoPropertiesBuilder.h"

eoOGLColorWidget::eoOGLColorWidget(eoColorDialog *parent, bool hue, float *hueValue) 
    :   QOpenGLWidget(parent), hue(hue), hueValue(hueValue), dialog(parent)
{
    
    QSurfaceFormat format;
    format.setVersion(3, 3);
    format.setProfile(QSurfaceFormat::CoreProfile);
    setFormat(format);

}

void eoOGLColorWidget::initializeGL() {
    
    setFocusPolicy(Qt::StrongFocus);
    makeCurrent();

    glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    eoShaderFile *fragment;


    if(hue) {
        fragment = new eoShaderFile(GL_FRAGMENT_SHADER, "data/glsl/fragment_hue_diag.glsl");
    } else {
        fragment = new eoShaderFile(GL_FRAGMENT_SHADER, "data/glsl/fragment_color_diag.glsl");
    }

    auto vertex = new eoShaderVertexTrivial();

    program = new eoGLProgram();
    program->attachShader(vertex);
    program->attachShader(fragment);
    program->link();

    delete fragment;
    delete vertex;

    buffer = eoVertexBuffer::square(1.0f, 1.0f);

    update();
}

void eoOGLColorWidget::resizeGL(int w, int h) {
    
}

void eoOGLColorWidget::paintGL() {

    glClearColor(1.0f, 1.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    program->use();
    program->setTransform(eoMatrix());
    if(!hue) {
        program->setFloat("hue", *hueValue);
    }
    buffer->render();

    glFlush();
}

void eoOGLColorWidget::mouseMoveEvent(QMouseEvent *event) {

    float x, y;

    x = event->x();
    y = event->y();

    if(x < 0.0f)
        x = 0.0f;

    if(y < 0.0f)
        y = 0.0f;

    if(x > width())
        x = width();

    if(y > height())
        y = height();

    if(hue) {
        *hueValue = 360.0f - 360.0f * y / (float)height();
        
        dialog->getColorWidget()->update();
        dialog->getHueWidget()->update();
        dialog->getProperties()->onFloatSliderChange(0);
    } else {
        
        dialog->setSV(100.0f * x / (float)width(),  100.0f - 100.0f * y / (float)height());
    }

    dialog->calculateRGB();
}
