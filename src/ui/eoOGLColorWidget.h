#pragma once
//
// Created by ego on 11/10/18.
//
#include <QtOpenGL>
#include <QOpenGLWidget>
#include "../eoGLProgram.h"
#include "../eoVertexBuffer.h"

class eoColorDialog;
class eoPropertiesBuilder;

class eoOGLColorWidget : public QOpenGLWidget {

    Q_OBJECT

private:

    eoGLProgram *program;
    eoVertexBuffer *buffer;
    bool hue;
    float *hueValue;
    eoColorDialog *dialog;

public:

    eoOGLColorWidget(eoColorDialog *parent, bool hue, float* hueValue);

    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;

    void mouseMoveEvent(QMouseEvent *event) override;
};
