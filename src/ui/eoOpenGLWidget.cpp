//
// Created by ego on 6/30/18.
//

#include "eoOpenGLWidget.h"
#include "eoWindowMain.h"
#include "../eoRender.h"
#include "../eoInput.h"


eoOpenGLWidget::eoOpenGLWidget(eoWindowMain *parent)
    : QOpenGLWidget((QWidget*)parent), mainWindow(parent)
{
    screenWidth = size().width();
    screenHeight = size().height();

    setMouseTracking(true);

    QSurfaceFormat format;
    format.setVersion(4, 5);
    format.setProfile(QSurfaceFormat::CoreProfile);
    setFormat(format);
}

eoOpenGLWidget::~eoOpenGLWidget()
{
    delete render;
}

void eoOpenGLWidget::initializeGL() {
    setFocusPolicy(Qt::StrongFocus);
    initializeOpenGLFunctions();
    makeCurrent();

    glClear(GL_COLOR_BUFFER_BIT);
    render = new eoRender(screenWidth, screenHeight, mainWindow);

    mainWindow->onRenderReady();
}

void eoOpenGLWidget::resizeGL(int w, int h) {
    screenWidth = w;
    screenHeight = h;
    render->resize(w, h);
}

void eoOpenGLWidget::paintGL() {
    render->draw();
}

void eoOpenGLWidget::mousePressEvent(QMouseEvent *event) {
    auto canvas = render->getCanvas();

    if(canvas)
        canvas->getInput()->buttonPressHandle(event);
}

void eoOpenGLWidget::mouseReleaseEvent(QMouseEvent *event) {
    auto canvas = render->getCanvas();

    if(canvas)
        canvas->getInput()->buttonReleaseHandle(event);
}

void eoOpenGLWidget::mouseMoveEvent(QMouseEvent *event) {
    auto canvas = render->getCanvas();

    if(canvas)
        canvas->getInput()->motionHandle(event);
}

void eoOpenGLWidget::keyPressEvent(QKeyEvent *event) {
    auto canvas = render->getCanvas();

    if(canvas)
        canvas->getInput()->keyPressHandle(event);
}

void eoOpenGLWidget::keyReleaseEvent(QKeyEvent *event) {
    auto canvas = render->getCanvas();

    if(canvas)
        canvas->getInput()->keyReleaseHandle(event);
}

void eoOpenGLWidget::wheelEvent(QWheelEvent *event) {
    auto canvas = render->getCanvas();

    if(canvas)
        canvas->getInput()->wheelHandle(event);
}

eoRender *eoOpenGLWidget::getRender() const {
    return render;
}
