//
// Created by ego on 6/30/18.
//

#ifndef GFX_PROTOTYPE_EOOPENGLWIDGET_H
#define GFX_PROTOTYPE_EOOPENGLWIDGET_H

#include <QtOpenGL>
#include <QOpenGLWidget>

class eoRender;
class eoInput;
class eoWindowMain;

class eoOpenGLWidget : public QOpenGLWidget, protected QOpenGLFunctions {

    Q_OBJECT

    int screenWidth;
    int screenHeight;

    eoRender* render = nullptr;
    eoWindowMain* mainWindow;

public:
    explicit eoOpenGLWidget(eoWindowMain *parent);
    ~eoOpenGLWidget() override;

    eoRender *getRender() const;

private:
    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;

    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;

protected:
    void wheelEvent(QWheelEvent *event) override;

private:

    void keyPressEvent(QKeyEvent *event) override;

    void keyReleaseEvent(QKeyEvent *event) override;
};


#endif //GFX_PROTOTYPE_EOOPENGLWIDGET_H
