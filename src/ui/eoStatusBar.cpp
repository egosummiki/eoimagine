//
// Created by ego on 7/9/18.
//

#include "eoStatusBar.h"
#include "eoWindowMain.h"
#include "../tools/eoTool.h"
#include <QKeyEvent>


eoStatusBar::eoStatusBar(eoWindowMain* windowMain) : QStatusBar(nullptr), windowMain(windowMain) {


    modeLabel = new QLabel();
    modeLabel->setText(tr("NORMAL"));
    modeLabel->setStyleSheet("QLabel { background-color: #24cd6a; color: #31363b; margin-right: 4px; margin-left: 2px; font-weight: bold; border-radius: 2px; padding-left: 12px; padding-right: 12px; }");
    addWidget(modeLabel);

    commandBox = new QLineEdit();
    commandBox->setStyleSheet("QLineEdit { background-color: rgba(0,0,0,0); border: none; }");
    addWidget(commandBox, 1);
    connect(commandBox, SIGNAL(returnPressed()), this, SLOT(commandReady()));
    connect(commandBox, SIGNAL(editingFinished()), this, SLOT(commandDismiss()));
    commandBox->setVisible(false);

    auto runCommand = new QShortcut(QKeySequence(Qt::SHIFT + Qt::Key_Semicolon), windowMain);
    
    connect(runCommand, &QShortcut::activated, [this] () {
       setCommandMode();
    });

    spacer = new QSplitter();
    addWidget(spacer, true);

    infoWidget = new QWidget();
    infoLayout = new QHBoxLayout();
    infoWidget->setLayout(infoLayout);

    infoLayout->setSpacing(0);
    infoLayout->setMargin(0);

    mouseXDecor = new QLabel();
    mouseXDecor->setText("X");
    mouseXDecor->setStyleSheet("QLabel {background-color: #505050; color: #FFF; padding: 4px; padding-left: 2px; padding-right: 1px; margin: 0px; }");
    infoLayout->addWidget(mouseXDecor);

    mouseXValue = new QLabel();
    mouseXValue->setStyleSheet("QLabel {background-color: #404040; color: #D0D0D0; padding: 4px; padding-left: 2px; padding-right: 1px; margin: 0px; }");
    infoLayout->addWidget(mouseXValue);

    mouseYDecor = new QLabel();
    mouseYDecor->setText("Y");
    mouseYDecor->setStyleSheet("QLabel {background-color: #505050; color: #FFF; padding: 4px; padding-left: 2px; padding-right: 1px; margin: 0px; }");
    infoLayout->addWidget(mouseYDecor);

    mouseYValue = new QLabel();
    mouseYValue->setStyleSheet("QLabel {background-color: #404040; color: #D0D0D0; padding: 4px; padding-left: 2px; padding-right: 1px; margin: 0px; margin-right: 12px; }");
    infoLayout->addWidget(mouseYValue);

    zoomDecor = new QLabel();
    zoomDecor->setText("🔍");
    zoomDecor->setStyleSheet("QLabel {background-color: #505050; color: #FFF; padding-bottom: 0px; padding-top: 4px; padding-left: 2px; padding-right: 1px; margin: 0px; }");
    infoLayout->addWidget(zoomDecor);

    zoomValue = new QLabel();
    zoomValue->setStyleSheet("QLabel {background-color: #404040; color: #D0D0D0; padding: 4px; padding-left: 2px; padding-right: 1px; margin: 0px; }");
    infoLayout->addWidget(zoomValue);

    addWidget(infoWidget);

    updateMousePos(0.0, 5.2);
    updateZoom(1.0f);
}

void eoStatusBar::updateMousePos(double x, double y) {

    QString string;
    string.sprintf("%.2f", x);
    mouseXValue->setText(string);
    string.sprintf("%.2f", y);
    mouseYValue->setText(string);
}

void eoStatusBar::updateZoom(float zoom) {

    QString string;
    string.sprintf("%.2f\%", zoom*100.0f);
    zoomValue->setText(string);
}

void eoStatusBar::setCommandMode() {

    commandMode = true;

    setModeLabel("COMMAND", "#dbce57", "#31363b");

    spacer->setVisible(false);

    commandBox->setText(tr(":"));
    commandBox->setVisible(true);
    commandBox->setFocus();
}

void eoStatusBar::commandReady() {

    windowMain->getGlArea()->getRender()->getCanvas()->getCommandParser()->parse(commandBox->text());
    commandDismiss();
}

void eoStatusBar::commandDismiss() {

    commandMode = false;

    auto tool = windowMain->getGlArea()->getRender()->getCurrentTool();

    if(tool) {

        auto label = windowMain->getGlArea()->getRender()->getCurrentTool()->getLabel();
        setModeLabel(label.label, label.background, label.foreground);
    }

    spacer->setVisible(true);
    commandBox->setVisible(false);

}

void eoStatusBar::keyPressEvent(QKeyEvent *event) {

    if(commandMode) {

        if(event->key() == Qt::Key_Escape) {

            commandDismiss();
        }
    }

}

void eoStatusBar::setModeLabel(QString label, QString backColor, QString textColor) {

    modeLabel->setText(label);
    modeLabel->setStyleSheet(QString("QLabel { background-color: %1; color: %2; margin-right: 4px; margin-left: 2px; font-weight: bold; border-radius: 2px; padding-left: 12px; padding-right: 12px; }")
        .arg(backColor).arg(textColor));

}

eoStatusBar::~eoStatusBar() {

    delete modeLabel;
    delete spacer;
    delete infoWidget;
    delete infoLayout;

}

