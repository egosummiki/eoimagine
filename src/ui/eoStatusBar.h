//
// Created by ego on 7/9/18.
//

#ifndef GFX_PROTOTYPE_EOSTATUSBAR_H
#define GFX_PROTOTYPE_EOSTATUSBAR_H

#include <QStatusBar>
#include <QLabel>
#include <QLineEdit>
#include <QHBoxLayout>
#include <QSplitter>

class eoWindowMain;

class eoStatusBar : public QStatusBar  {

    Q_OBJECT


    QLabel* modeLabel;

    QSplitter* spacer;

    QWidget* infoWidget;
    QHBoxLayout* infoLayout;

    QLabel* mouseXDecor;
    QLabel* mouseXValue;
    QLabel* mouseYDecor;
    QLabel* mouseYValue;

    QLabel* zoomDecor;
    QLabel* zoomValue;

    QLineEdit* commandBox;
    eoWindowMain* windowMain;

    bool commandMode;

public:

    explicit eoStatusBar(eoWindowMain* windowMain);
    ~eoStatusBar();
    void updateMousePos(double x, double y);
    void updateZoom(float zoom);
    void setCommandMode();
    void setModeLabel(QString label, QString backColor, QString textColor);

protected:
    void keyPressEvent(QKeyEvent *event) override;

private slots:

    void commandReady();
    void commandDismiss();

};


#endif //GFX_PROTOTYPE_EOSTATUSBAR_H
