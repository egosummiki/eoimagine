//
// Created by ego on 20/09/18.
//

#include "eoTabButton.h"
#include "eoOpenGLWidget.h"
#include <cstdio>

eoTabButton::eoTabButton(eoCanvas* canvas)
    :   QWidget(),
        canvas(canvas),
        label(new QPushButton(canvas->getDocumentTitle().c_str())),
        closeButton(new QToolButton())
{
    setInactiveStyle();
    this->setMaximumHeight(24);

    closeButton->setIcon(QIcon("data/close.png"));

    layout = new QHBoxLayout();
    layout->setMargin(0);
    layout->setSpacing(0);
    layout->addWidget(label);
    layout->addWidget(closeButton);

    //label->setStyleSheet(
            //"padding-top: 2px; padding-bottom: 8px"
            //"padding-left: 4px; padding-right: 4px;"
            //);
//
    closeButton->setStyleSheet(
            "padding-top: 2px; padding-bottom: 0px;"
            "height: 100%;"
            );

    connect(label, &QPushButton::pressed, this, &eoTabButton::setCurrent);
    connect(closeButton, &QPushButton::released, this, &eoTabButton::remove);

    this->setLayout(layout);
}

eoTabButton::~eoTabButton() {

    delete label;
    delete closeButton;
    
}

void eoTabButton::setCurrent() {
    
    canvas->setCurrent();
}

void eoTabButton::setInactiveStyle() {
    
    setStyleSheet(
            "border-radius: 0px;"
            "border: 0px;"
            "background: #353535;"
            "font-size: 11pt;"
            "padding-left: 8px; padding-right: 14px;"
            "padding-top: 2px; padding-bottom:2px"
            );
}

void eoTabButton::setActiveStyle() {
    
    setStyleSheet(
            "border-radius: 0px;"
            "border: 0px;"
            "background: #252525;"
            "font-size: 11pt;"
            "padding-left: 8px; padding-right: 14px;"
            "padding-top: 2px; padding-bottom:2px"
            );
}

eoCanvas* eoTabButton::getCanvas() {
    
    return canvas;
}


void eoTabButton::remove() {
    
    canvas->kill();
}
