#pragma once
//
// Created by ego on 20/09/18.
//

#include <QWidget>
#include <QHBoxLayout>
#include <QToolButton>
#include <QPushButton>
#include "../eoCanvas.h"

class eoTabButton : public QWidget {

    Q_OBJECT

private:
    QHBoxLayout *layout;

    eoCanvas* canvas;
    QPushButton *label;
    QToolButton *closeButton;


public:
    
    explicit eoTabButton(eoCanvas* canvas);
    ~eoTabButton();

    void setInactiveStyle();
    void setActiveStyle();
    eoCanvas* getCanvas();

private slots:

    void setCurrent();
    void remove();
};
