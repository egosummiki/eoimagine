//
// Created by ego on 22/09/18.
//

#include "eoTabLayout.h"

eoTabLayout::eoTabLayout() {

    setMargin(0);
    setSpacing(0);
    setAlignment(Qt::AlignLeft);
}

void eoTabLayout::addTab(eoCanvas* canvas) {
    
    auto tab = new eoTabButton(canvas);
    tabList.append(tab);
    addWidget(tab);
}

eoTabButton* eoTabLayout::getTab(eoCanvas* canvas) {
    
    for(auto& tab :tabList) {

        auto tabButton = static_cast<eoTabButton*>(tab);

        if(tabButton->getCanvas() == canvas)
            return tabButton;
    }

    return nullptr;
}

void eoTabLayout::removeTab(eoCanvas* canvas) {
    int index = 0;

    for(auto& tab :tabList) {

        auto tabButton = static_cast<eoTabButton*>(tab);

        if(tabButton && tabButton->getCanvas() == canvas) {
            removeWidget(tabButton);
            tabList.erase(tabList.begin() + index); 
            delete tabButton;
        }

        ++index;
    }

}
