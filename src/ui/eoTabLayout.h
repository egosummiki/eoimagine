#pragma once
//
// Created by ego on 22/09/18.
//
#include <QHBoxLayout>
#include <QWidgetList>
#include "../eoCanvas.h"
#include "eoTabButton.h"

class eoTabLayout : public QHBoxLayout {

    Q_OBJECT

    QWidgetList tabList;

public:

    eoTabLayout();
    void addTab(eoCanvas* canvas);
    void removeTab(eoCanvas* canvas);
    eoTabButton* getTab(eoCanvas* canvas);
};
