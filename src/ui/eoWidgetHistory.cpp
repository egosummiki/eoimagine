//
// Created by ego on 7/9/18.
//

#include <QHBoxLayout>
#include "eoWidgetHistory.h"
#include <QTreeView>

eoWidgetHistory::eoWidgetHistory(QWidget *parent) {

    auto layout = new QHBoxLayout();

    listView = new QListView();
    layout->addWidget(listView);
    setLayout(layout);

}

QListView* eoWidgetHistory::getListView() {
    
    return listView;
}
