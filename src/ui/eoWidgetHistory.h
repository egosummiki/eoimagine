//
// Created by ego on 7/9/18.
//

#ifndef GFX_PROTOTYPE_EOWIDGETHISTORY_H
#define GFX_PROTOTYPE_EOWIDGETHISTORY_H

#include <QWidget>
#include <QListView>

class eoWidgetHistory : public QWidget {

    Q_OBJECT

private:

    QListView* listView;

public:

    explicit eoWidgetHistory(QWidget *parent = nullptr);
    QListView* getListView();

};


#endif //GFX_PROTOTYPE_EOWIDGETHISTORY_H
