#pragma once
//
// Created by ego on 31/08/18.
//
#include <QWidget>
#include <QToolButton>
#include "../eoLayer.h"

class eoWidgetLayer : public QWidget {

    Q_OBJECT

    
private:

    QToolButton* visibleButton;

public:

    eoWidgetLayer(eoLayer* layer);
    static void loadIcons();
};
