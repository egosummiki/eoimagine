//
// Created by ego on 7/9/18.
//

#include "eoWidgetLayers.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QToolButton>
#include <QTreeWidget>

#include "../shaders/eoShaderModifiers.h"
#include "../eoLayer.h"
#include "../eoLayerRaster.h"
#include "../eoLayerTreeDelegate.h"
#include "../actions/eoActionSetCurrentLayer.h"

QIcon eoWidgetLayers::iconVisible;
QIcon eoWidgetLayers::iconInvisible;

eoWidgetLayers::eoWidgetLayers(eoWindowMain *parent)
: QWidget(parent), mainWindow(parent)
{

    auto layout = new QVBoxLayout();
    auto bottomLayout = new QHBoxLayout();
    bottomLayout->setSpacing(0);
    bottomLayout->setMargin(0);

    blendingCombo = new QComboBox();
    blendingCombo->addItem("Normal");
    blendingCombo->addItem("Darken");
    blendingCombo->addItem("Multiply");
    blendingCombo->addItem("Color burn");
    blendingCombo->addItem("Linear burn");
    blendingCombo->addItem("Lighten");
    blendingCombo->addItem("Screen");
    blendingCombo->addItem("Color dodge");
    blendingCombo->addItem("Linear dodge");
    blendingCombo->addItem("Overlay");
    blendingCombo->addItem("Soft light");
    blendingCombo->addItem("Hard light");
    blendingCombo->addItem("Vivid light");
    blendingCombo->addItem("Linear light");
    blendingCombo->addItem("Pin light");
    blendingCombo->addItem("Difference");
    blendingCombo->addItem("Exclusion");

    connect(blendingCombo, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &eoWidgetLayers::changedBlending);

    opacitySlider = new QSlider(Qt::Horizontal);
    opacitySlider->setRange(0, 100);
    opacitySlider->setValue(100);

    connect(opacitySlider, QOverload<int>::of(&QSlider::sliderMoved), this, &eoWidgetLayers::changedOpacity);

    auto addButton = new QToolButton();
    addButton->setIcon(QIcon("data/new_layer.png"));
    auto removeButton = new QToolButton();
    removeButton->setIcon(QIcon("data/remove_layer.png"));
    auto moveUp = new QToolButton();
    moveUp->setIcon(QIcon("data/moveup_layer.png"));
    auto moveDown = new QToolButton();
    moveDown->setIcon(QIcon("data/movedown_layer.png"));
    auto propertiesButton = new QToolButton();
    propertiesButton->setIcon(QIcon("data/mod.png"));

    iconVisible = QIcon("data/eye.png");
    iconInvisible = QIcon("data/eye-crossed.png");

    bottomLayout->addWidget(addButton);
    bottomLayout->addWidget(removeButton);
    bottomLayout->addWidget(moveDown);
    bottomLayout->addWidget(moveUp);
    bottomLayout->addWidget(propertiesButton);

    connect(removeButton, &QToolButton::released, this, &eoWidgetLayers::removeCurrentLayer);
    connect(addButton, SIGNAL(released()), this, SLOT(addNewLayer()));
    connect(moveDown, SIGNAL(released()), this, SLOT(moveLayerDown()));
    connect(moveUp, SIGNAL(released()), this, SLOT(moveLayerUp()));

    layerTree = new QTreeView();
    layerTree->setHeaderHidden(true);
    layerTree->setRootIsDecorated(false);
    layerTree->setSelectionBehavior(QAbstractItemView::SelectRows);
    layerTree->setItemDelegateForColumn(0, new eoLayerTreeDelegate());
    layerTree->setEditTriggers(QAbstractItemView::AllEditTriggers);

    connect(layerTree, &QTreeView::clicked, this, &eoWidgetLayers::treeClicked);

    layout->addWidget(blendingCombo);
    layout->addWidget(opacitySlider);
    layout->addWidget(layerTree);
    layout->addLayout(bottomLayout);
    setLayout(layout);

}

QTreeView *eoWidgetLayers::getLayerTree() const {
    return layerTree;
}

void eoWidgetLayers::removeCurrentLayer() {

    mainWindow->getCurrentCanvas()->getCurrentLayer()->remove();

}

void eoWidgetLayers::addNewLayer() {

    mainWindow->getCurrentCanvas()->performOnRender([] (eoCanvas* canvas) {
        canvas->addLayer(
                new eoLayerRaster(
                        canvas,
                        eoColor::TRANSPARENT,
                        QString("Layer %1").arg(canvas->getLayerCount()))
        );
    });

    mainWindow->getGlArea()->update();
}

void eoWidgetLayers::moveLayerUp() {

    mainWindow->getCurrentCanvas()->moveCurrentLayerUp();
    mainWindow->getGlArea()->update();
}

void eoWidgetLayers::moveLayerDown() {

    mainWindow->getCurrentCanvas()->moveCurrentLayerDown();
    mainWindow->getGlArea()->update();
}

/*
 * Selection changed slot.
 * */
void eoWidgetLayers::onSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected) {

    eoCanvas* canvas;
    eoLayer* layer;
    canvas = mainWindow->getCurrentCanvas();

    if(!canvas)
        return;

    // If selection is empty select the current layer.
    if(!selected.size()) {

        layer = canvas->getCurrentLayer();

        if(!layer || layer->getModelIndex().row() < 0)
            return;

        selectRow(layer->getModelIndex().row());

        return;
    }

    layer = canvas->rowToLayer(selected.indexes().front().row());

    // Set first element of selection as current layer.
    if(canvas->getCurrentLayer() != layer) {

        canvas->getActionManager()->perform(new eoActionSetCurrentLayer(canvas, layer));
    }
}

/*
 * Signal needs to be connected after the model is attached.
 * */
void eoWidgetLayers::connectSelectionSignal() {
    auto selectionModel = layerTree->selectionModel();
    connect(selectionModel, &QItemSelectionModel::selectionChanged, this, &eoWidgetLayers::onSelectionChanged);

    layerTree->setColumnWidth(0, 0);
}

/*
 * Disconnection used on canvas change.
 * */
void eoWidgetLayers::disconnectSelectionSignal() {

    auto selectionModel = layerTree->selectionModel();
    disconnect(selectionModel, &QItemSelectionModel::selectionChanged, 0, 0);
}

/*
 * Select given row from the layer tree.
 * */
void eoWidgetLayers::selectRow(int row) {
    
    auto selection = layerTree->selectionModel();
    selection->clearSelection();
    selection->select(
        QItemSelection(
            layerTree->model()->index(row, 0),
            layerTree->model()->index(row, 1)
        ), QItemSelectionModel::Select);
}

void eoWidgetLayers::treeClicked(const QModelIndex &index) {


    if(!index.column()) {
        auto layer = static_cast<eoLayer*>(index.internalPointer());

        if(layer) {
            
            if(layer->hasAttribute(eoLayer::INVISIBLE)) {

                layer->removeAttribute(eoLayer::INVISIBLE);
            } else {

                layer->setAttribute(eoLayer::INVISIBLE);
            }

            mainWindow->getGlArea()->update();
            emit layerTree->dataChanged(index, index);

        }
    }
}

void setBlendMode(eoCanvas* canvas, eoShaderModifiers::BlendingMode mode) {

    canvas->performOnRender([mode] (eoCanvas* canvas) {

        auto layer = canvas->getCurrentLayer();

        if(layer && layer->hasAttribute(eoLayer::RASTER)) {
            
            auto rasterLayer = static_cast<eoLayerRaster*>(layer);
            rasterLayer->setBlendingMode(mode);
        }
    });
}

void eoWidgetLayers::changedBlending(int index) {
    
    setBlendMode(mainWindow->getCurrentCanvas(), static_cast<eoShaderModifiers::BlendingMode>(index));
    mainWindow->getGlArea()->update();
}

QComboBox *eoWidgetLayers::getBlendingCombo() {
    
    return blendingCombo;
}

QSlider *eoWidgetLayers::getOpacitySlider() {

    return opacitySlider;
}

void eoWidgetLayers::changedOpacity(int value) {

    if(!mainWindow->getCurrentCanvas())
        return;
    
    auto layer = mainWindow->getCurrentCanvas()->getCurrentLayer();

    if(layer && layer->hasAttribute(eoLayer::RASTER)) {

            auto rasterLayer = static_cast<eoLayerRaster*>(layer);
            rasterLayer->setOpacity(static_cast<float>(value) / 100.0f);
    }

    mainWindow->getGlArea()->update();
}
