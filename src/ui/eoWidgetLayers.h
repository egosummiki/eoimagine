//
// Created by ego on 7/9/18.
//

#ifndef GFX_PROTOTYPE_EOWIDGETLAYERS_H
#define GFX_PROTOTYPE_EOWIDGETLAYERS_H

#include <QWidget>
#include <QComboBox>
#include <QSlider>
#include "eoWindowMain.h"
#include "../eoLayerTreeModel.h"

class eoWidgetLayers : public QWidget  {

    Q_OBJECT

    eoWindowMain    *mainWindow;
    QTreeView       *layerTree;
    QComboBox       *blendingCombo;
    QSlider         *opacitySlider;


public:
    explicit eoWidgetLayers(eoWindowMain *parent = nullptr);

    QTreeView *getLayerTree() const;
    void connectSelectionSignal();
    void disconnectSelectionSignal();
    void selectRow(int row);
    QComboBox *getBlendingCombo();
    QSlider *getOpacitySlider();

    static QIcon iconVisible;
    static QIcon iconInvisible;

private slots:

    void onSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
    void removeCurrentLayer();
    void addNewLayer();
    void moveLayerUp();
    void moveLayerDown();
    void treeClicked(const QModelIndex &index);

    void changedBlending(int index);
    void changedOpacity(int value);

};


#endif //GFX_PROTOTYPE_EOWIDGETLAYERS_H
