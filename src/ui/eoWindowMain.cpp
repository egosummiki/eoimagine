//
// Created by ego on 11/24/17.
//

#include "eoWindowMain.h"
#include "../eoInput.h"
#include "../eoMessageBox.h"
#include "../tools/eoToolBar.h"
#include "eoWidgetLayers.h"
#include "eoMenuBar.h"
#include <QVBoxLayout>
#include <QHBoxLayout>


eoWindowMain::eoWindowMain(const char *window_title, int width, int height) :
        QMainWindow()
{
    setWindowTitle(window_title);
    resize(width, height);

    auto toolBar = new eoToolBar(this);
    addToolBar(Qt::LeftToolBarArea, toolBar);

    statusBar = new eoStatusBar(this);
    setStatusBar(statusBar);

    glArea = new eoOpenGLWidget(this);

    auto centralWidget = new QWidget();
    auto centralLayout = new QVBoxLayout();
    centralLayout->setMargin(0);
    centralLayout->setSpacing(0);
    tabLayout = new eoTabLayout();
    centralLayout->addLayout(tabLayout);
    centralLayout->addWidget(glArea);

    centralWidget->setLayout(centralLayout);

    setCentralWidget(centralWidget);

    menuBar = new eoMenuBar(this);
    setMenuBar(menuBar);

    propertiesDock = new QDockWidget(tr("Properties"), this);
    addDockWidget(Qt::RightDockWidgetArea, propertiesDock);

    auto historyDock = new QDockWidget(tr("History"), this);
    historyWidget = new eoWidgetHistory();
    historyDock->setWidget(historyWidget);
    addDockWidget(Qt::RightDockWidgetArea, historyDock);

    tabifyDockWidget(historyDock, propertiesDock);

    auto layersDock = new QDockWidget(tr("Layers"), this);
    layerWidget = new eoWidgetLayers(this);
    layersDock->setWidget(layerWidget);
    addDockWidget(Qt::RightDockWidgetArea, layersDock);

    eoMessageBox::mainWindow = this;
}

eoStatusBar *eoWindowMain::getStatusBar() const {
    return statusBar;
}

eoOpenGLWidget *eoWindowMain::getGlArea() const {
    return glArea;
}

void eoWindowMain::onRenderReady() {


}

eoWidgetLayers *eoWindowMain::getLayerWidget() const {
    return layerWidget;
}

eoWidgetHistory *eoWindowMain::getHistoryWidget() const {
    return historyWidget;
}

eoCanvas *eoWindowMain::getCurrentCanvas() const {
    eoRender* render = getGlArea()->getRender();

    if(!render)
        return nullptr;

    return getGlArea()->getRender()->getCanvas();
}

void eoWindowMain::commandView() {
}


eoTabLayout* eoWindowMain::getTabLayout() const {
    
    return tabLayout;
}

eoRender *eoWindowMain::getRender() const {
    
    return glArea->getRender();
}

QDockWidget *eoWindowMain::getPropertiesDock() {
    
    return propertiesDock;
}
