//
// Created by ego on 11/24/17.
//

#ifndef GFX_PROTOTYPE_EOWINDOWMAIN_H
#define GFX_PROTOTYPE_EOWINDOWMAIN_H

#include <QMainWindow>
#include "../eoRender.h"
#include "eoOpenGLWidget.h"
#include "eoStatusBar.h"
#include "eoWidgetHistory.h"
#include "eoTabLayout.h"

class eoWidgetLayers;
class eoMenuBar;

class eoWindowMain : public QMainWindow {

    Q_OBJECT

    eoOpenGLWidget *glArea;
    eoStatusBar *statusBar;
    eoMenuBar *menuBar;
    eoWidgetLayers *layerWidget;
    eoWidgetHistory *historyWidget;
    eoTabLayout *tabLayout;
    QDockWidget *propertiesDock;


public:
    eoWindowMain(const char* window_title, int width, int height);

    eoOpenGLWidget *getGlArea() const;
    eoStatusBar *getStatusBar() const;
    void onRenderReady();

    eoWidgetLayers *getLayerWidget() const;
    eoWidgetHistory *getHistoryWidget() const;
    eoCanvas *getCurrentCanvas() const;
    eoRender *getRender() const;
    eoTabLayout *getTabLayout() const;
    QDockWidget *getPropertiesDock();

private slots:

    void commandView();

};


#endif //GFX_PROTOTYPE_EOWINDOWMAIN_H
