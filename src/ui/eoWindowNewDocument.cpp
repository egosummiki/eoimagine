//
// Created by ego on 19/09/18.
//

#include "eoWindowNewDocument.h"
#include <cstdio>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QToolButton>
#include <QComboBox>
#include <QSplitter>
#include <QSpacerItem>
#include <QPushButton>
#include <QListWidget>

eoWindowNewDocument::eoWindowNewDocument(eoWindowMain *mainWindow)
    : QWidget(mainWindow, Qt::Window), mainWindow(mainWindow)
{
    int width, height;

    auto screenGeometry = QApplication::desktop()->screenGeometry();
    width   = 600;
    height  = 400;

    setWindowTitle("Create New Document");
    setGeometry(
        screenGeometry.width() / 2 - width / 2,
        screenGeometry.height() / 2 - height / 2,
        width,
        height );

    setFixedSize(QSize(width, height));

    setFocus();

    auto layout         = new QHBoxLayout();
    auto layoutRight    = new QVBoxLayout();

    auto documentNameLabel = new QLabel();
    documentNameLabel->setText("Document Name");
    documentNameLabel->setStyleSheet("QLabel {font-weight: bold}");
    layoutRight->addWidget(documentNameLabel);
    documentNameField = new QLineEdit();
    documentNameField->setStyleSheet("QLineEdit {font-size: 22px}");
    documentNameField->setText("Untitled Document");
    layoutRight->addWidget(documentNameField);

    auto split = new QSpacerItem(30, 30);
    layoutRight->addItem(split);

    auto documentDimensionLabel = new QLabel();
    documentDimensionLabel->setText("Document Dimension");
    documentDimensionLabel->setStyleSheet("QLabel {font-weight: bold}");
    layoutRight->addWidget(documentDimensionLabel);
    auto documentDimensionLayout = new QGridLayout();

    auto documentWidthLabel = new QLabel();
    documentWidthLabel->setText("Width");
    documentDimensionLayout->addWidget(documentWidthLabel, 0, 0);
    documentWidthField = new QLineEdit();
    documentWidthField->setValidator(new QIntValidator(1, 10000, this));
    documentWidthField->setMaximumWidth(100);
    documentWidthField->setAlignment(Qt::AlignLeft);
    documentWidthField->setText("800");
    documentDimensionLayout->addWidget(documentWidthField, 0, 1);

    auto documentHeightLabel = new QLabel();
    documentHeightLabel->setText("Height");
    documentDimensionLayout->addWidget(documentHeightLabel, 1, 0);
    documentHeightField = new QLineEdit();
    documentHeightField->setValidator(new QIntValidator(1, 10000, this));
    documentHeightField->setMaximumWidth(100);
    documentHeightField->setAlignment(Qt::AlignLeft);
    documentHeightField->setText("600");
    documentDimensionLayout->addWidget(documentHeightField, 1, 1);

    auto dimensionUnit = new QComboBox();
    dimensionUnit->addItem("Pixels");
    dimensionUnit->addItem("Centimeters");
    dimensionUnit->addItem("Points");
    dimensionUnit->addItem("Milimeters");
    documentDimensionLayout->addWidget(dimensionUnit, 1, 2);
    documentDimensionLayout->addWidget(new QSplitter, 0, 3, 2, 1);

    layoutRight->addLayout(documentDimensionLayout);

    auto split2 = new QSpacerItem(30, 30);
    layoutRight->addItem(split2);

    auto canvasFillLabel = new QLabel();
    canvasFillLabel->setText("Canvas Fill");
    canvasFillLabel->setStyleSheet("QLabel {font-weight: bold}");
    layoutRight->addWidget(canvasFillLabel);

    auto canvasFillField = new QComboBox();
    canvasFillField->addItem("White");
    canvasFillField->addItem("Black");
    canvasFillField->addItem("Transparent");
    canvasFillField->setMaximumWidth(160);
    layoutRight->addWidget(canvasFillField);

    layoutRight->addWidget(new QSplitter);

    auto buttonLayout = new QHBoxLayout();
    buttonLayout->setAlignment(Qt::AlignRight);

    auto createButton = new QPushButton("Create");
    connect(createButton, &QPushButton::released, this, &eoWindowNewDocument::createDocument);
    buttonLayout->addWidget(createButton);

    layoutRight->addLayout(buttonLayout);

    auto presetList = new QListWidget();
    presetList->addItem("HD 1080p (1920x1080px)");
    presetList->addItem("HD 720p (1280x720px)");
    presetList->addItem("A5 Paper (148x210mm)");
    presetList->addItem("A4 Paper (210x297mm)");
    presetList->addItem("A3 Paper (297x420mm)");
    presetList->addItem("A2 Paper (420x594mm)");
    presetList->addItem("A1 Paper (594x841mm)");
    presetList->addItem("A0 Paper (841x1189mm)");
    presetList->setMaximumWidth(200);

    layout->addWidget(presetList);
    layout->addLayout(layoutRight);

    setLayout(layout);
}

void eoWindowNewDocument::createDocument() {
    
    int width, height;
    std::string name;

    width = std::stoi(documentWidthField->text().toStdString());
    height = std::stoi(documentHeightField->text().toStdString());
    name = documentNameField->text().toStdString();

    if(width < 1 || height < 1 || width > 10000 || height > 10000 || name.empty())
        return;

    mainWindow->getGlArea()->getRender()->performOnRender([this, name, width, height] (eoCanvas* canvas) {

        mainWindow->getGlArea()->getRender()->createDocument(name, width, height);
    });

    mainWindow->getGlArea()->update();


    this->close();
}
