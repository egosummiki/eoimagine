#pragma once
//
// Created by ego on 19/09/18.
//

#include <QWidget>
#include <QLineEdit>
#include "eoWindowMain.h"

class eoWindowNewDocument : public QWidget {

    Q_OBJECT

private:

    eoWindowMain *mainWindow;
    QLineEdit *documentNameField;
    QLineEdit *documentWidthField;
    QLineEdit *documentHeightField;

public:

    eoWindowNewDocument(eoWindowMain *mainWindow);

private slots:

    void createDocument();


};
