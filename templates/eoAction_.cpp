#include "ACTION_NAME.h"

ACTION_NAME::ACTION_NAME(char* name, eoCanvas *canvas)
    : eoAction(name), canvas(canvas)
{
}

void ACTION_NAME::perform() {
}

void ACTION_NAME::neglect() {
}
