#pragma once

#include "eoAction.h"
#include "../eoCanvas.h"

class ACTION_NAME : public eoAction {

    eoCanvas*   canvas;

public:
    ACTION_NAME(char* name, eoCanvas* canvas);

    void perform() override;
    void neglect() override;
};
