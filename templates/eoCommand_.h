#pragma once

#include "eoCommand.h"
#include "../eoCanvas.h"

class COMMAND_NAME : public eoCommand {

    eoCanvas* canvas;

public:
    explicit COMMAND_NAME(eoCanvas* canvas);

    void execute(QString arguments) override;
};
