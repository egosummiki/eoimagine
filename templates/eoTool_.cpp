#include "TOOL_NAME.h"

TOOL_NAME::TOOL_NAME(eoRender* render)
    : eoTool(render)
{
}

eoTool::CastingResult TOOL_NAME::onCast() {
    
    return ResultSuccess;
}

void TOOL_NAME::onLose() {

}

eoTool::SelectionLabel TOOL_NAME::getLabel() {
    
    return (eoTool::SelectionLabel) {"NORMAL", "#24cd6a", "#31363b"};
}
