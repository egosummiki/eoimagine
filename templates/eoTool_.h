#pragma once

#include "eoTool.h"
#include "../eoRender.h"

class TOOL_NAME : public eoTool {

public:

    TOOL_NAME(eoRender* render);
    CastingResult onCast() override;
    void onLose() override;
    SelectionLabel getLabel() override;
};
